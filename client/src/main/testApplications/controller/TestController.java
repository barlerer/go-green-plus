package controller;

import com.google.inject.matcher.Matchers;
import hashing.Hashing;
import model.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
//import sun.security.util.Password;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@PrepareForTest
public class TestController {

    JSONObject jsonObj1;
    JSONArray testArr;
    User userNull;
    User user1;
    User user2;
    User zeroUser;
    Achievement achievement;
    Achievement zeroAchievement;
    ArrayList<Activity> actArray;
    Activity help;
    ArrayList<Activity> test;
    ArrayList<User> friendsList;
    User userMax;


    @Mock
    private DatabaseModel testDB;

    @Mock
    private ActivityLister actLister;

    @InjectMocks
    @Spy
    private Controller controller;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        jsonObj1 = new JSONObject();
        jsonObj1.put("name", "Bar");
        testArr = new JSONArray();
        testArr.put(jsonObj1);
        userNull = new User(null, null);
        when(testDB.getActivites()).thenReturn(testArr);
        when(testDB.getIntfromServer(anyString())).thenReturn(50);
        when(testDB.getDoublefromserver(anyString())).thenReturn(27.8);
        when(testDB.getAllActivitesOfUser(anyString())).thenReturn(testArr);
        when(testDB.registerUser(any())).thenReturn(true);
        when(testDB.addFriend(any(), anyString())).thenReturn(true);
        user1 = new User("123", "LoveTesting", 15);
        user2 = new User("12345", "LoveTesTingG", 16);
        zeroUser = new User(0);
        zeroAchievement = new Achievement("This achievement has 0 id", 0, 420);
        achievement = new Achievement("Testers of the World Unite", 1848, 1917);

        actArray = new ArrayList<>();
        help = new Activity("get help", 99);
        help.setQuantity(2);
        help.setCo2(4.5);
        help.setPoints(100);

        actArray.add(help);

        test = new ArrayList<>();
        test = controller.listActivities(7);
        userMax = new User("123", "Pulica", 99);
        userMax.setPoints(10000);


        friendsList = new ArrayList<>();
        friendsList.add(userMax);



    }

    @Test
    public void testListActivities() {

        assertEquals("Eating a vegetarian meal" , test.get(0).getActivityName());
    }

    @Test
    public void tesCountProduce() {

        assertEquals(1,controller.countProduce(7));
    }

    @Test
    public void testCountPublicTransport() {

        assertEquals(0, controller.countPublicTransport(7));
    }

    @Test
    public void testCountActivity() {

        assertEquals(3, controller.countActivity(7));
    }

    @Test
    public void testCountMorePoints() {

        assertEquals(0, controller.countMorePoints(friendsList, 7));
    }

    @Test
    public void testCountMorePointsEqual() {
        when(testDB.getPointsOfUser(anyInt())).thenReturn(10000);
        assertEquals(0, controller.countMorePoints(friendsList, 7));
    }

    @Test
    public void testCountMorePointsLarger() {
        when(testDB.getPointsOfUser(anyInt())).thenReturn(100000);
        assertEquals(1, controller.countMorePoints(friendsList, 7));
    }



    @Test
    public void testCountCarbon() {

        assertTrue(controller.countCarbon(7) > 0.0);
    }

    @Test
    public void testCountPoints() {

        assertTrue(controller.countPoints(7) == 56);
    }

    @Test
    public void testCountBike() {

        assertEquals(0, controller.countBike(7));
    }

    @Test
    public void testCountVegetarianMeal() {

        assertEquals(2, controller.countVegetarianMeal(7));
    }

    @Test
    public void testAddAchievementUser() {
        when(testDB.addAchievementUser(user1.getId(), achievement.getId())).thenReturn(true);
        assertTrue(controller.addAchievementUser(user1.getId(), achievement.getId()));
    }

    @Test
    public void testAddAchievementUserZeroUserID() {
        assertFalse(controller.addAchievementUser(zeroUser.getId(), achievement.getId()));
    }

    @Test
    public void testAddAchievementUserZeroAchievementID() {
        assertFalse(controller.addAchievementUser(user1.getId(), zeroAchievement.getId()));
    }

    @Test
    public void testServerCon() {
        ArrayList<String> testServer = new ArrayList<>();
        testServer.add("Bar");
        assertEquals(testServer, controller.getAllActivites());
    }



    @Test
    public void testCO2ByUser() {
        when(testDB.getCO2ByUser(2)).thenReturn(25.8);
        assertEquals(25.8, controller.getCO2ByUser(2), 5);
    }

    @Test
    public void testGetterAllActivities() {
        assertEquals(testArr, controller.getAllUserActivities(2));
    }

    @Test
    public void testGetPointsOfUser() {
        when(testDB.getPointsOfUser(4)).thenReturn(56);
        assertEquals(controller.getPointsOfUser(4), 56);
    }

    @Test
    public void userVerificationNullParam(){
        assertEquals(controller.verifyUser(userNull), -1);
    }

    @Test
    public void userPasswordEditGoodCred(){

        User user = new User("123456", "IloveTesting");
        String newPassword = "ILOVETESTING";
        doReturn(1).when(controller).verifyUser(user);
        doReturn(true).when(testDB).updatePassword(any(), anyString());
        assertTrue(controller.changePassword(user, newPassword));
    }

    @Test
    public void userPasswordEditNull(){
        assertFalse(controller.changePassword(null, "IloveTesting"));
    }

    @Test
    public void userPasswordEditNullPass(){
        assertFalse(controller.changePassword(new User("ILOVETEST","TEST"),null));
    }

    @Test
    public void userVerificationNoConnection(){
        User user = new User("1234", "TestUse");
        when(testDB.retrieveUserByUserName(any())).thenReturn(null);
        assertEquals(controller.verifyUser(user), -1);
    }

    @Test
    public void userVerificationNullPassword(){
        User user = new User("1234", "TestUse");
        when(testDB.retrieveUserByUserName(any())).thenReturn(new User(null, "Testing"));
        assertEquals(controller.verifyUser(user), -1);
    }

    @Test
    public void userVerificationWrongCred(){
        User user = new User("12345", "TestUse1");
        User userOnServer = new User("$2a$06$JIdJdxQFfvT.aQX4GW.fYuDyvNPPRKsjeq8cPIgadUNDVGAFm.6bm",
                "TestUse", 3);

        when(testDB.retrieveUserByUserName("TestUse1")).thenReturn(userOnServer);
        assertEquals(controller.verifyUser(user), -1);
    }

    @Test
    public void userVerificationGoodCred(){
        User user = new User("1234", "TestUse");
        User userOnServer = new User("$2a$06$JIdJdxQFfvT.aQX4GW.fYuDyvNPPRKsjeq8cPIgadUNDVGAFm.6bm",
                "TestUse", 3);

        when(testDB.retrieveUserByUserName("TestUse")).thenReturn(userOnServer);
        assertEquals(controller.verifyUser(user), 3);
    }

    @Test
    public void userVerificationNullDetails(){
        userNull.setUsername("Jo");
        assertEquals(-1, controller.verifyUser(userNull));
    }

    @Test
    public void testRemoveFriendNullUser() {
        assertFalse(controller.removeFriend(null, user1));
    }

    @Test
    public void testRemoveFriendNullFriend() {
        assertFalse(controller.removeFriend(user1, null));
    }


    @Test
    public void testRemoveFriendUserBadId() {
        user1.setId(0);
        assertFalse(controller.removeFriend(user1, user2));
    }

    @Test
    public void testRemoveFriendFriendBadId() {
        user2.setId(0);
        assertFalse(controller.removeFriend(user1, user2));
    }

    @Test
    public void testRemoveFriendFriendNull() {

        assertFalse(controller.removeFriend(null, null));
    }

    @Test
    public void testRemoveFriend() {
        when(testDB.removeFriend(any(), any())).thenReturn(true);
        assertTrue(controller.removeFriend(user1, user2));
    }

    @Test
    public void testGetAchievementsNulluser() {
        assertNull(controller.getAchievements(null));
    }

    @Test
    public void testGetAchievementsBadId() {
        user1.setId(0);
        assertNull(controller.getAchievements(user1));
    }

    @Test
    public void testGetAchievements() {
        ArrayList<Achievement> achievementArrayList = new ArrayList<>();
        Achievement achievement = new Achievement("Ilovetesting", 2, 200);
        achievementArrayList.add(achievement);
        when(testDB.getAchievements(any())).thenReturn(achievementArrayList);
        assertEquals(achievementArrayList, controller.getAchievements(user1));
    }
    
    @Test
    public void usePasswordEditBadCred(){
        User userTest = new User("1234567", "AyrthonK");
        String newPassword = "Hello World!!!!";
        assertFalse(controller.changePassword(userTest, newPassword));
    }

    @Test
    public void userVerificationNullUser(){
        assertEquals(-1, controller.verifyUser(null));
    }


    @Test
    public void registerUserNullPassword(){
        User badUser = new User(null, "ILoveTesting");
        assertFalse(controller.registerUser(badUser));
    }

    @Test
    public void registerUserNullUsername(){
        User badUser = new User("ILoveTesting", null);
        assertFalse(controller.registerUser(badUser));
    }

    @Test
    public void registerUserNullUser(){
        User badUser = null;
        assertFalse(controller.registerUser(badUser));
    }

    @Test
    public void registerUser(){
        User user = new User("ILove","Testing");
        assertTrue(controller.registerUser(user));
    }

    @Test
    public void registerUserEmptyUser(){
        User user = new User("ILove","");
        assertFalse(controller.registerUser(user));
    }

    @Test
    public void registerUserEmptyPass(){
        User user = new User("","a");
        assertFalse(controller.registerUser(user));
    }

    @Test
    public void fillUserWithOnlyId(){
        User user = new User(null, null, 2);
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                Object[] arguments = invocation.getArguments();
                if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                    User user = (User) arguments[0];
                    user.setUsername("BarL");
                }
                return null;
            }
        }).when(testDB).getUsernameOfUser(user);
        controller.fillDataFromServer(user);
        assertEquals("BarL", user.getUsername());
    }

    @Test
    public void testNullUserFill(){
        controller.fillDataFromServer(null);
        verify(testDB, times(0)).getIdOfUser(any());
        verify(testDB, times(0)).getUsernameOfUser(any());
    }

    @Test
    public void fillUserWithOnlyUsername(){
        User user = new User(null, "ILoveTesting");
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                Object[] arguments = invocation.getArguments();
                if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                    User user = (User) arguments[0];
                    user.setId(2);
                }
                return null;
            }
        }).when(testDB).getIdOfUser(user);
        controller.fillDataFromServer(user);
        assertEquals(2, user.getId());
    }

    @Test
    public void addFriendNullUser(){
        assertFalse(controller.addFriend(null, "ILoveTesting"));
    }

    @Test
    public void addFriendNullFriend(){
        User user = new User("Ilove","Testing", 5);
        assertFalse(controller.addFriend(user, null ));
    }

    @Test
    public void addFriendEmptyFriend(){
        User user = new User("Ilove","Testing", 5);
        assertFalse(controller.addFriend(user, "" ));
    }

    @Test
    public void addFriendNoId(){
        User user = new User("Ilove","Testing");
        assertFalse(controller.addFriend(user, "" ));
    }

    @Test
    public void addFriend(){
        User user = new User("Ilove","Testing", 2);
        assertTrue(controller.addFriend(user, "James"));
    }

    @Test
    public void getFriendsNullUser(){
        assertNull(controller.getFriends(null));
    }

    @Test
    public void getFriendsNoId(){
        assertNull(controller.getFriends(userNull));
    }

    @Test
    public void getFriends(){
        ArrayList<User> testList = new ArrayList<>();
        User user1 = new User(null, "ILoveTesting", 6);
        User user2 = new User(null, "ILoveTesting2", 7);
        User user3 = new User(null, "IAmTheTester", 99);
        testList.add(user1);
        testList.add(user2);
        when(testDB.getFriendsList(user3)).thenReturn(testList);
        assertEquals(testList, controller.getFriends(user3));
    }

    @Test
    public void testGetActivities(){
        ArrayList<Activity> listOfActivities = new ArrayList<>();
        Activity activity = new Activity("ILoveTesting", 1);
        listOfActivities.add(activity);
        when(testDB.getAllActivities()).thenReturn(listOfActivities);
        assertEquals(listOfActivities, controller.getActivities());
    }

    @Test
    public void testSendActivityNull(){
        User user = new User(null, null, 1);
        User baduser = new User(null, null, 0);
        Activity activity = new Activity("ILoveTesting", 1);
        Activity badActivity = new Activity("ILoveTesting", -5);
        assertFalse(controller.sendDoneActivity(user, null));
        assertFalse(controller.sendDoneActivity(null, activity));
        assertFalse(controller.sendDoneActivity(user, badActivity));
        assertFalse(controller.sendDoneActivity(baduser, activity));
    }

    @Test
    public void testSendActivity(){
        User user = new User(null, null, 1);
        Activity activity = new Activity("ILoveTesting", 1);
        when(testDB.sendDoneActivityToServer(user, activity)).thenReturn(true);
        assertTrue(controller.sendDoneActivity(user, activity));
    }

    @Test
    public void testGetAllAchievements() {

        ArrayList<Achievement> testArray = new ArrayList<Achievement>();
        Achievement achievement = new Achievement("test", 1, 100 );
        testArray.add(achievement);

        Achievement achievement1 = new Achievement("Testing", 2 , 200);
        testArray.add(achievement1);
        when(testDB.getAllAchievements()).thenReturn(testArray);

        assertEquals(testArray, controller.getAllAchievements());

    }


    @Test
    public void testParseDate() {
        Date date = new Date(10000);
        String testDate = new String("19700001");

        when(testDB.dateParser(any())).thenReturn(date);
        assertEquals(date,testDB.dateParser(testDate));

    }


}
