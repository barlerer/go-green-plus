package hashing;
import org.junit.Test;

import static org.junit.Assert.*;

public class HashingTest {

    @Test
    public void testCryptDechyper() {
        String password="HelloTest";
        String hashedPassword= Hashing.crypt(password);
        assertTrue(Hashing.verifyCrypt(password, hashedPassword));
    }

    @Test
    public void testCryptDechyperDiffPass() {
        String password="HelloTest";
        String password2="Hellotest";
        String hashedPassword= Hashing.crypt(password);
        assertFalse(Hashing.verifyCrypt(password2, hashedPassword));
    }

    @Test
    public void testCryptDechyperNull() {
        String password="HelloTest";
        String password2=null;
        String hashedPassword= Hashing.crypt(password);
        assertFalse(Hashing.verifyCrypt(password2, hashedPassword));
    }

    @Test
    public void testCryptNull() {
        String password=null;
        assertNull(Hashing.crypt(password));
    }

    @Test
    public void testCounstructor() {
        Hashing hashing = new Hashing();
        assertTrue(hashing instanceof  Hashing);
    }

}
