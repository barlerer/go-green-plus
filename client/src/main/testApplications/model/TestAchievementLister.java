package model;

import controller.Controller;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class TestAchievementLister {

    AchievementLister lister1;
    AchievementLister lister2;
    AchievementLister lister3;
    AchievementLister lister4;
    ArrayList<Activity> testarray;
    Activity activity1;
    Activity activity2;
    Activity activity3;

    JSONArray jsonarr;
    JSONObject jsonObject;
    JSONObject jsonObject1;
    JSONObject jsonObject2;
    JSONObject jsonObject3;
    ArrayList<Activity> activityArrayList;

    @Mock
    private Controller controller;

    @InjectMocks
    AchievementLister achievementLister;

    @Before
    public void testSetup(){

        MockitoAnnotations.initMocks(this);

        lister1 = new AchievementLister(1);
        lister2 = new AchievementLister(2);
        lister3 = new AchievementLister(3);
        lister4 = new AchievementLister(4);
        activity1 = new Activity("Eating a vegetarian meal",1);
        activity2 = new Activity("Buying local produce", 2);
        activity3 = new Activity("Using a bike instead of a car", 3);
        testarray = new ArrayList<>();

        jsonObject = new JSONObject();
        jsonObject1 = new JSONObject();
        jsonObject2 = new JSONObject();
        jsonObject3 = new JSONObject();
        jsonarr = new JSONArray();

        jsonObject2.put("points", 100);
        jsonObject2.put("kgCO2Emission", 3.5);

        jsonObject.put("name", "die testing");
        jsonObject.put("id", 69);
        jsonObject.put("co2Category", jsonObject2);

        jsonObject1.put("activity",jsonObject);
        jsonObject1.put("quantity", 2);

        jsonarr.put(jsonObject1);

        when(controller.getAllUserActivities(0)).thenReturn(jsonarr);
    }

    @Test
    public void testGetJsonArray() {
        assertEquals(achievementLister.getJsonArray(), jsonarr);
    }

    @Test
    public void testGetActivityArray() {
        ArrayList<Activity> test = achievementLister.getActivityArray();
        assertEquals("die testing", test.get(0).getActivityName());
        assertEquals(2, test.get(0).getQuantity());
        assertEquals(3.5, test.get(0).getCo2(),0);
    }

    @Test
    public void testGetId() {
        assertEquals(lister1.getId(),1);
        assertEquals(lister2.getId(),2);
        assertNotEquals(lister1.getId(),3);
    }

    @Test
    public void testSetId() {
        lister1.setId(99);
        assertEquals(lister1.getId(),99);
        assertNotEquals(lister1.getId(),420);
    }

    @Test
    public void testListVegetarianMeal() {
        activity1.setQuantity(20);
        testarray.add(activity1);

        activity2.setQuantity(100);
        testarray.add(activity2);

        activity3.setQuantity(2);
        testarray.add(activity1);
        testarray.add(activity3);

        assertEquals(2, lister3.listVegetarianMeal(testarray));
        assertNotEquals(1, lister3.listVegetarianMeal(testarray));
    }

    @Test
    public void testListBike() {
        activity3.setQuantity(20);
        testarray.add(activity3);

        activity2.setQuantity(100);
        testarray.add(activity2);

        activity1.setQuantity(2);
        testarray.add(activity3);
        testarray.add(activity1);

        assertEquals(2, lister3.listBike(testarray));
        assertNotEquals(1, lister3.listBike(testarray));
    }

    @Test
    public void testCountPoints() {
        activity1.setPoints(5);
        testarray.add(activity1);
        assertEquals(5, lister1.countPoints(testarray));
        assertNotEquals(420, lister1.countPoints(testarray));
    }
}
