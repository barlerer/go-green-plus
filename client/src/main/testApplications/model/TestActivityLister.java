package model;

import controller.Controller;
import kong.unirest.JsonNode;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class TestActivityLister {

    ActivityLister lister1;
    ActivityLister lister2;
    ActivityLister lister3;
    ActivityLister lister4;
    ArrayList<Activity> testarray;
    Activity activity1;
    Activity activity2;
    Activity activity3;
    Activity activity4;

    JSONArray jsonarr;
    JSONObject jsonObject;
    JSONObject jsonObject1;
    JSONObject jsonObject2;
    JSONObject jsonObject3;
    ArrayList<Activity> activityArrayList;

    @Mock
    private Controller controller;

    @InjectMocks
    ActivityLister activityLister;

    @Before
    public void testSetUp() {

        MockitoAnnotations.initMocks(this);

        jsonarr = new JSONArray();
        lister1 = new ActivityLister(1);
        lister2 = new ActivityLister(2);
        lister3 = new ActivityLister(3);
        lister4 = new ActivityLister(4);
        activity1 = new Activity("Sleeping",10);
        activity2 = new Activity("Using public transport instead of a car", 4);
        activity3 = new Activity("Buying local produce", 2);
        activity4 = new Activity("Eating a vegetarian meal", 1);
        testarray = new ArrayList<>();

        jsonObject = new JSONObject();
        jsonObject1 = new JSONObject();
        jsonObject2 = new JSONObject();
        jsonObject3 = new JSONObject();

        jsonObject2.put("points", 100);
        jsonObject2.put("kgCO2Emission", 3.5);

        jsonObject.put("name", "die testing");
        jsonObject.put("id", 69);
        jsonObject.put("co2Category", jsonObject2);

        jsonObject1.put("activity",jsonObject);
        jsonObject1.put("quantity", 2);

        jsonarr.put(jsonObject1);

        activityArrayList = controller.getActivities();

        when(controller.getAllUserActivities(0)).thenReturn(jsonarr);

    }

    @Test
    public void testGetJsonArray() {
        assertEquals(activityLister.getJsonArray(), jsonarr);
    }

    @Test
    public void testGetActivityArray() {
        ArrayList<Activity> test = activityLister.getActivityArray();
        assertEquals("die testing", test.get(0).getActivityName());
        assertEquals(69, test.get(0).getActivityId());
    }

    @Test
    public void testGetId() {
        assertEquals(lister1.getId(),1);
        assertEquals(lister2.getId(),2);
        assertNotEquals(lister1.getId(),3);

    }

    @Test
    public void testSetId() {

        lister1.setId(99);
        assertEquals(lister1.getId(),99);
        assertNotEquals(lister1.getId(),420);

    }

    @Test
    public void testEqualsSameId() {
        lister2.setId(420);
        lister1.setId(420);
        assertTrue(lister1.equals(lister2));

    }

    @Test
    public void testEqualsDifferentId() {
        lister1.setId(999);
        lister2.setId(44);
        assertFalse(lister1.equals(lister2));
    }

    @Test
    public void testEqualsSameObject() {
        assertTrue(lister1.equals(lister1));
    }

    @Test
    public void testEqualsNull() {
        assertFalse(lister4.equals(null));
    }

    @Test
    public void testListLocalProduce() {
        activity3.setQuantity(20);
        testarray.add(activity3);

        activity2.setQuantity(100);
        testarray.add(activity2);

        activity1.setQuantity(2);
        testarray.add(activity1);
        testarray.add(activity3);


        assertEquals(2, lister3.listLocalProduce(testarray));
        assertNotEquals(1, lister3.listLocalProduce(testarray));
    }

    @Test
    public void testListPublicTrans() {
        activity3.setQuantity(20);
        testarray.add(activity3);

        activity2.setQuantity(100);
        testarray.add(activity2);

        activity1.setQuantity(2);
        testarray.add(activity1);
        testarray.add(activity3);
        testarray.add(activity2);

        Activity newAct = new Activity("Using public transport instead of a car", 4);
        newAct.setQuantity(1000);
        testarray.add(newAct);

        assertEquals(1200,lister1.listPublicTrans(testarray));
        assertNotEquals(100,lister1.listPublicTrans(testarray));
        assertNotEquals(200,lister1.listPublicTrans(testarray));
    }

    @Test
    public void testListVegetarianMeal() {

        testarray.add(activity1);
        testarray.add(activity2);
        testarray.add(activity4);

        Activity act = new Activity("Eating a vegetarian meal", 1);
        testarray.add(act);

        assertEquals(2,lister1.listVegetarianMeal(testarray));
        assertNotEquals(1,lister1.listVegetarianMeal(testarray));
        assertNotEquals(3,lister2.listVegetarianMeal(testarray));
    }

    @Test
    public void testListBike() {

        testarray.add(activity1);
        testarray.add(activity3);
        testarray.add(activity1);

        Activity bike = new Activity("Using a bike instead of a car", 3);
        Activity bike2 = new Activity("Using a bike instead of a car", 3);
        bike.setQuantity(10);
        bike2.setQuantity(2);

        testarray.add(bike);
        testarray.add(bike2);

        assertEquals(12, lister1.listBike(testarray));
        assertNotEquals(100, lister1.listBike(testarray));

    }

    @Test
    public void testCountCarbon() {

        activity1.setCo2(10.0);
        activity1.setQuantity(2);

        activity3.setCo2(30.0);
        activity3.setQuantity(2);

        testarray.add(activity1);
        testarray.add(activity3);

        assertEquals(80.0,lister1.countCarbon(testarray), 0);
        assertNotEquals(20.0,lister1.countCarbon(testarray));
        assertNotEquals(60.0,lister2.countCarbon(testarray));
    }

    @Test
    public void testCountPoints() {
        activity1.setPoints(10);
        activity1.setQuantity(2);

        activity3.setPoints(30);
        activity3.setQuantity(2);

        testarray.add(activity1);
        testarray.add(activity3);

        assertEquals(80, lister1.countPoints(testarray), 0);
        assertNotEquals(20, lister1.countPoints(testarray));
        assertNotEquals(60, lister2.countPoints(testarray));
    }
}