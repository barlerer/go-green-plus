package model;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class TestActivity {

    Activity activity;
    Activity activity1;
    Activity activity2;
    Activity activity3;
    Activity activity4;
    Activity activityDate;
    final String act = "EatingVegMeal";
    final String act1 = "LocalBuying";
    final String act2 = "Bicycle";
    Date date1 = new Date(1000);
    Date date2 = new Date(100000);

    @Before
    public void setUp(){
        activity = new Activity(act, 1);
        activity1 = new Activity(act1, 15);
        activity2 = new Activity(act2, 2);
        activity3 = new Activity(act, 1);
        activity4 = new Activity("LoveTesting", 19);
        activity1.setPoints(20);
        activity1.setCo2(17.54);

        activityDate = new Activity("DateTesting", 100);
        activityDate.setDate(date1);
    }

    @Test
    public void testConstructor(){
        assertEquals(activity.getActivityName(), act);
        assertEquals(activity.getActivityId(), 1);
    }

    @Test
    public void testGetterOfName(){
        assertEquals(activity2.getActivityName(), act2);
    }

    @Test
    public void testGetterOfNameDiff(){
        assertNotEquals(activity2.getActivityName(), act1);
    }

    @Test
    public void testGetterOfID(){
        assertEquals(activity1.getActivityId(), 15);
    }

    @Test
    public void testGetterOfIDDiff(){
        assertNotEquals(activity1.getActivityId(), 12);
    }

    @Test
    public void testGetterOfPoints(){
        assertEquals(20, activity1.getPoints());
    }

    @Test
    public void testGetterOfCO2(){
        assertEquals(17.54, activity1.getCo2(), 10);
    }

    @Test
    public void testSetterOfPoints(){
        activity2.setPoints(1995);
        assertEquals(1995, activity2.getPoints());
        assertNotEquals(17, activity2.getPoints());
    }

    @Test
    public void testSetterOfCO2(){
        activity3.setCo2(18.6543);
        assertEquals(18.6543, activity3.getCo2(), 10);
    }

    @Test
    public void testDefConstructor(){
        assertEquals(0, activity4.getPoints());
        assertEquals(0.0, activity4.getCo2(), 5);
    }

    @Test
    public void testGetterQuantity(){
        assertEquals(1, activity4.getQuantity());
    }

    @Test
    public void testSetterQuantity(){
        activity4.setQuantity(3);
        assertEquals(3, activity4.getQuantity());
        assertNotEquals(1, activity4.getQuantity());
    }

    @Test
    public void testSetDate() {
        activityDate.setDate(date2);
        assertEquals(date2, activityDate.getDate());
        assertNotEquals(date1,activityDate.getDate());
    }

    @Test
    public void testGetDate() {

        assertEquals(date1,activityDate.getDate());
        assertNotEquals(date2,activityDate.getDate());
    }


}
