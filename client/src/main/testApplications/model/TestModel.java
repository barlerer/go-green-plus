package model;


import kong.unirest.*;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.Null;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Unirest.class)

public class TestModel {

    @Mock
    private GetRequest getRequest;

    @Mock
    private HttpResponse<JsonNode> httpResponse;

    @Mock
    private HttpRequestWithBody requestWithBody;

    @Mock
    private RequestBodyEntity requestBodyEntity;

    @Mock
    JsonNode value;

    @Mock
    SimpleDateFormat parser1;

    @Mock
    HttpResponse httpString;


    @InjectMocks
    DatabaseModel databaseModel;

    public User user1;
    JSONArray jsonArray;
    JSONObject jsonObject;
    JSONObject jsonObject1;
    JSONObject jsonObject4;
    JSONArray jsonArray3;
    JSONObject jsonObject5;
    JSONObject jsonObject6;
    JSONObject jsonObject7;
    JSONObject jsonObject8;
    JSONObject jsonObject9;
    JSONArray jsonArray4;
    ArrayList<Activity> activityArrayList;

    @Before
    public void setUp() {
        PowerMockito.mockStatic(Unirest.class);
        when(Unirest.get(anyString())).thenReturn(getRequest);
        when(getRequest.header("accept", "application/json")).thenReturn(getRequest);
        when(getRequest.asJson()).thenReturn(httpResponse);

        //FROM THE PATCH IF NEEDED TO MOVE
        when(requestWithBody.header("accept", "application/json")).thenReturn(requestWithBody);
        when(requestWithBody.header("Content-Type", "application/json")).thenReturn(requestWithBody);
        when(requestBodyEntity.asJson()).thenReturn(httpResponse);

        //post
        when(Unirest.post(anyString())).thenReturn(requestWithBody);
        when(requestWithBody.header("accept", "application/json")).thenReturn(requestWithBody);
        when(requestWithBody.header("Content-Type", "application/json")).thenReturn(requestWithBody);
        when(requestWithBody.body(any(JSONObject.class))).thenReturn(requestBodyEntity);
        when(requestBodyEntity.asJson()).thenReturn(httpResponse);
        when(httpResponse.getStatus()).thenReturn(200);

        //users
        user1 = new User("LoveTesting", "User", 1);

        //
        when(databaseModel.jsonRequest(anyString())).thenReturn(httpResponse);
        when(requestWithBody.body(jsonObject)).thenReturn(requestBodyEntity);
        when(getRequest.asString()).thenReturn(httpString);


        //POST NO BODY
        when(Unirest.post(anyString())).thenReturn(requestWithBody);
        when(requestWithBody.header("accept", "application/json")).thenReturn(requestWithBody);
        when(requestWithBody.header("Content-Type", "application/json")).thenReturn(requestWithBody);
        when(requestWithBody.asJson()).thenReturn(httpResponse);

        //GetActivities
        jsonArray = new JSONArray();
        jsonObject = new JSONObject();
        jsonObject1 = new JSONObject();
        jsonObject1.put("kgCO2Emission", 12.27);
        jsonObject1.put("points", 1227);
        jsonObject.put("name", "test1");
        jsonObject.put("id", 22);
        jsonObject.put("co2Category", jsonObject1);
        jsonArray.put(jsonObject);
        when(httpResponse.getBody()).thenReturn(value);
        when(value.getArray()).thenReturn(jsonArray);
        activityArrayList = databaseModel.getAllActivities();

        //Delete
        when(Unirest.delete(anyString())).thenReturn(requestWithBody);

        //User Achievements
        jsonArray3 = new JSONArray();
        jsonObject4 = new JSONObject();
        jsonObject5 = new JSONObject();

        //
        jsonArray3 = new JSONArray();
        jsonObject5 = new JSONObject();
        jsonObject6 = new JSONObject();
        jsonObject7 = new JSONObject();

        jsonObject8 = new JSONObject();
        jsonObject9 = new JSONObject();
        jsonArray4 = new JSONArray();
    }

    @Test
    public void testGetRequest() {


        assertEquals(200, databaseModel.jsonRequest("12345").getStatus());
    }

    @Test
    public void testGetRequestBadPath() {
        when(httpResponse.getStatus()).thenReturn(404);
        assertEquals(404, databaseModel.jsonRequest("12345").getStatus());
    }

    @Test
    public void testGetRequestJsonNode() {
        when(httpResponse.getStatus()).thenReturn(200);
        when(httpResponse.getBody()).thenReturn(value);
        assertEquals(value, databaseModel.jsonRequest("12345").getBody());
    }

    @Test(expected = NullPointerException.class)
    public void testGetRequestException() {
        assertNull(databaseModel.jsonRequest(null));
    }

    @Test
    public void testGetRequestUnirestException() {
        when(Unirest.get("123")).thenThrow(new UnirestException("123"));
        assertNull(databaseModel.jsonRequest("123"));
    }

    @Test
    public void testretrieveUserByUserNameBadRequest() {
        when(httpResponse.getStatus()).thenReturn(404);
        assertNull(databaseModel.retrieveUserByUserName("IloveTesting"));
    }

    @Test
    public void testretrieveUserByUserNoUser() {

        JSONObject jsonObject = new JSONObject();

        when(value.getObject()).thenReturn(jsonObject);
        User user = databaseModel.retrieveUserByUserName("IloveTesting");

        assertNull(user.getUsername());
        assertNull(user.getPassword());
        assertEquals(0, user.getId());
    }

    @Test
    public void testretrieveUserByUserNoID() {
        when(httpResponse.getStatus()).thenReturn(200);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("hashedPassword", "LoveTesting");
        when(httpResponse.getBody()).thenReturn(value);
        when(value.getObject()).thenReturn(jsonObject);
        User user = databaseModel.retrieveUserByUserName("IloveTesting");

        assertNull(user.getUsername());
        assertNull(user.getPassword());
        assertEquals(0, user.getId());
    }

    @Test
    public void testretrieveUserByUserNoHashedPassword() {
        when(httpResponse.getStatus()).thenReturn(200);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", "3");
        when(httpResponse.getBody()).thenReturn(value);
        when(value.getObject()).thenReturn(jsonObject);
        User user = databaseModel.retrieveUserByUserName("IloveTesting");

        assertNull(user.getUsername());
        assertNull(user.getPassword());
        assertEquals(0, user.getId());
    }

    @Test
    public void testretrieveUserByUserGoodRequset() {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", 3);
        jsonObject.put("hashedPassword", "LoveTesting");

        when(value.getObject()).thenReturn(jsonObject);
        User user = databaseModel.retrieveUserByUserName("IloveTesting");

        assertEquals("IloveTesting", user.getUsername());
        assertEquals("LoveTesting", user.getPassword());
        assertEquals(3, user.getId());
    }

    @Test
    public void testUpdatePasswordSuccess() {
        User user12 = new User("123", "321", 2);
        String newPassword = "IloveTESTING";
        when(Unirest.patch(anyString())).thenReturn(requestWithBody);
        when(requestWithBody.body(any(JSONObject.class))).thenReturn(requestBodyEntity);
        when(httpResponse.getStatus()).thenReturn(200);
        assertTrue(databaseModel.updatePassword(user12, newPassword));
    }

    @Test
    public void testUpdatePasswordFail() {
        User user12 = new User("123", "321", 2);
        String newPassword = "IloveTESTING";
        when(Unirest.patch(anyString())).thenReturn(requestWithBody);
        when(requestWithBody.body(any(JSONObject.class))).thenReturn(requestBodyEntity);
        when(httpResponse.getStatus()).thenReturn(404);
        assertFalse(databaseModel.updatePassword(user12, newPassword));
    }

    @Test
    public void testPatchGoodRequest() {
        JSONObject jsonObject = new JSONObject();
        when(Unirest.patch(anyString())).thenReturn(requestWithBody);
        when(requestWithBody.body(jsonObject)).thenReturn(requestBodyEntity);

        assertEquals(200, databaseModel.jsonPatchRequest("123", jsonObject).getStatus());
    }

    @Test
    public void testPatchBadRequest() {
        JSONObject jsonObject = new JSONObject();
        when(Unirest.patch(anyString())).thenReturn(requestWithBody);

        when(httpResponse.getStatus()).thenReturn(404);
        assertEquals(404, databaseModel.jsonPatchRequest("123", jsonObject).getStatus());
    }

    @Test
    public void testPatchUnirestException() {
        JSONObject jsonObject = new JSONObject();
        when(Unirest.patch(anyString())).thenReturn(requestWithBody);

        when(httpResponse.getStatus()).thenReturn(404);
        when(Unirest.patch(anyString())).thenThrow(new UnirestException("123"));
        assertNull(databaseModel.jsonPatchRequest("123", jsonObject));
    }

    @Test
    public void testGetIntFromServer() {

        when(httpString.getBody()).thenReturn("1234");
        assertEquals(1234, databaseModel.getIntfromServer("123"));
    }

    @Test
    public void testGetIntBadValue() {

        when(Unirest.get(anyString())).thenThrow(new UnirestException("123"));
        assertEquals(-1, databaseModel.getIntfromServer("123"));
    }

    @Test
    public void testGetDoubleFromServer() {
        HttpResponse<String> httpString = mock(HttpResponse.class);
        when(getRequest.asString()).thenReturn(httpString);
        when(httpString.getBody()).thenReturn("25.555");
        assertEquals(25.555, databaseModel.getDoublefromserver("123"), 10);
    }

    @Test(expected = UnirestException.class)
    public void testGetDoubleFromServerBadRequest() {
        HttpResponse<String> httpString = mock(HttpResponse.class);
        when(getRequest.asString()).thenReturn(httpString);
        when(httpString.getBody()).thenThrow(new UnirestException("123"));
        assertEquals(-1, databaseModel.getDoublefromserver("123"), 10);
    }

    @Test
    public void testGetDoubleFromServerNegResult() {
        HttpResponse<String> httpString = mock(HttpResponse.class);
        when(getRequest.asString()).thenReturn(null);
        assertEquals(-1, databaseModel.getDoublefromserver("123"), 10);
    }

    @Test
    public void testGetDoubleFromUnirestEcxeption() {
        HttpResponse<String> httpString = mock(HttpResponse.class);
        when(getRequest.asString()).thenThrow(new UnirestException("123"));
        assertEquals(-1, databaseModel.getDoublefromserver("123"), 10);
    }


    @Test
    public void testJsonPost() {
        assertEquals(200, databaseModel.jsonPostRequest("123", new JSONObject()).getStatus());
    }

    @Test
    public void testJsonPostBadRequest() {
        when(Unirest.post(anyString())).thenThrow(new UnirestException("123"));
        assertNull(databaseModel.jsonPostRequest("123", new JSONObject()));
    }

    @Test
    public void testJsonPostNoBody() {
        assertEquals(200, databaseModel.jsonPostRequestNoBody("123").getStatus());
    }

    @Test
    public void testGetActivities() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(jsonObject);
        when(databaseModel.jsonRequest(anyString())).thenReturn(httpResponse);
        when(httpResponse.getStatus()).thenReturn(200);
        when(httpResponse.getBody()).thenReturn(value);
        when(value.getArray()).thenReturn(jsonArray);
        assertEquals(jsonArray, databaseModel.getActivites());
    }

    @Test
    public void testGetActivitiesBadRequest() {
        when(databaseModel.jsonRequest(anyString())).thenReturn(httpResponse);
        when(httpResponse.getStatus()).thenReturn(500);
        assertNull(databaseModel.getActivites());
    }

    @Test
    public void testGetActivitiesOfUser() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(jsonObject);
        when(databaseModel.jsonRequest(anyString())).thenReturn(httpResponse);
        when(httpResponse.getStatus()).thenReturn(200);
        when(httpResponse.getBody()).thenReturn(value);
        when(value.getArray()).thenReturn(jsonArray);
        assertEquals(jsonArray, databaseModel.getAllActivitesOfUser("123"));
    }

    @Test
    public void testGetActivitiesOfUserBadRequest() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(jsonObject);
        when(databaseModel.jsonRequest(anyString())).thenReturn(httpResponse);
        when(httpResponse.getStatus()).thenReturn(404);
        assertNull(databaseModel.getAllActivitesOfUser("123"));
    }

    @Test
    public void testRegisterUser() {
        assertTrue(databaseModel.registerUser(user1));
    }

    @Test
    public void testRegisterUserBadRequest() {
        when(httpResponse.getStatus()).thenReturn(300);
        assertFalse(databaseModel.registerUser(user1));
    }

    @Test
    public void testGetIdOfUserNotFound() {
        user1.setId(0);

        when(httpResponse.getStatus()).thenReturn(404);
        databaseModel.getIdOfUser(user1);
        assertEquals(0, user1.getId());
    }

    @Test
    public void testGetIdOfUserFound() {
        user1.setId(0);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", 42);
        when(httpResponse.getBody()).thenReturn(value);
        when(value.getObject()).thenReturn(jsonObject);
        databaseModel.getIdOfUser(user1);
        assertEquals(42, user1.getId());
    }

    @Test
    public void testGetIdOfBadRequest() {
        user1.setId(0);
        JSONObject jsonObject = new JSONObject();
        when(httpResponse.getBody()).thenReturn(value);
        when(value.getObject()).thenReturn(jsonObject);
        databaseModel.getIdOfUser(user1);
        assertEquals(0, user1.getId());
    }

    @Test
    public void testGetUsernameOfUser() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", "LoveTet");
        user1.setUsername(null);
        when(httpResponse.getBody()).thenReturn(value);
        when(value.getObject()).thenReturn(jsonObject);
        databaseModel.getUsernameOfUser(user1);
        assertEquals("LoveTet", user1.getUsername());
    }

    @Test
    public void testGetUsernameOfUserBadRequest() {
        user1.setUsername(null);
        when(httpResponse.getStatus()).thenReturn(300);
        databaseModel.getUsernameOfUser(user1);
        assertEquals(null, user1.getUsername());
    }

    @Test
    public void testGetUsernameOfUserEmptyJSON() {
        user1.setUsername(null);
        JSONObject jsonObject = new JSONObject();
        when(value.getObject()).thenReturn(jsonObject);
        databaseModel.getUsernameOfUser(user1);
        assertEquals(null, user1.getUsername());
    }

    @Test
    public void testAddFriend() {
        String friendToAdd = "LoveTesting";
        assertTrue(databaseModel.addFriend(user1, friendToAdd));
    }

    @Test
    public void testAddFriendFail() {
        String friendToAdd = "LoveTesting";
        when(httpResponse.getStatus()).thenReturn(404);
        assertFalse(databaseModel.addFriend(user1, friendToAdd));
    }

    @Test
    public void testGetFriendsListArrayList() {
        setterForGetFriends(jsonArray3, jsonObject5, jsonObject6, jsonObject7);
        ArrayList<User> userArrayList = databaseModel.getFriendsList(user1);
        assertEquals(1, userArrayList.size());
    }

    private void setterForGetFriends(JSONArray jsonArray3, JSONObject jsonObject5, JSONObject jsonObject6, JSONObject jsonObject7) {
        jsonObject7.put("username", "testtest");
        jsonObject7.put("totalPoints", 12);
        jsonObject7.put("totalKgCo2", 13.37);
        jsonObject7.put("id", 15);
        jsonObject6.put("user2", jsonObject7);
        jsonObject5.put("id", jsonObject6);
        jsonArray3.put(jsonObject5);
        when(value.getArray()).thenReturn(jsonArray3);
    }


    @Test
    public void testGetFriendsListPoints() {
        setterForGetFriends(jsonArray3, jsonObject5, jsonObject6, jsonObject7);
        ArrayList<User> userArrayList = databaseModel.getFriendsList(user1);
        assertEquals(12, userArrayList.get(0).getPoints());
    }

    @Test
    public void testGetFriendsListCo2() {
        setterForGetFriends(jsonArray3, jsonObject5, jsonObject6, jsonObject7);
        ArrayList<User> userArrayList = databaseModel.getFriendsList(user1);
        assertEquals(13.37, userArrayList.get(0).getCo2(), 10);
    }

    @Test
    public void testGetFriendsListid() {
        setterForGetFriends(jsonArray3, jsonObject5, jsonObject6, jsonObject7);
        ArrayList<User> userArrayList = databaseModel.getFriendsList(user1);
        assertEquals(15, userArrayList.get(0).getId());
    }

    @Test
    public void testGetFriendsListName() {
        setterForGetFriends(jsonArray3, jsonObject5, jsonObject6, jsonObject7);
        ArrayList<User> userArrayList = databaseModel.getFriendsList(user1);
        assertEquals("testtest", userArrayList.get(0).getUsername());
    }

    @Test
    public void testGetFriendsListNoPoints() {
        setterForGetFriends(jsonArray3, jsonObject5, jsonObject6, jsonObject7);
        jsonObject7.put("totalPoints", 0);
        ArrayList<User> userArrayList = databaseModel.getFriendsList(user1);
        assertEquals(0.0, userArrayList.get(0).getCo2(), 5);
    }

    @Test
    public void testGetAllActivitiesBadRequest() {
        when(httpResponse.getStatus()).thenReturn(404);
        assertNull(databaseModel.getAllActivities());
    }

    @Test
    public void testGetAllActivitiesID() {

        assertEquals(22, activityArrayList.get(0).getActivityId());
    }

    @Test
    public void testGetAllActivitiesName() {
        assertEquals("test1", activityArrayList.get(0).getActivityName());
    }

    @Test
    public void testGetAllActivitiesPoints() {
        assertEquals(1227, activityArrayList.get(0).getPoints());
    }

    @Test
    public void testGetAllActivitiesCo2() {
        assertEquals(12.27, activityArrayList.get(0).getCo2(), 10);
    }

    @Test
    public void testSendActivityToServer() {
        Activity activity = new Activity(null, 5);
        assertTrue(databaseModel.sendDoneActivityToServer(user1, activity));
    }

    @Test
    public void testSendActivityToServerBadRequest() {
        Activity activity = new Activity(null, 5);
        when(httpResponse.getStatus()).thenReturn(404);
        assertFalse(databaseModel.sendDoneActivityToServer(user1, activity));
    }


    @Test
    public void testDateParser() {
        String date = "2019-04-05T13:10:39.000";

        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        Date dateParsed = null;
        try {
            dateParsed = parser.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assertEquals(dateParsed, databaseModel.dateParser(date));
    }

    @Test
    public void testDateParserNull() {
        assertNull(databaseModel.dateParser(null));
    }

    @Test
    public void testDateParserBadDate() {
        String date = "2019-04-05K13:10:39.000+0000";
        assertNull(databaseModel.dateParser(date));
    }

    @Test
    public void testDelete() {
        assertEquals(200, databaseModel.jsonDeleteRequest("123").getStatus());
    }

    @Test
    public void testDeleteBadRequest() {
        when(httpResponse.getStatus()).thenReturn(404);
        assertEquals(404, databaseModel.jsonDeleteRequest("123").getStatus());
    }


    @Test
    public void testRemoveFriends() {
        User user2 = new User(null, null, 4);
        assertTrue(databaseModel.removeFriend(user1, user2));
    }

    @Test
    public void testRemoveFriendsBadRequest() {
        when(httpResponse.getStatus()).thenReturn(404);
        User user2 = new User(null, null, 4);
        assertFalse(databaseModel.removeFriend(user1, user2));
    }

    @Test
    public void testGetCO2ByUser() {
        HttpResponse<String> httpString = mock(HttpResponse.class);
        when(getRequest.asString()).thenReturn(httpString);
        when(httpString.getBody()).thenReturn("35.555");
        assertEquals(35.555, databaseModel.getCO2ByUser(2), 10);
    }

    @Test
    public void testGetPointsByUser() {
        HttpResponse<String> httpString = mock(HttpResponse.class);
        when(getRequest.asString()).thenReturn(httpString);
        when(httpString.getBody()).thenReturn("5567");
        assertEquals(5567, databaseModel.getPointsOfUser(13));
    }


    @Test
    public void testGetUserAchievementsArrayList() {

        helperMethodsForAchievements(jsonObject4, jsonObject5, jsonArray3);
        when(value.getArray()).thenReturn(jsonArray3);
        ArrayList<Achievement> achievementArrayList = databaseModel.getAchievements(user1);
        assertEquals(1, achievementArrayList.size());
    }

    private void helperMethodsForAchievements(JSONObject jsonObject4, JSONObject jsonObject5, JSONArray jsonArray3) {
        jsonObject4.put("id", 5);
        jsonObject4.put("name", "ILOVETESTING");
        jsonObject4.put("points", 578);
        jsonObject5.put("achievement", jsonObject4);
        jsonObject5.put("date", "2019-04-05T13:10:39.000");
        jsonArray3.put(jsonObject5);
    }

    private void helperGetAllAchievements(JSONObject jsonObject8, JSONObject jsonObject9, JSONArray jsonArray4) {

        jsonObject8.put("id", 99);
        jsonObject8.put("name", "Reach 50 points");
        jsonObject8.put("points", 100);
        jsonObject9.put("id", 25);
        jsonObject9.put("name", "Eat a vegetarian meal");
        jsonObject9.put("points", 69);
        jsonArray4.put(jsonObject8);
        jsonArray4.put(jsonObject9);
    }

    @Test
    public void testGetUserAchievementsId() {
        helperMethodsForAchievements(jsonObject4, jsonObject5, jsonArray3);
        when(value.getArray()).thenReturn(jsonArray3);
        ArrayList<Achievement> achievementArrayList = databaseModel.getAchievements(user1);
        assertEquals(5, achievementArrayList.get(0).getId());
    }

    @Test
    public void testGetUserAchievementsName() {
        helperMethodsForAchievements(jsonObject4, jsonObject5, jsonArray3);
        when(value.getArray()).thenReturn(jsonArray3);
        ArrayList<Achievement> achievementArrayList = databaseModel.getAchievements(user1);
        assertEquals("ILOVETESTING", achievementArrayList.get(0).getName());
    }

    @Test
    public void testGetUserAchievementsPoints() {
        helperMethodsForAchievements(jsonObject4, jsonObject5, jsonArray3);
        when(value.getArray()).thenReturn(jsonArray3);
        ArrayList<Achievement> achievementArrayList = databaseModel.getAchievements(user1);
        assertEquals(578, achievementArrayList.get(0).getPoints());
    }

    @Test
    public void testGetUserAchievementsBadRequest() {
        helperMethodsForAchievements(jsonObject4, jsonObject5, jsonArray3);
        when(httpResponse.getStatus()).thenReturn(404);
        ArrayList<Achievement> achievementArrayList = databaseModel.getAchievements(user1);
        assertEquals(0, achievementArrayList.size());
    }

    User user = new User("123", "321", 2);
    //To Continure after patch request testing

    @Test
    public void testGetAllAchievementsBadRequest() {
        when(httpResponse.getStatus()).thenReturn(404);
        assertNull(databaseModel.getAllAchievements());
    }

    @Test
    public void testGetAllAchievementsId() {
        helperGetAllAchievements(jsonObject8, jsonObject9, jsonArray4);
        when(value.getArray()).thenReturn(jsonArray4);
        ArrayList<Achievement> achievementArrayList = databaseModel.getAllAchievements();
        assertEquals(99, achievementArrayList.get(0).getId());
    }

    @Test
    public void testGetAllAchievementsName() {
        helperGetAllAchievements(jsonObject8, jsonObject9, jsonArray4);
        when(value.getArray()).thenReturn(jsonArray4);
        ArrayList<Achievement> achievementArrayList = databaseModel.getAllAchievements();
        assertEquals("Eat a vegetarian meal", achievementArrayList.get(1).getName());
        assertEquals("Reach 50 points", achievementArrayList.get(0).getName());
    }

    @Test
    public void testGetAllAchievementsPoints() {
        helperGetAllAchievements(jsonObject8, jsonObject9, jsonArray4);
        when(value.getArray()).thenReturn(jsonArray4);
        ArrayList<Achievement> achievementArrayList = databaseModel.getAllAchievements();
        assertEquals(69, achievementArrayList.get(1).getPoints());
        assertNotEquals(420, achievementArrayList.get(0).getPoints());
    }

    @Test
    public void testAddAchievementUser() {
        Achievement achievement = new Achievement("VinDiesel", 99, 6969);
        assertTrue(databaseModel.addAchievementUser(user1.getId(), achievement.getId()));
    }

    @Test
    public void testAddAchievementNull() {
        Achievement achievement = new Achievement(null, 0, 100);
        when(httpResponse.getStatus()).thenReturn(404);
        assertFalse(databaseModel.addAchievementUser(user1.getId(),achievement.getId()));
    }
}