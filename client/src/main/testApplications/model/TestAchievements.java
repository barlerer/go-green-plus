package model;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class TestAchievements {

    User user;
    Achievement achievement;
    Achievement achievementNoUser;
    Achievement achievement1;
    Achievement achievement2;
    Achievement achievement3;
    Achievement achievement4;
    Achievement achievementDate;
    Date date1 = new Date(1000);
    Date date2 = new Date(100000);

    @Before
    public void setUp() {

        user = new User("IloveTesting", "TestingRock67", 13);
        achievement = new Achievement(user, "TestName", 42, 420);
        achievementNoUser = new Achievement("LoveTesting", 12, 1223);
        achievement1 = new Achievement(user, "TestNAame", 42, 420);
        achievement2 = new Achievement(user, "TestName", 420, 420);
        achievement3 = new Achievement(user, "TestName", 42, 4220);
        achievement4 = new Achievement(user, "TestName", 42, 420);
        achievementDate = new Achievement(user, "testDate",56,69, date1);
    }

    @Test
    public void testConstructorNoUser() {
        assertEquals(12, achievementNoUser.getId());
        assertEquals(1223, achievementNoUser.points);
        assertEquals("LoveTesting", achievementNoUser.getName());
        assertNull(achievementNoUser.getUser());
    }

    @Test
    public void testGetterForUser() {
        assertEquals(user.getId(), achievement.getUser().getId());
    }

    @Test
    public void testGetterForName() {
        assertEquals("TestName", achievement.getName());
    }

    @Test
    public void testGetterForId() {
        assertEquals(42, achievement.getId());
    }

    @Test
    public void testGetterForPoints() {
        assertEquals(420, achievement.getPoints());
    }

    @Test
    public void testSetterUser() {
        User user1 = new User(null, null, 9);
        achievement.setUser(user1);
        assertEquals(9, achievement.getUser().getId());
    }

    @Test
    public void testSetterPoints() {
        achievement.setPoints(421);
        assertEquals(421, achievement.getPoints());
    }

    @Test
    public void testSetterId() {
        achievement.setId(12);
        assertEquals(12, achievement.getId());
    }

    @Test
    public void testSetterName() {
        achievement.setName("TUDelft_Student");
        assertEquals("TUDelft_Student", achievement.getName());
    }

    @Test
    public void testEqualsSame() {
        assertTrue(achievement.equals(achievement));
    }

    @Test
    public void testEqualsDifferent() {
        assertFalse(achievement.equals(achievementNoUser));
    }

    @Test
    public void testEquals() {
        assertTrue(achievement.equals(achievement4));
    }

    @Test
    public void testEqualsDifferentName() {
        assertFalse(achievement.equals(achievement1));
    }

    @Test
    public void testEqualsDifferentId() {
        assertFalse(achievement.equals(achievement2));
    }

    @Test
    public void testEqualsDifferentPoints() {
        assertFalse(achievement.equals(achievement3));
    }

    @Test
    public void testEqualsNullParams() {
        assertFalse(achievement.equals(null));
    }

    @Test
    public void testEqualsDiffClass() {
        assertFalse(achievement.equals(user));
    }

    @Test
    public void testSetDate() {

        achievementDate.setDate(date2);
        assertEquals(date2,achievementDate.getDate());
        assertNotEquals(date1,achievementDate.getDate());
    }

    @Test
    public void testGetDate() {

        assertEquals(date1,achievementDate.getDate());
        assertNotEquals(date2,achievementDate.getDate());
    }

    @Test
    public void testConstructorDate() {

        assertEquals(achievementDate.getUser(),user);
        assertEquals(achievementDate.getId(),56);
        assertEquals(achievementDate.getName(),"testDate");
        assertEquals(achievementDate.getPoints(),69);
        assertEquals(achievementDate.getDate(),date1);
        assertNotEquals(achievementDate.getDate(),date2);
    }

}
