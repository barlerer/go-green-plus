package model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestUser {

    User user;
    User user2;
    User user3;
    User user4;
    User user8;
    String password;
    String password2;
    String username;
    String username2;
    @Before
    public void setUp(){
        password = "123456";
        username = "Barlerer";
        password2 = "1234567";
        username2 = "Ba1lerer";
        user=new User(password,username);
        user2=new User(password2,username);
        user3=new User(password,username2);
        user4=new User(password,username);
        user8 = new User(5);
    }

    @Test
    public void testConstructor(){
        assertTrue(user.getUsername().equals(username)&&user.getPassword().equals(password));
    }

    @Test
    public void testConstructorBadCred(){
        assertFalse(user.getUsername().equals(username2)&&user.getPassword().equals(password));
        assertFalse(user.getUsername().equals(username)&&user.getPassword().equals(password2));
    }

    @Test
    public void testGetterUsername(){
        assertTrue(user2.getUsername().equals(username));
        assertFalse(user2.getUsername().equals("Admin"));
    }

    @Test
    public void testGetterPassword(){
        assertTrue(user3.getPassword().equals(password));
        assertFalse(user3.getPassword().equals("Password"));
    }

    @Test
    public void testSetterPassword(){
        String newPass = "ILoveTesting";
        user4.setPassword(newPass);
        assertTrue(user4.getPassword().equals(newPass));
        assertFalse(user4.getPassword().equals(password));
    }

    @Test
    public void testSetterUsername(){
        String newUsername = "ILoveTestingVeryMuch";
        user.setUsername(newUsername);
        assertTrue(user.getUsername().equals(newUsername));
        assertFalse(user.getUsername().equals(username));
    }


    @Test
    public void noIdConstructor(){
        assertEquals(user3.getId(), 0);
    }

    @Test
    public void IdConstructor(){
        User user5 = new User(password, username, 8);
        assertEquals(user5.getId(), 8);
    }

    @Test
    public void SetterForId(){
        user3.setId(17);
        assertEquals(17, user3.getId());
    }

    @Test
    public void onlyIdConstructor(){

        assertEquals(5, user8.getId());
    }

    @Test
    public void testSetterPoints() {
        user8.setPoints(500);
        assertEquals(500, user8.getPoints());
    }

    @Test
    public void testSetterCO2() {
        user8.setCo2(16.66);
        assertEquals(16.66, user8.getCo2(), 5);
    }

    @Test
    public void testGetterCO2() {
        assertEquals(0.0, user.getCo2(), 5);

    }

}
