package client.gui;


import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import controller.Controller;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import model.User;


import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;


public class FriendsController {

    @FXML
    private Label userName;
    @FXML
    private JFXTextField removeText;
    @FXML
    private JFXButton removeButton;
    @FXML
    private Label warningLabel;
    @FXML
    private TableColumn<User, String> nameCol;
    @FXML
    private TableColumn<User, Integer>  pointCol;
    @FXML
    private TableColumn<User, Double> carbonCol;
    @FXML
    private TableView<User> friendsList;
    @FXML
    private TextField friendLabel;
    @FXML
    private Button addFriend;
    @FXML
    private Button backButton;
    @FXML
    private GridPane friendsPane;

    private  Controller controller = new Controller();
    private int ok = 0;

    @FXML
    private void handleBackToMenu(ActionEvent actionEvent) throws IOException {

        URL url = new File("client/src/main/java/client/gui/GuiX.fxml").toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);

    }

    @FXML
    private void handleAddFriend(ActionEvent actionEvent) {

        String friend = friendLabel.getText();
        String friendLower = friend.toLowerCase();

        if ( friendLower.equals(LoginController.getUsername().toLowerCase())) {
            warningLabel.setText("Sorry! You can't add yourself as a friend.");
            warningLabel.visibleProperty().setValue(true);
        } else {

            warningLabel.visibleProperty().setValue(false);
            controller.addFriend(LoginController.getLoggedUser(), friend);

            if (controller.addFriend(LoginController.getLoggedUser(),friend)) {
                System.out.println("friend added successfully!");
                friendLabel.clear();
                friendsList.refresh();

            } else {
                warningLabel.setText("There was an error in adding the user.");
                warningLabel.visibleProperty().setValue(true);

                System.out.println("There was an error in adding the user.");
            }
        }

    }

    @FXML
    private void displayFriends(MouseEvent mouseEvent) {

        ArrayList<User> friends = controller.getFriends(LoginController.getLoggedUser());

        if (!friends.isEmpty()) {

            if ( ok == 0) {
                nameCol.setCellValueFactory(new PropertyValueFactory<>("username"));
                pointCol.setCellValueFactory(new PropertyValueFactory<>("points"));
                carbonCol.setCellValueFactory(new PropertyValueFactory<>("co2"));

                friendsList.setItems(getObsList(friends));


            }
        }

        ok++;
    }

    private ObservableList<User> getObsList(ArrayList<User> userList) {

        ObservableList<User> friendsList  = FXCollections.observableArrayList();
        friendsList.addAll(userList);

        for (User user: userList) {
            System.out.println(user.getCo2() + " " + user.getPoints() + "\n");
        }

        return friendsList;
    }

    @FXML
    private void handleRemoveFriend(ActionEvent actionEvent) throws IOException {

        ArrayList<Integer> friendsId = new ArrayList<>();
        ArrayList<User> friends = controller.getFriends(LoginController.getLoggedUser());
        for (User user: friends
             ) {
            friendsId.add(user.getId());
        }

        String removed = removeText.getText();

        User user = LoginController.getLoggedUser();
        User removeUser = new User("password", removed);

        controller.fillDataFromServer(removeUser);

        boolean isFriend = friendsId.contains(removeUser.getId());
        System.out.println(isFriend);


        if (!removed.isEmpty()) {

            if (isFriend) {

                boolean check = controller.removeFriend(user,removeUser);

                if (check ) {
                    System.out.println("friend removed successfully");

                    String path = "client/src/main/java/client/gui/GuiX.fxml";
                    URL url = new File(path).toURL();
                    Parent pane = FXMLLoader.load(url);

                    Gui.primaryStage.getScene().setRoot(pane);
                }  else {
                    System.out.println("something went wrong");
                }
            } else {

                System.out.println("The user is not in your friends list");
            }
        }
    }

    @FXML
    private void displayName(MouseEvent mouseEvent) {

        userName.setText(LoginController.getUsername());
    }
}
