package client.gui;

import controller.Controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import model.Activity;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.function.UnaryOperator;

public class ActivityController {


    @FXML
    public GridPane activityPane;
    @FXML
    private SplitMenuButton chooseButton;
    @FXML
    private MenuItem solarPanel;
    @FXML
    private Label totalEmissions;
    @FXML
    private Label degreeLabel;
    @FXML
    private Label tempLabel;
    @FXML
    private Label kilometerLabel;
    @FXML
    private TextField kilometerText;
    @FXML
    private Label totalPointsLabel;
    @FXML
    private Label emissionsLabel;
    @FXML
    private Label carbonPointLabel;

    private Controller controller = new Controller();

    private Activity userActivity;

    private ArrayList<Activity> activities;

    private int counter = 0;

    private UnaryOperator<TextFormatter.Change> filter  = change -> {
        String text = change.getText();

        if (text.matches("[0-9]*")) {
            return change;
        }
        return null;
    };



    @FXML
    private void formatText() {
        TextFormatter<String> textFormatter = new TextFormatter<>(filter);
        kilometerText.setTextFormatter(textFormatter);

    }

    @FXML
    private void handleToStart(ActionEvent actionEvent) throws IOException {
        String path = "client/src/main/java/client/gui/GuiX.fxml";
        URL url = new File(path).toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);
    }

    @FXML
    private void activityVegetarianMeal(ActionEvent actionEvent) throws IOException {
        hideFields();
        hideTemp();

        Activity vegetarianMeal = activities.get(0);
        userActivity = vegetarianMeal;

        carbonPointLabel.setText("" +  vegetarianMeal.getPoints());
        emissionsLabel.setText("" + vegetarianMeal.getCo2());
        chooseButton.setText("Eat a vegetarian meal");

    }

    @FXML
    private void handleConfirm(ActionEvent actionEvent) throws IOException {

        if (userActivity.getActivityId() == 1 || userActivity.getActivityId() == 2) {
            if ( controller.sendDoneActivity(LoginController.getLoggedUser(), userActivity)) {
                System.out.println("Activity registered successfully.");
            } else {
                System.out.println("Error in sending activity to server.");
            }
        }  else {

            if ( !kilometerText.getText().equals("")) {

                String text = kilometerText.getText();
                int kilometers = Integer.parseInt(text);
                userActivity.setQuantity(kilometers);
            }

            if ( controller.sendDoneActivity(LoginController.getLoggedUser(), userActivity)) {
                System.out.println("Activity registered successfully.");
            } else {
                System.out.println("Error in sending activity to server.");
            }
        }


        String path = "client/src/main/java/client/gui/GuiX.fxml";
        URL url = new File(path).toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);

    }


    /**
     * Displays the user's total points on the screen through a label.
     * @param mouseEvent event.
     */
    public void handleTotalPoints(MouseEvent mouseEvent) {

        if (LoginController.getId() != 0) {

            int totalPoints  =  controller.getPointsOfUser(LoginController.getId());
            double totalCO2 = controller.getCO2ByUser(LoginController.getId());

            totalPointsLabel.setText("" + totalPoints );
            totalEmissions.setText("" + totalCO2 + " Kg of CO2");
        } else {
            totalPointsLabel.setText("");
            totalEmissions.setText("");
        }

        activities = controller.getActivities();

        if ( counter == 0) {
            formatText();
        }

        counter++;
    }

    @FXML
    private void handlePublicTrans(ActionEvent actionEvent) {
        displayFields();
        hideTemp();
        kilometerText.setPromptText("How many KM you travelled");

        Activity publicTransport = activities.get(3);
        userActivity = publicTransport;

        carbonPointLabel.setText("" +  publicTransport.getPoints());
        emissionsLabel.setText("" + publicTransport.getCo2());
        chooseButton.setText("Travel by public transport");

    }

    @FXML
    private void handleProduce(ActionEvent actionEvent) {
        hideFields();
        hideTemp();

        Activity produce = activities.get(1);
        userActivity = produce;

        carbonPointLabel.setText("" +  produce.getPoints());
        emissionsLabel.setText("" + produce.getCo2());
        chooseButton.setText("Buy local produce");
    }

    @FXML
    private void handleBicycle(ActionEvent actionEvent) {
        displayFields();
        hideTemp();
        kilometerText.setPromptText("How many KM you travelled");

        Activity bicycle = activities.get(2);
        userActivity = bicycle;

        carbonPointLabel.setText("" +  bicycle.getPoints());
        emissionsLabel.setText("" + bicycle.getCo2());
        chooseButton.setText("Travel by bicycle");
    }

    @FXML
    private void displayFields() {
        if ( !kilometerText.isVisible() ) {
            kilometerText.visibleProperty().setValue(true);
        }

        if ( !kilometerLabel.isVisible()) {
            kilometerLabel.visibleProperty().setValue(true);
        }
    }

    @FXML
    private void hideFields() {
        if (kilometerText.isVisible()) {
            kilometerText.visibleProperty().setValue(false);
        }

        if (kilometerLabel.isVisible()) {
            kilometerLabel.visibleProperty().setValue(false);
        }
    }

    @FXML
    private void hideTemp() {
        if (tempLabel.isVisible()) {
            tempLabel.visibleProperty().setValue(false);
        }

        if (degreeLabel.isVisible()) {
            degreeLabel.visibleProperty().setValue(false);
        }
    }

    @FXML
    private void displayTemp() {
        if (!tempLabel.isVisible()) {
            tempLabel.visibleProperty().setValue(true);
        }

        if (!degreeLabel.isVisible()) {
            degreeLabel.visibleProperty().setValue(true);
        }
    }

    @FXML
    private void handleGetActivities(MouseEvent mouseEvent) {
    }

    @FXML
    private void reduceTemperature(ActionEvent actionEvent) {
        hideFields();
        displayTemp();
        kilometerText.visibleProperty().setValue(true);
        kilometerText.setPromptText("The temp in degrees Celsius");

        Activity lowerTemp = activities.get(5);
        userActivity = lowerTemp;

        carbonPointLabel.setText("" +  lowerTemp.getPoints());
        emissionsLabel.setText("" + lowerTemp.getCo2());
        chooseButton.setText("Reduce the temperature in your home");
    }

    @FXML
    private void handleSolarPanel(ActionEvent actionEvent) {
        hideFields();
        hideTemp();

        Activity installPanels = activities.get(4);
        userActivity = installPanels;

        carbonPointLabel.setText("" + installPanels.getPoints());
        emissionsLabel.setText("" + installPanels.getPoints());
        chooseButton.setText("Installs solar panels");
    }
}
