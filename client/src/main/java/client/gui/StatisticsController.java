package client.gui;

import controller.Controller;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import model.User;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class StatisticsController {

    Controller controller = new Controller();
    ArrayList<User> userList = new ArrayList<>();

    @FXML
    private PieChart chart;
    @FXML
    private Label statisticLabel;
    @FXML
    private TableView<User> users;
    @FXML
    private TableColumn<User, String> userColumn;
    @FXML
    private TableColumn<User, Integer> idColumn;
    @FXML
    private TextField userField;

    @FXML
    private void handleToStart(ActionEvent actionEvent) throws IOException {
        String path = "client/src/main/java/client/gui/GuiX.fxml";
        URL url = new File(path).toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);
    }

    @FXML
    private void handleAddUser(ActionEvent actionEvent) throws NumberFormatException {

        String userString = userField.getText();

        try {
            if (userString != null) {

                userField.clear();
                users.refresh();

                User user = createUser(userString);
                addUserToArray(user);

                ObservableList<User> observableList = FXCollections.observableArrayList(userList);
                idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
                userColumn.setCellValueFactory(new PropertyValueFactory<>("username"));
                users.setItems(observableList);

                handlePoints(null);
            }
        } catch (NullPointerException E) {
            System.out.println("User does not exist");
        }
    }

    /**
     * Removes the highlighted user from the table.
     * @param actionEvent Nothing lol
     */
    @FXML
    public void handleRemoveUser(ActionEvent actionEvent) {
        ObservableList<User> selected = users.getSelectionModel().getSelectedItems();

        if (selected.get(0) != null) {
            userList.remove(selected.get(0));

            ObservableList<User> data = FXCollections.observableArrayList(userList);
            users.setItems(data);

            handlePoints(null);
        }
    }

    @FXML
    private void handlePoints(ActionEvent actionEvent) {
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

        for (User user : userList) {
            PieChart.Data data = new PieChart.Data(
                    user.getUsername() + " - " + controller.countPoints(user.getId()),
                    controller.countPoints(user.getId())
            );
            pieChartData.add(data);
        }

        chart.setData(pieChartData);
        statisticLabel.setText("Points");
        chartSetUp();
    }

    @FXML
    private void handleEmissions(ActionEvent actionEvent) {
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

        for (User user : userList) {
            double carbon = Math.round(controller.countCarbon(user.getId()));
            PieChart.Data data = new PieChart.Data(
                    user.getUsername() + " - " + carbon,
                    carbon
            );
            pieChartData.add(data);
        }

        chart.setData(pieChartData);
        statisticLabel.setText("CO2 Emissions Saved");
        chartSetUp();
    }

    @FXML
    private void handleActivities(ActionEvent actionEvent) {
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

        for (User user : userList) {
            PieChart.Data data = new PieChart.Data(
                    user.getUsername() + " - " + controller.countActivity(user.getId()),
                    controller.countActivity(user.getId())
            );
            pieChartData.add(data);
        }

        chart.setData(pieChartData);
        statisticLabel.setText("Total Activities");
        chartSetUp();
    }

    @FXML
    private void handleProduce(ActionEvent actionEvent) {
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

        for (User user : userList) {
            PieChart.Data data = new PieChart.Data(
                    user.getUsername() + " - " + controller.countProduce(user.getId()),
                    controller.countProduce(user.getId())
            );
            pieChartData.add(data);
        }

        chart.setData(pieChartData);
        statisticLabel.setText("Local Produce");
        chartSetUp();
    }

    @FXML
    private void handleBike(ActionEvent actionEvent) {
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

        for (User user : userList) {
            PieChart.Data data = new PieChart.Data(
                    user.getUsername() + " - " + controller.countBike(user.getId()),
                    controller.countBike(user.getId())
            );
            pieChartData.add(data);
        }

        chart.setData(pieChartData);
        statisticLabel.setText("Bicycle");
        chartSetUp();
    }

    @FXML
    private void handlePublicTransport(ActionEvent actionEvent) {
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

        for (User user : userList) {
            PieChart.Data data = new PieChart.Data(
                    user.getUsername() + " - " + controller.countPublicTransport(user.getId()),
                    controller.countPublicTransport(user.getId())
            );
            pieChartData.add(data);
        }

        chart.setData(pieChartData);
        statisticLabel.setText("Public Transportation");
        chartSetUp();
    }

    @FXML
    private void handleVeg(ActionEvent actionEvent) {
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

        for (User user : userList) {
            PieChart.Data data = new PieChart.Data(
                    user.getUsername() + " - " + controller.countVegetarianMeal(user.getId()),
                    controller.countVegetarianMeal(user.getId())
            );
            pieChartData.add(data);
        }

        chart.setData(pieChartData);
        statisticLabel.setText("Vegetarian Meal");
        chartSetUp();
    }

    /**
     * Returns a User with data from the database.
     * @param userName The username of the User
     * @return A User with data from the database
     */
    public User createUser(String userName) {
        User user = new User(null, userName);
        controller.fillDataFromServer(user);

        return user;
    }

    /**
     * Simplifies adding user to the ArrayList.
     * @param user The user to add
     */
    public void addUserToArray(User user) {
        if (user == null) {
            throw new IllegalArgumentException();
        }
        userList.add(user);
    }

    /**
     * Avoids chart setup repetition.
     */
    @FXML
    public void chartSetUp() {
        chart.setClockwise(true);
        chart.setLabelLineLength(20);
        chart.setLabelsVisible(true);
        chart.setStartAngle(180);
    }

}