package client.gui;

import controller.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;


public class Gui extends Application {
    public static Stage primaryStage;
    
    private Controller guiController = new Controller();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        URL url = new File("client/src/main/java/client/gui/LoginScreen.fxml").toURL();
        Parent root = FXMLLoader.load(url);

        primaryStage.setTitle("GoGreenPlus Application");
        primaryStage.setScene(new Scene(root,1020,660));
        primaryStage.show();

    }

    @Override
    public void stop() {
        System.out.println("App is closing");
    }

    /**
     * Method which changes scenes.
     * @param fxml fxml file which the scene changes to.
     * @throws IOException when the file is not found.
     */
    public void changeScene(String fxml) throws IOException {
        Parent pane = FXMLLoader.load(getClass().getResource(fxml));

        primaryStage.getScene().setRoot(pane);
    }

}
