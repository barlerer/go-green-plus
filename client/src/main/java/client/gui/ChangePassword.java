package client.gui;


import controller.Controller;
import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import model.User;

import java.io.File;
import java.io.IOException;
import java.net.URL;


public class ChangePassword {
    @FXML
    private Label errorLabel;
    @FXML
    private Label goodChange;
    @FXML
    private TextField userText;
    @FXML
    private Label warningLabel;
    @FXML
    private Button changeButton;
    @FXML
    private PasswordField repeatPassword;
    @FXML
    private PasswordField newPassword;
    @FXML
    private PasswordField currentPassword;
    @FXML
    private Button backButton;

    private Controller controller = new Controller();

    @FXML
    private void handleBackToMenu(ActionEvent actionEvent) throws IOException {
        String path = "client/src/main/java/client/gui/LoginScreen.fxml";
        URL url = new File(path).toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);
    }


    @FXML
    private void confirmChange(ActionEvent actionEvent) throws IOException {
        System.out.println("Change confirmed!");

        String username = userText.getText();
        String pass = currentPassword.getText();
        String newPass = newPassword.getText();
        String repeatPass = repeatPassword.getText();

        if (newPass.equals(repeatPass)) {

            warningLabel.visibleProperty().setValue(false);

            User user = new User(pass, username);
            controller.fillDataFromServer(user);

            boolean change = controller.changePassword(user, newPass);

            if (change) {
                goodChange.visibleProperty().setValue(true);
                errorLabel.visibleProperty().setValue(false);
                System.out.println( " The password has been changed!");

                PauseTransition delay = new PauseTransition(Duration.seconds(1.2));

                delay.setOnFinished( ae -> {
                    try {
                        displayLogin();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                delay.play();

            } else {
                errorLabel.visibleProperty().setValue(true);
                System.out.println(" Username or password incorrect!");

            }
        } else {

            System.out.println(" The passwords are not matching.");
            errorLabel.visibleProperty().setValue(false);
            warningLabel.visibleProperty().setValue(true);
            goodChange.visibleProperty().setValue(false);
        }
    }

    @FXML
    private void handleCheck(MouseEvent mouseEvent) {
        String user = userText.getText();
        String userPass = currentPassword.getText();
        String newPass = newPassword.getText();
        String repeat = repeatPassword.getText();

        boolean val = userPass.isEmpty() || newPass.isEmpty() || repeat.isEmpty() || user.isEmpty();

        if ( changeButton.isDisable()) {
            if (!val) {
                changeButton.setDisable(false);
            }
        } else if ( !changeButton.isDisable()) {
            if (val) {
                changeButton.setDisable(true);
            }
        }
    }

    @FXML
    private static void displayLogin() throws IOException {
        String path = "client/src/main/java/client/gui/LoginScreen.fxml";
        URL url = new File(path).toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);
    }

}
