package client.gui;

import com.jfoenix.controls.JFXProgressBar;
import controller.Controller;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import model.Achievement;
import model.Activity;
import model.User;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class AchievementsController {

    @FXML
    private FontAwesomeIconView pointTwo;
    @FXML
    private FontAwesomeIconView pointThree;
    @FXML
    private FontAwesomeIconView pointFour;
    @FXML
    private FontAwesomeIconView pointFive;
    @FXML
    private FontAwesomeIconView activityOne;
    @FXML
    private FontAwesomeIconView activityTwo;
    @FXML
    private FontAwesomeIconView carbonOne;
    @FXML
    private FontAwesomeIconView carbonTwo;
    @FXML
    private FontAwesomeIconView carbonThree;
    @FXML
    private FontAwesomeIconView carbonFour;
    @FXML
    private FontAwesomeIconView transOne;
    @FXML
    private FontAwesomeIconView transTwo;
    @FXML
    private FontAwesomeIconView transThree;
    @FXML
    private FontAwesomeIconView transFour;
    @FXML
    private FontAwesomeIconView vegOne;
    @FXML
    private FontAwesomeIconView vegTwo;
    @FXML
    private FontAwesomeIconView vegThree;
    @FXML
    private FontAwesomeIconView produceOne;
    @FXML
    private FontAwesomeIconView produceTwo;
    @FXML
    private FontAwesomeIconView produceThree;
    @FXML
    private FontAwesomeIconView bikeOne;
    @FXML
    private FontAwesomeIconView bikeTwo;
    @FXML
    private FontAwesomeIconView bikeThree;
    @FXML
    private FontAwesomeIconView bikeFour;
    @FXML
    private FontAwesomeIconView friendOne;
    @FXML
    private FontAwesomeIconView friendTwo;
    @FXML
    private FontAwesomeIconView friendThree;
    @FXML
    private FontAwesomeIconView friendFour;
    @FXML
    private Label percentage;
    @FXML
    private JFXProgressBar percentageBar;
    @FXML
    private FontAwesomeIconView pointOne;


    private Controller controller  = new Controller();
    private double totalCo2 = 0.0;
    private int counter = 0;
    private ArrayList<Achievement> userAchievements = new ArrayList<>();

    @FXML
    private void handleBackToMenu(ActionEvent actionEvent) throws IOException {
        String path = "client/src/main/java/client/gui/GuiX.fxml";
        URL url = new File(path).toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);
    }

    @FXML
    private void handleGetActivities(MouseEvent mouseEvent) {
        if ( counter == 0) {
            counter++;
            ArrayList<Activity> activities = new ArrayList<>();
            activities = controller.listActivities(LoginController.getId());


            for (Activity act: activities) {

                System.out.println( act.getActivityId() + " "
                        + act.getActivityName() + " "
                        + act.getCo2() + " "
                        + act.getPoints() + " "
                        + act.getQuantity());

                totalCo2 += act.getCo2() * act.getQuantity();

            }

            User user = createUser(LoginController.getId());
            userAchievements = controller.getAchievements(user);

            int produceCounter = controller.countProduce(LoginController.getId());
            System.out.println("Buying local produce " + produceCounter + " times");

            int publicCounter = controller .countPublicTransport(LoginController.getId());
            System.out.println("Distance travelled by public transport " + publicCounter);

            int bikeCounter = controller.countBike(LoginController.getId());
            System.out.println("Distance traveled by bike " + bikeCounter);

            int pointCounter = controller.countPoints(LoginController.getId());
            System.out.println("Points accumulated so far " + pointCounter);

            int vegetarianmealCounter = controller.countVegetarianMeal(LoginController.getId());
            System.out.println("Ate a vegetarian meal " + vegetarianmealCounter + " times");

            double carbonCounter = controller.countCarbon(LoginController.getId());
            System.out.println("Total Co2 saved " + carbonCounter + " Kg");

            checkLocalProduce();
            checkPublicTransport();
            checkBike();
            checkPoints();
            checkVegetarianMeal();
            checkCarbon();
            checkActivity();
            countFriends();//throws error if the user doesn't have any friends.
            displayAchievements(LoginController.getId());

        } else if (counter > 0) {
            counter++;
        }

    }

    /**
     * Method to create a user object, if the id is given.
     *
     * @param userId The id of the user.
     * @return An user object.
     */
    @FXML
    public User createUser( int userId) {
        User user = new User(userId);
        controller.fillDataFromServer(user);

        user.setPoints(controller.getPointsOfUser(LoginController.getId()));
        user.setCo2(controller.getCO2ByUser(LoginController.getId()));

        return user;
    }

    /**
     * Method to check for local produce achievements.
     * It gets the arraylist of the user's achievements thus far,
     * And stores the achievements related to local produce in an arraylist
     * Called localProduce.
     * Then if the condition of one achievement is met, it checks in local produce
     * to see if that particular achievement has already been reached.
     * If it has not been reached yet, then it will send the achievement to the server.
     * Else, if the achievement has been already reached, it doesn't send anything.
     */
    @FXML
    public void checkLocalProduce() {

        boolean firstProduce = false;
        boolean tenProduce = false;
        boolean twentyProduce = false;

        ArrayList<Achievement> localProduce = new ArrayList<>();

        for (Achievement achievement : userAchievements) {
            if (achievement.getName().contains("produce")) {
                localProduce.add(achievement);
            }
        }

        for (Achievement achievement : localProduce) {
            if (achievement.getId() == 28) {
                firstProduce = true;
            } else if (achievement.getId() == 29) {
                tenProduce = true;
            } else if (achievement.getId() == 30) {
                twentyProduce = true;
            }
        }

        checkLocalProduceCont(firstProduce, tenProduce, twentyProduce);
    }

    /**
     * Continues from the previous method. Used to reduce cyclomatic complexity.
     * @param firstProduce If one local produce activity has been done
     * @param tenProduce If ten local produce activities has been done
     * @param twentyProduce If twenty local produce activities has been done
     */
    @FXML
    public void checkLocalProduceCont(boolean firstProduce,
                                      boolean tenProduce, boolean twentyProduce) {

        if ( controller.countProduce(LoginController.getId()) >= 1) {
            if (!firstProduce) {
                controller.addAchievementUser(LoginController.getId(), 28);
            }
            produceOne.setFill(Color.GREEN);
        }

        if (controller.countProduce(LoginController.getId()) >= 10) {
            if (!tenProduce) {
                controller.addAchievementUser(LoginController.getId(), 29);
            }
            produceTwo.setFill(Color.GREEN);
        }

        if (controller.countProduce(LoginController.getId()) >= 20) {
            if (!twentyProduce) {
                controller.addAchievementUser(LoginController.getId(), 30);
            }
            produceThree.setFill(Color.GREEN);
        }
    }

    /**
     * This method checks for public transport achievements.
     */
    @FXML
    public void checkPublicTransport() {

        boolean tenKms = false;
        boolean fiftyKms = false;
        boolean hundredKms = false;
        boolean thousandKms = false;
        int userId = LoginController.getId();

        ArrayList<Achievement> publicTransport = new ArrayList<>();

        for (Achievement achievement : userAchievements) {
            if (achievement.getName().contains("transport")) {
                publicTransport.add(achievement);
            }
        }

        for (Achievement achievement : publicTransport) {
            if (achievement.getId() == 16) {
                tenKms = true;
            } else if (achievement.getId() == 17) {
                fiftyKms = true;
            } else if (achievement.getId() == 18) {
                hundredKms = true;
            } else if (achievement.getId() == 19) {
                thousandKms = true;
            }

        }
        checkPublicTransportCont(userId, tenKms, fiftyKms,
                hundredKms, thousandKms);
    }

    /**
     * Continues from the previous method. Used to reduce cyclomatic complexity.
     * @param userId The user
     * @param tenKms If ten kilometers has been traveled
     * @param fiftyKms If fifty kilometers has been traveled
     * @param hundredKms If a hundred kilometers has been traveled
     * @param thousandKms If a thousand kilometers has been traveled
     */
    @FXML
    public void checkPublicTransportCont(int userId, boolean tenKms,
                                         boolean fiftyKms,
                                         boolean hundredKms,
                                         boolean thousandKms) {

        if (controller.countPublicTransport(userId) >= 10) {
            if (!tenKms) {
                controller.addAchievementUser(userId,16);
            }
            transOne.setFill(Color.GREEN);
        }

        if (controller.countPublicTransport(userId) >= 50) {
            if (!fiftyKms) {
                controller.addAchievementUser(userId,17);
            }
            transTwo.setFill(Color.GREEN);
        }

        if (controller.countPublicTransport(userId) >= 100) {
            if (!hundredKms) {
                controller.addAchievementUser(userId,18);
            }
            transThree.setFill(Color.GREEN);
        }

        if (controller.countPublicTransport(userId) >= 1000) {
            if (!thousandKms) {
                controller.addAchievementUser(userId,19);
            }
            transFour.setFill(Color.GREEN);
        }

    }

    /**
     * This method checks for public transport achievements.
     */
    @FXML
    public void checkCarbon() {

        boolean tenCarbon = false;
        boolean twentyCarbon = false;
        boolean hundredCarbon = false;
        boolean thousandCarbon = false;
        int userId = LoginController.getId();

        ArrayList<Achievement> totalCarbon = new ArrayList<>();

        for (Achievement achievement : userAchievements) {
            if (achievement.getName().contains("Co2")) {
                totalCarbon.add(achievement);
            }
        }

        for (Achievement achievement : totalCarbon) {
            if (achievement.getId() == 8) {
                tenCarbon = true;
            } else if (achievement.getId() == 9) {
                twentyCarbon = true;
            } else if (achievement.getId() == 10) {
                hundredCarbon = true;
            } else if (achievement.getId() == 11) {
                thousandCarbon = true;
            }
        }
        checkCarbonCont(userId, tenCarbon, twentyCarbon,
                hundredCarbon, thousandCarbon);
    }

    /**
     * Continues from the previous method. Used to reduce cyclomatic complexity.
     * @param userId The user
     * @param tenCarbon If ten carbon activities has been done
     * @param twentyCarbon If twenty carbon activities has been done
     * @param hundredCarbon If a hundred carbon activities has been done
     * @param thousandCarbon If a thousand carbon activities has been done
     */
    @FXML
    public void checkCarbonCont(int userId, boolean tenCarbon,
                                boolean twentyCarbon,
                                boolean hundredCarbon,
                                boolean thousandCarbon) {

        if (controller.countCarbon(userId) >= 10.0) {
            if (!tenCarbon) {
                controller.addAchievementUser(userId, 8);
            }
            carbonOne.setFill(Color.GREEN);
        }

        if (controller.countCarbon(userId) >= 25.0) {
            if (!twentyCarbon) {
                controller.addAchievementUser(userId, 9);
            }
            carbonTwo.setFill(Color.GREEN);
        }

        if (controller.countCarbon(userId) >= 100.0) {
            if (!hundredCarbon) {
                controller.addAchievementUser(userId,10);
            }
            carbonThree.setFill(Color.GREEN);
        }

        if (controller.countCarbon(userId) >= 1000.0) {
            if (!thousandCarbon) {
                controller.addAchievementUser(userId,11);
            }
            carbonFour.setFill(Color.GREEN);
        }
    }

    /**
     * This method checks for activity achievements.
     */
    @FXML
    public void checkActivity() {

        boolean firstAct = false;
        boolean fiveAct = false;
        int userId = LoginController.getId();

        ArrayList<Achievement> totalActivity = new ArrayList<>();

        for (Achievement achievement: userAchievements) {
            if (achievement.getName().contains("activit")) {
                totalActivity.add(achievement);
            }
        }

        for (Achievement achievement: totalActivity) {
            if (achievement.getId() == 1) {
                firstAct = true;
            } else if (achievement.getId() == 2) {
                fiveAct = true;
            }
        }

        if (controller.countActivity(userId) >= 1) {
            if (!firstAct) {
                controller.addAchievementUser(userId, 1);
            }
            activityOne.setFill(Color.GREEN);
        }

        if (controller.countActivity(userId) >= 5) {
            if (!fiveAct) {
                controller.addAchievementUser(userId,2);
            }
            activityTwo.setFill(Color.GREEN);
        }

    }

    /**
     * This method checks for friend achievements.
     */
    @FXML
    public void countFriends() {

        boolean firstFriend = false;
        boolean tenFriend = false;

        User user = createUser(LoginController.getId());

        ArrayList<User> friendsList = new ArrayList<>();
        friendsList = controller.getFriends(user);

        ArrayList<Achievement> friendAchievement = new ArrayList<>();
        for (Achievement achievement : userAchievements) {
            if (achievement.getName().equals("Add a friend")
                    || achievement.getName().equals("Add ten friend")) {

                friendAchievement.add(achievement);
            }
        }

        for (Achievement achieve : friendAchievement) {
            if (achieve.getId() == 22) {
                firstFriend = true;
            } else if (achieve.getId() == 23) {
                tenFriend = true;
            }
        }

        countFriendsCont(friendsList, firstFriend, tenFriend);
    }

    /**
     * Continues from the previous method. Used to reduce cyclomatic complexity.
     * @param friendsList An ArrayList of all friends
     * @param firstFriend If one friend has been added
     * @param tenFriend If ten friends have been added
     */
    @FXML
    public void countFriendsCont(ArrayList<User> friendsList,
                                 boolean firstFriend, boolean tenFriend) {

        int userId = LoginController.getId();
        boolean onePoint = false;
        boolean threePoint = false;

        ArrayList<Achievement> morePoints = new ArrayList<>();
        for (Achievement achievement : userAchievements) {
            if (achievement.getName().contains("Have more points than")) {
                morePoints.add(achievement);
            }
        }

        for (Achievement achieve : morePoints) {
            if (achieve.getId() == 20) {
                onePoint = true;
            } else if (achieve.getId() == 21) {
                threePoint = true;
            }
        }
        countFriendsContCont(userId, friendsList, firstFriend,
                tenFriend, onePoint, threePoint);
    }

    /**
     * Continues from the previous method. Used to reduce cyclomatic complexity.
     * @param userId The user
     * @param friendsList An ArrayList of all friends
     * @param firstFriend If one friend has been added
     * @param tenFriend If ten friends have been added
     * @param onePoint If one points
     * @param threePoint If three points
     */
    @FXML
    public void countFriendsContCont(int userId, ArrayList<User> friendsList,
                                     boolean firstFriend,
                                     boolean tenFriend,
                                     boolean onePoint,
                                     boolean threePoint) {

        int friendNumber = friendsList.size();
        if (friendNumber >= 1) {
            if (!firstFriend) {
                controller.addAchievementUser(userId, 22);
            }
            friendThree.setFill(Color.GREEN);
        }

        if (friendNumber >= 10) {
            if (!tenFriend) {
                controller.addAchievementUser(userId, 23);
            }
            friendFour.setFill(Color.GREEN);
        }

        if (controller.countMorePoints(friendsList,userId) >= 1) {
            if (!onePoint) {
                controller.addAchievementUser(userId, 20);
            }
            friendOne.setFill(Color.GREEN);
        }

        if (controller.countMorePoints(friendsList,userId) >= 3) {
            if (!threePoint) {
                controller.addAchievementUser(userId, 21);
            }
            friendTwo.setFill(Color.GREEN);
        }
    }

    /**
     * This method checks for vegetarian meals achievements.
     */
    @FXML
    public void checkVegetarianMeal() {

        boolean firstVegMeal = false;
        boolean tenVegMeal = false;
        boolean twentyVegMeal = false;

        ArrayList<Achievement> vegetarianMeal = new ArrayList<>();

        for (Achievement achievement : userAchievements) {
            if (achievement.getName().contains("vegetarian")) {
                vegetarianMeal.add(achievement);
            }
        }

        for (Achievement achievement : vegetarianMeal) {
            if (achievement.getId() == 25) {
                firstVegMeal = true;
            } else if (achievement.getId() == 26) {
                tenVegMeal = true;
            } else if (achievement.getId() == 27) {
                twentyVegMeal = true;
            }
        }
        checkVegetarianMealCont(firstVegMeal, tenVegMeal, twentyVegMeal);
    }

    /**
     * Continues from the previous method. Used to reduce cyclomatic complexity.
     * @param firstVegMeal If one Vegetarian Meal activity has been done
     * @param tenVegMeal If ten Vegetarian Meal activities have been done
     * @param twentyVegMeal If twenty Vegetarian Meal activities have been done
     */
    @FXML
    public void checkVegetarianMealCont(boolean firstVegMeal,
                                        boolean tenVegMeal, boolean twentyVegMeal) {

        if ( controller.countVegetarianMeal(LoginController.getId()) >= 1) {
            if (!firstVegMeal) {
                controller.addAchievementUser(LoginController.getId(), 25);
            }
            vegOne.setFill(Color.GREEN);
        }

        if (controller.countVegetarianMeal(LoginController.getId()) >= 10) {
            if (!tenVegMeal) {
                controller.addAchievementUser(LoginController.getId(), 26);
            }
            vegTwo.setFill(Color.GREEN);
        }

        if (controller.countVegetarianMeal(LoginController.getId()) >= 20) {
            if (!twentyVegMeal) {
                controller.addAchievementUser(LoginController.getId(), 27);
            }
            vegThree.setFill(Color.GREEN);
        }
    }

    /**
     * This method checks for bike achievements.
     */
    @FXML
    public void checkBike() {
        boolean tenKms = false;
        boolean fiftyKms = false;
        boolean hundredKms = false;
        boolean thousandKms = false;

        int userId = LoginController.getId();

        ArrayList<Achievement> bikeInsteadOfCar = new ArrayList<>();

        for (Achievement achievement : userAchievements) {
            if (achievement.getName().contains("bicycle")) {
                bikeInsteadOfCar.add(achievement);
            }
        }

        for (Achievement achievement : bikeInsteadOfCar) {
            if (achievement.getId() == 12) {
                tenKms = true;
            }
            if (achievement.getId() == 13) {
                fiftyKms = true;
            }
            if (achievement.getId() == 14) {
                hundredKms = true;
            }
            if (achievement.getId() == 15) {
                thousandKms = true;
            }
        }

        if (controller.countBike(userId) >= 10) {
            if (!tenKms) {
                controller.addAchievementUser(userId, 12);
            }
            bikeOne.setFill(Color.GREEN);
        }
        checkBikeCont(userId, fiftyKms, hundredKms, thousandKms);
    }

    /**
     * Continues from the previous method. Used to reduce cyclomatic complexity.
     * @param userId The user
     * @param fiftyKms If fifty kilometers have been traveled
     * @param hundredKms If a hundred kilometers have been traveled
     * @param thousandKms If a thousand kilometers have been traveled
     */
    @FXML
    public void checkBikeCont(int userId, boolean fiftyKms,
                              boolean hundredKms, boolean thousandKms) {

        if (controller.countBike(userId) >= 50) {
            if (!fiftyKms) {
                controller.addAchievementUser(userId, 13);
            }
            bikeTwo.setFill(Color.GREEN);
        }

        if (controller.countBike(userId) >= 100) {
            if (!hundredKms) {
                controller.addAchievementUser(userId, 14);
            }
            bikeThree.setFill(Color.GREEN);
        }

        if (controller.countBike(userId) >= 1000) {
            if (!thousandKms) {
                controller.addAchievementUser(userId, 15);
            }
            bikeFour.setFill(Color.GREEN);
        }
    }

    /**
     * This method checks for points achievements.
     */
    @FXML
    public void checkPoints() {

        boolean fiftyPoints = false;
        boolean hundredPoints = false;
        boolean fivehundredPoints = false;
        boolean thousandPoints = false;
        boolean tenthousandPoints = false;
        int userId = LoginController.getId();

        ArrayList<Achievement> totalPoints = new ArrayList<>();

        for (Achievement achievement : userAchievements) {
            if (achievement.getName().contains("points")) {
                totalPoints.add(achievement);
            }
        }

        for (Achievement achievement : totalPoints) {
            if (achievement.getId() == 3) {
                fiftyPoints = true;
            } else if (achievement.getId() == 4) {
                hundredPoints = true;
            } else if (achievement.getId() == 5) {
                fivehundredPoints = true;
            } else if (achievement.getId() == 6) {
                thousandPoints = true;
            } else if (achievement.getId() == 7) {
                tenthousandPoints = true;
            }
        }
        checkPointsCont(userId, fiftyPoints, hundredPoints,fivehundredPoints,
                thousandPoints, tenthousandPoints);
    }

    /**
     * Continues from the previous method. Used to reduce cyclomatic complexity.
     * @param userId The user
     * @param fiftyPoints If fifty points have been achieved
     * @param hundredPoints If a hundred points have been achieved
     * @param fivehundredPoints If five hundred points have been achieved
     * @param thousandPoints If a thousand points have been achieved
     * @param tenthousandPoints If ten thousand points have been achieved
     */
    @FXML
    public void checkPointsCont(int userId, boolean fiftyPoints,
                                boolean hundredPoints,
                                boolean fivehundredPoints,
                                boolean thousandPoints,
                                boolean tenthousandPoints) {

        if (controller.countPoints(userId) >= 50) {
            if (!fiftyPoints) {
                controller.addAchievementUser(userId, 3);
            }
            pointOne.setFill(Color.GREEN);
        }

        if (controller.countPoints(userId) >= 100) {
            if (!hundredPoints) {
                controller.addAchievementUser(userId, 4);
            }
            pointTwo.setFill(Color.GREEN);
        }

        if (controller.countPoints(userId) >= 500) {
            if (!fivehundredPoints) {
                controller.addAchievementUser(userId, 5);
            }
            pointThree.setFill(Color.GREEN);
        }

        checkPointsContCont(userId, fiftyPoints, hundredPoints, fivehundredPoints,
                thousandPoints, tenthousandPoints);
    }

    /**
     * Continues from the previous method. Used to reduce cyclomatic complexity.
     * @param userId The user
     * @param fiftyPoints If fifty points have been achieved
     * @param hundredPoints If a hundred points have been achieved
     * @param fivehundredPoints If five hundred points have been achieved
     * @param thousandPoints If a thousand points have been achieved
     * @param tenthousandPoints If ten thousand points have been achieved
     */
    @FXML
    public void checkPointsContCont(int userId, boolean fiftyPoints,
                                    boolean hundredPoints,
                                    boolean fivehundredPoints,
                                    boolean thousandPoints,
                                    boolean tenthousandPoints) {

        if (controller.countPoints(userId) >= 1000) {
            if (!thousandPoints) {
                controller.addAchievementUser(userId, 6);
            }
            pointFour.setFill(Color.GREEN);
        }

        if (controller.countPoints(userId) >= 10000) {
            if (!tenthousandPoints) {
                controller.addAchievementUser(userId, 7);
            }
            pointFive.setFill(Color.GREEN);
        }
    }


    /**
     * This method displays the achievements of the user to the terminal.
     *
     * @param userId The id of the user.
     */
    @FXML
    public void displayAchievements(int userId) {

        double userTotal = 0.0;
        User user = createUser(userId);
        ArrayList<Achievement> userAchievements = new ArrayList<>();

        userAchievements = controller.getAchievements(user);

        if (userAchievements.isEmpty()) {
            percentageBar.setProgress(0.0);
        } else {

            for (Achievement achievement: userAchievements) {
                System.out.println("Achievement: " + achievement.getName());
                userTotal ++;
            }

            percentageBar.setProgress(userTotal / 28);
            int progress = (int)userTotal * 100 / 28;
            String text = "You have completed ";
            percentage.setText(text + progress + "% of the achievements.");
        }

    }
}
