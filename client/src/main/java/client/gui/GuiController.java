package client.gui;

import controller.Controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class GuiController {

    @FXML
    public GridPane rootPane;
    @FXML
    private Label emissionsLabel;
    @FXML
    private Label welcomeText;

    private Controller controller = new Controller();

    @FXML
    protected void handleAddActivity(ActionEvent event) throws IOException {
        String path = "client/src/main/java/client/gui/Activity.fxml";
        URL url = new File(path).toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);
    }

    @FXML
    private void handleSeeFriends(ActionEvent event) throws IOException {
        String path = "client/src/main/java/client/gui/Friends.fxml";
        URL url = new File(path).toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);
    }

    @FXML
    private void handleAchievements(ActionEvent event) throws IOException {
        String path = "client/src/main/java/client/gui/Achievements.fxml";

        URL url = new File(path).toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);
    }

    @FXML
    private void handleTimeline(ActionEvent event) throws IOException {
        System.out.println("Timeline");
    }

    @FXML
    private void handleStatistics(ActionEvent event) throws IOException {
        String path = "client/src/main/java/client/gui/Statistics.fxml";
        URL url = new File(path).toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);
    }

    @FXML
    private void handleAccount(ActionEvent event) throws IOException {
        System.out.println("Account");
    }

    @FXML
    private void handleLogin(ActionEvent event) throws IOException {
        String path = "client/src/main/java/client/gui/LoginScreen.fxml";
        URL url = new File(path).toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);
    }

    @FXML
    private void handleLogout(ActionEvent actionEvent) throws IOException {

        LoginController.logoutUser();
        String path = "client/src/main/java/client/gui/LoginScreen.fxml";
        URL url = new File(path).toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);
    }

    @FXML
    private void handleWelcome(MouseEvent mouseEvent) {

        String name = LoginController.getUsername();
        welcomeText.setText("Welcome back " + name);

        if (controller.getCO2ByUser(LoginController.getId()) != -1) {
            Double emissions = controller.getCO2ByUser(LoginController.getId());
            emissionsLabel.setText(emissions + " Kg of CO2!");

        }

    }

}
