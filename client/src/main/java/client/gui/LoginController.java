package client.gui;

import controller.Controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import model.User;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class LoginController {

    private static User loggedUser = new User("pwd", "user");
    @FXML
    private Label forgotPass;
    @FXML
    private Label errorLabel;
    @FXML
    private Button signUpButton;
    @FXML
    private Button loginButton;
    @FXML
    private TextField userText;
    @FXML
    private PasswordField passText;
    @FXML
    private GridPane loginPane;


    private Controller controller = new Controller();

    @FXML
    private void handlePassword(ActionEvent actionEvent) {
        System.out.println("You have entered your password");
    }

    @FXML
    public void handleUsername(ActionEvent actionEvent) {
        System.out.println("You have entered your username");
    }

    @FXML
    private void handleLogin(ActionEvent actionEvent) throws IOException {

        String usernameText = userText.getText();
        String usernamePass = passText.getText();

        System.out.println(usernameText);

        User user = new User(usernamePass, usernameText);

        int userId = controller.verifyUser(user);
        user.setId(userId);

        if (userId == -1) {
            System.out.println("User not found.");
            errorLabel.visibleProperty().setValue(true);
        } else {

            System.out.println("User is registered. The user id is: " + user.getId());
            errorLabel.visibleProperty().setValue(false);

            this.loggedUser.setId(userId);
            this.loggedUser.setUsername(usernameText);

            String path = "client/src/main/java/client/gui/GuiX.fxml";
            URL url = new File(path).toURL();
            Parent pane = FXMLLoader.load(url);

            Gui.primaryStage.getScene().setRoot(pane);
        }

    }

    @FXML
    private void handleSignUp(ActionEvent actionEvent) throws IOException {

        String path = "client/src/main/java/client/gui/SignUp.fxml";
        URL url = new File(path).toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);

    }

    @FXML
    private void handleLostPass(MouseEvent mouseEvent) throws IOException {

        String path = "client/src/main/java/client/gui/ChangePassword.fxml";
        URL url = new File(path).toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);
    }

    public static User getLoggedUser() {
        return loggedUser;
    }

    public static String getUsername() {
        return loggedUser.getUsername();
    }

    public static int getId() {
        return loggedUser.getId();
    }

    /**
     * Logs out the user.
     */
    public static void logoutUser() {

        loggedUser.setUsername("");
        loggedUser.setId(0);
    }

    /**
     * This method logs the user out of the application.
     *
     * @param usr The user to be logged out.
     */
    public static void loginUser(User usr) {

        loggedUser.setId(usr.getId());
        loggedUser.setUsername(usr.getUsername());

    }

    public static int getUserPoints() {
        return loggedUser.getPoints();
    }

    @FXML
    private void handleToStart(ActionEvent actionEvent) throws IOException {
        String path = "client/src/main/java/client/gui/GuiX.fxml";
        URL url = new File(path).toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);
    }
}
