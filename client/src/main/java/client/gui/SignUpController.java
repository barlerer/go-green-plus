package client.gui;

import controller.Controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import model.User;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class SignUpController {

    @FXML
    private Label warning2;
    @FXML
    private Label warning1;
    @FXML
    private PasswordField repeatPass;
    @FXML
    private TextField userText;
    @FXML
    private PasswordField userPass;
    @FXML
    private Button signUpButton;
    @FXML
    private Button backButton;

    private Controller controller = new Controller();

    @FXML
    private void handleSignUp(ActionEvent actionEvent) throws IOException {

        String usernameText =  userText.getText();
        String usernamePass =  userPass.getText();
        String repeatPassword = repeatPass.getText();

        User user = new User(usernamePass,usernameText);

        if (usernamePass.equals(repeatPassword)) {

            warning1.visibleProperty().setValue(false);
            warning2.visibleProperty().setValue(false);

            boolean registration =  controller.registerUser(user);

            if ( registration) {
                System.out.println("User has been registered successfully!");
                user.setUsername(usernameText);
                user.setPassword(usernamePass);

                int userId = controller.verifyUser(user);
                user.setId(userId);

                System.out.println( user.getId() +  "\n");

                LoginController.loginUser(user);
                String path = "client/src/main/java/client/gui/GuiX.fxml";
                URL url = new File(path).toURL();
                Parent pane = FXMLLoader.load(url);

                Gui.primaryStage.getScene().setRoot(pane);

            } else {
                System.out.println("There was an error with registering this user :(");
            }
        } else {
            System.out.println("The passwords are not matching. Please try again.");

            warning1.visibleProperty().setValue(true);
            warning2.visibleProperty().setValue(true);
        }
    }

    @FXML
    private void handleToLogin(ActionEvent actionEvent) throws IOException {

        URL url = new File("client/src/main/java/client/gui/LoginScreen.fxml").toURL();
        Parent pane = FXMLLoader.load(url);

        Gui.primaryStage.getScene().setRoot(pane);
    }

    @FXML
    private void handleCheck(MouseEvent mouseEvent) {

        String userText =  this.userText.getText();
        String userPass =  this.userPass.getText();
        String repeatPassword = repeatPass.getText();

        boolean val = userPass.equals("") || userText.equals("") || repeatPassword.equals("");

        if ( signUpButton.isDisable()) {
            if ( !val) {
                signUpButton.setDisable(false);
            }

        }  else if (!signUpButton.isDisable()) {
            if ( val) {
                signUpButton.setDisable(true);
            }
        }
    }
}
