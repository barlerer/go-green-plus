package client.app;

import client.gui.Gui;
import controller.Controller;
import javafx.application.Application;


/**
 * This class defines the methods of our application project GoGreenPlus
 *
 * @author OOP Project Group 17
 * @version 1.9
 * @since 1.0
 */
public class App {
    /**
     * This is the main method. This makes the file App.java runnable.
     * @param args the arguments that are passed to the main method
     */
    public static void main(String[] args) {
        Controller controller = new Controller();
        //AchievementLister achievementLister = new AchievementLister(36);
        //ArrayList<Activity> activityList = achievementLister.getActivityArray();
        Application.launch(Gui.class,args);
    }

}
