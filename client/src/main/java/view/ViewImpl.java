package view;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * This class defines the view of the MVC (Model-View-Controller)
 *
 * @author OOP Project Group 17
 * @version 1.9
 * @since 1.9
 */
public class ViewImpl {

    /**
     * This method creates a new ArrayList of strings the lists all activities.
     *
     * @param activities the JSONArrayList of all the activities
     * @return the name of the activities
     */
    public  ArrayList<String> listActivities(JSONArray activities) {
        ArrayList<String> nameOfActivities = new ArrayList<>();

        for (int i = 0; i < activities.length(); i++) {
            JSONObject tempObj = activities.getJSONObject(i);
            nameOfActivities.add(tempObj.getString("name"));
        }
        return nameOfActivities;
    }
}