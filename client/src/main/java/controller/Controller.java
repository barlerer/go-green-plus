package controller;

import hashing.Hashing;
import model.Achievement;
import model.AchievementLister;
import model.Activity;
import model.ActivityLister;
import model.DatabaseModel;
import model.User;
import org.json.JSONArray;
import view.ViewImpl;

import java.util.ArrayList;
import java.util.Collections;


/**
 * This class defines the controller of the MVC (Model-View-Controller)
 *
 * @author OOP Project Group 17
 * @version 1.9
 * @since 1.9
 */
public class Controller {

    static final String url = "http://136.144.209.147:8080";
    DatabaseModel database;
    ViewImpl view;
    String path;

    /**
     * This is the main method. This makes the file Controller.java runnable.
     * We define a JSONArrayList which lists all activities
     */
    public Controller() {
        database = new DatabaseModel();
        view = new ViewImpl();
    }

    public ArrayList<String> getAllActivites() {
        return view.listActivities(database.getActivites());
    }


    public JSONArray getAllUserActivities(int idofActivity) {
        path = url + "/user/" + idofActivity + "/doneActivities";
        return database.getAllActivitesOfUser(path);
    }

    /**
     * Takes a user id and returns all the user's activities.
     *
     * @param userId The id of the user.
     * @return An ArrayList of activities.
     */
    public ArrayList<Activity> listActivities(int userId ) {

        ArrayList<Activity> activityList = new ArrayList<>();
        ActivityLister activityLister = new ActivityLister(userId);
        activityList = activityLister.getActivityArray();

        return activityList;
    }

    /**
     * Counts the times a user has bought local produce.
     *
     * @param userId The id of the user.
     * @return The count of times the user has bought local produce.
     */
    public int countProduce(int userId) {

        int count = 0;
        ArrayList<Activity> activityList = new ArrayList<>();
        ActivityLister activityLister = new ActivityLister(userId);
        activityList = activityLister.getActivityArray();

        count = activityLister.listLocalProduce(activityList);

        return count;
    }

    /**
     * Counts how many kms the user has travelled by public transport.
     *
     * @param userId The id of the user.
     * @return The number of kms travelled by public transport.
     */
    public int countPublicTransport(int userId) {

        int count = 0;
        ArrayList<Activity> activityList = new ArrayList<>();
        ActivityLister activityLister = new ActivityLister(userId);
        activityList = activityLister.getActivityArray();

        count = activityLister.listPublicTrans(activityList);

        return count;
    }

    /**
     * Method which counts how many activities the user has done.
     *
     * @param userId The id of the user.
     * @return The number of activities the user has done.
     */
    public int countActivity(int userId) {

        int count = 0;
        ArrayList<Activity> activityList = new ArrayList<>();
        ActivityLister activityLister = new ActivityLister(userId);
        activityList = activityLister.getActivityArray();

        for (Activity activity : activityList) {
            count++;
        }

        return count;
    }

    /**
     * Method which counts how many of the user's friends have fewer points than him.
     *
     * @param friendsList An arraylist with the user's friend.
     * @param userId      The user's id.
     * @return The number of friends that have fewer points than the user.
     */
    public int countMorePoints(ArrayList<User> friendsList, int userId) {

        int friendsNumber = 0;
        int userPoints = getPointsOfUser(userId);

        for (User friend : friendsList) {

            if (friend.getPoints() < userPoints) {
                friendsNumber++;
            }
        }

        return friendsNumber;

    }

    /**
     * Counts the total Co2 emissions of a user.
     *
     * @param userId the id of the user.
     * @return The total Co2 emissions in Kg.
     */
    public double countCarbon(int userId) {

        double total = 0.0;
        ArrayList<Activity> activityList = new ArrayList<>();
        ActivityLister activityLister = new ActivityLister(userId);
        activityList = activityLister.getActivityArray();

        total = activityLister.countCarbon(activityList);

        return total;
    }

    /**
     * Returns the total points of the user.
     *
     * @param userId The user
     * @return The total points of the user
     */
    public int countPoints(int userId) {

        int totalPoints = 0;
        ArrayList<Activity> activityList = new ArrayList<>();
        AchievementLister achievementLister = new AchievementLister(userId);
        activityList = achievementLister.getActivityArray();

        totalPoints = achievementLister.countPoints(activityList);

        return totalPoints;
    }

    /**
     * Returns the total kilometers traveled by bike by the user.
     *
     * @param userId The user
     * @return The total kilometers traveled by bike by the user
     */
    public int countBike(int userId) {

        int count = 0;
        ArrayList<Activity> activityList = new ArrayList<>();
        ActivityLister activityLister = new ActivityLister(userId);
        activityList = activityLister.getActivityArray();

        count = activityLister.listBike(activityList);

        return count;
    }

    /**
     * Counts how many vegetarian meals the user has eaten.
     *
     * @param userId The id of the user.
     * @return The number of vegetarian meals eaten by the user.
     */
    public int countVegetarianMeal(int userId) {

        int count = 0;
        ArrayList<Activity> activityList = new ArrayList<>();
        ActivityLister activityLister = new ActivityLister(userId);
        activityList = activityLister.getActivityArray();

        count = activityLister.listVegetarianMeal(activityList);

        return count;
    }

    /**
     * Method to recieve a CO2 by userId.
     *
     * @param userId The id of user we want to recieve its co2
     * @return The co2
     */

    public double getCO2ByUser(int userId) {
        return database.getCO2ByUser(userId);
    }

    /**
     * Method to recieve a CO2 by userId.
     *
     * @param userId The id of user we want to recieve its points
     * @return The points
     */
    public int getPointsOfUser(int userId) {
        //path = url + "/user/" + userId + "/totalPoints";
        return database.getPointsOfUser(userId);

    }

    /**
     * Verifies the user in the database.
     *
     * @param user The user to verify
     * @return The id of the user, if the password matches the on in the database
     */
    public int verifyUser(User user) {
        if (user == null || user.getUsername() == null || user.getPassword() == null) {
            return -1;
        }

        String username = user.getUsername();
        String password = user.getPassword();

        User userFromServer = database.retrieveUserByUserName(username);

        if (userFromServer == null || userFromServer.getPassword() == null) {
            //TODO - ERROR MESSAGE NO CONNECTION
            return -1;

        } else if (Hashing.verifyCrypt(password, userFromServer.getPassword())) {
            return userFromServer.getId();
        }
        return -1;
    }

    /**
     * Changes a user password to what was provided.
     *
     * @param user        - The user credentials of whom to change password
     * @param newPassword - The new password to be changed
     * @return true if the password was changed, or false otherwise
     */
    public boolean changePassword(User user, String newPassword) {
        if (user == null || newPassword == null) {
            return false;
        }

        user.setId(verifyUser(user));
        if (user.getId() == -1) {
            return false;
        }
        return database.updatePassword(user, Hashing.crypt(newPassword));
    }

    /**
     * The method registers the new user in the database.
     *
     * @param user The user to be registered
     * @return True if the user was registerd, false otherwise
     */
    public boolean registerUser(User user) {
        if (user == null || user.getUsername() == null || user.getPassword() == null
                || user.getUsername().isEmpty() || user.getPassword().isEmpty()) {
            return false;
        }
        user.setPassword(Hashing.crypt(user.getPassword()));
        return database.registerUser(user);
    }

    /**
     * This methods fill the filling gaps of a user it recieves.
     *
     * @param user The user to complete id or username
     */
    public void fillDataFromServer(User user) {
        if (user == null) {
            return;
        }
        if (user.getId() == 0) {
            database.getIdOfUser(user);
        } else {
            database.getUsernameOfUser(user);
        }
    }

    /**
     * Method to add a friend.
     *
     * @param user        The user with the id
     * @param friendToadd The name of the friend to add
     * @return True if friend added, false otherwise
     */
    public boolean addFriend(User user, String friendToadd) {
        if (user == null || friendToadd == null || user.getId() == 0
                || friendToadd.isEmpty()) {
            return false;
        }
        return database.addFriend(user, friendToadd);
    }

    /**
     * Method to retrieve list of friends.
     *
     * @param user the user
     * @return null if user does not exist, list of all friends otherwise
     */
    public ArrayList<User> getFriends(User user) {
        if (user == null || user.getId() == 0) {
            return null;
        }
        ArrayList<User> listOfFriends = database.getFriendsList(user);
        Collections.sort(listOfFriends, (o1, o2) -> o2.getPoints() - o1.getPoints());

        return listOfFriends;
    }

    /**
     * Gets all of the activities and their properties from servers.
     * Get's all of the activities and their properties from servers.
     *
     * @return The activities
     */
    public ArrayList<Activity> getActivities() {
        ArrayList<Activity> listOfActivities = database.getAllActivities();
        return listOfActivities;
    }

    /**
     * Sends and activity a user has performed to the server.
     *
     * @param user     The user Id that performed the activity
     * @param activity The activity Id The user has performed
     * @return True if the server accepted
     */
    public boolean sendDoneActivity(User user, Activity activity) {

        if (user == null || user.getId() < 1 || activity == null || activity.getActivityId() < 1) {
            return false;
        }
        return database.sendDoneActivityToServer(user, activity);
    }

    /**
     * Return all of the achievements of a user.
     *
     * @param user The user of which we want to receive the achievements
     * @return The user achievements
     */
    public ArrayList<Achievement> getAchievements(User user) {
        ArrayList<Achievement> listOfAchievements = null;

        if (user == null || user.getId() < 1) {
            return listOfAchievements;
        }

        listOfAchievements = database.getAchievements(user);
        return listOfAchievements;
    }

    /**
     * Function to remove a friend.
     *
     * @param user           The user of who we want to remove a friend.
     * @param friendToRemove The friend we want to remove
     * @return True if friend removed
     */
    public boolean removeFriend(User user, User friendToRemove) {
        if (user == null
                || user.getId() < 1
                || friendToRemove == null
                || friendToRemove.getId() == 0) {
            return false;
        }

        return database.removeFriend(user, friendToRemove);
    }

    /**
     * Returns a list of all the possible achievements.
     *
     * @return A list of all the achievements.
     */
    public ArrayList<Achievement> getAllAchievements() {

        ArrayList<Achievement> listOfAchievements = database.getAllAchievements();
        return listOfAchievements;
    }

    /**
     * Assigns the specified achivement to the specified user.
     *
     * @param userId        The specified user
     * @param achievementId The specified achievement
     * @return If the request was successful
     */
    public boolean addAchievementUser(int userId, int achievementId) {
        if (userId == 0 || achievementId == 0) {
            return false;
        }
        return database.addAchievementUser(userId, achievementId);
    }

    /*
    public Date dateParser(String dateString) {
        Date date = new Date();
        date = database.dateParser(dateString);
        return date;
    }
    */
}