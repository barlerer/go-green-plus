package model;


/**
 * This class is for transferring user credentials.
 *
 * @author OOP Project Group 17
 * @version 1.9
 * @since 1.0
 */
public class User {
    String password;
    String username;
    int id;
    double co2 = 0;
    int points = 0;

    /**
     * Constructor for User class.
     *
     * @param password password of a user
     * @param username username of a user
     */
    public User(String password, String username) {
        this.password = password;
        this.username = username;
        this.id = 0;
    }

    /**
     * Constructor for user class with id.
     *
     * @param password password of a user
     * @param username username of a user
     * @param id       id of a user
     */
    public User(String password, String username, int id) {
        this.password = password;
        this.username = username;
        this.id = id;
    }

    /**
     * Constructor in case there is only the userId.
     *
     * @param id the id of the user
     */
    public User(int id) {
        this.id = id;
    }

    /**
     * Getter for co2 of a user.
     *
     * @return The co2 of a user.
     */
    public double getCo2() {
        return co2;
    }

    /**
     * Setter for the co2.
     *
     * @param co2 The new co2 value to change
     */
    public void setCo2(double co2) {
        this.co2 = co2;
    }

    /**
     * Getter for points of a user.
     *
     * @return The points of a user
     */
    public int getPoints() {
        return points;
    }

    /**
     * Setter for points.
     *
     * @param points The new value of points to be set
     */
    public void setPoints(int points) {
        this.points = points;
    }


    /**
     * Setter for the id.
     *
     * @param id The new id to be set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Getter for the user id.
     *
     * @return The id of the user
     */
    public int getId() {
        return id;
    }


    /**
     * Getter for password.
     *
     * @return The user password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter for password.
     *
     * @param password The new password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter for username.
     *
     * @return The username of a user
     */
    public String getUsername() {
        return username;
    }

    /**
     * Setter for username.
     *
     * @param username The new username to replace
     */
    public void setUsername(String username) {
        this.username = username;
    }

}
