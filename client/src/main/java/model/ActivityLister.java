package model;

import controller.Controller;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ActivityLister {

    private  Controller controller = new Controller();
    private int id;

    /**
     * Default constructor. Sets id to 0.
     */
    public ActivityLister() {
        this.id = 0;
    }

    /**
     * Constructor with userId.
     * @param userId the id of the current logged in user.
     */
    public ActivityLister(int userId) {
        this.id = userId;
    }

    /**
     * Getter for the id field.
     * @return The id.
     */
    public int getId() {
        return id;
    }

    /**
     * Setter for the id field.
     * @param id The value you want to set the id to.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Method which retrieves the json array from the database model.
     *
     * @return The json array.
     */
    public JSONArray getJsonArray() {
        JSONArray jsonArray = new JSONArray();
        jsonArray = controller.getAllUserActivities(id);
        return jsonArray;
    }

    /**
     * Method which takes the json array provided by getJsonArray and returns it in an ArrayList.
     *
     * @return An ArrayList of activities the user has done.
     */
    public ArrayList<Activity> getActivityArray() {

        JSONArray jsonArray = new JSONArray();
        jsonArray = getJsonArray();
        ArrayList<Activity> activityList = new ArrayList<>();

        for ( int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String activityName = jsonObject.getJSONObject("activity").getString("name");
            int activityId = jsonObject.getJSONObject("activity").getInt("id");
            int quantity = jsonObject.getInt("quantity");
            int points = jsonObject
                    .getJSONObject("activity")
                    .getJSONObject("co2Category")
                    .getInt("points");
            double emissions = jsonObject
                    .getJSONObject("activity")
                    .getJSONObject("co2Category")
                    .getDouble("kgCO2Emission");

            Activity activity = new Activity(activityName,activityId);
            activity.setCo2(emissions);
            activity.setPoints(points);
            activity.setQuantity(quantity);

            activityList.add(activity);
        }


        return activityList;
    }

    /**
     * Method which counts how many times a user bought local produce.
     *
     * @param arrayList Which contains the list of all the activities of a user.
     * @return An integer which counts how many times a user has bought local produce.
     */
    public int listLocalProduce( ArrayList<Activity> arrayList) {

        int counter = 0;

        for (Activity activity:arrayList) {

            String name = activity.getActivityName();
            int id = activity.getActivityId();
            double emissions = activity.getCo2();

            if (id == 2 && name.equals("Buying local produce")) {
                counter ++;
            }
        }

        return counter;
    }

    /**
     * Method which counts how many kms a user has travelled by public transport.
     *
     * @param arrayList Which contains the list of all the activities of a user.
     * @return An integer representing how many kms the user travelled by public transport.
     */
    public int listPublicTrans( ArrayList<Activity> arrayList) {

        int counter = 0;

        for (Activity activity: arrayList) {

            String name = activity.getActivityName();
            int id = activity.getActivityId();
            int quantity = activity.getQuantity();

            if (name.equals("Using public transport instead of a car") && id == 4) {
                counter += quantity;
            }
        }

        return counter;
    }

    /**
     * Method which counts how many veg meals a user has eaten.
     *
     * @param arrayList Which contains the list of all the activities of a user.
     * @return An integer with the number of vegetarian meals a user has eaten.
     */
    public int listVegetarianMeal(ArrayList<Activity> arrayList) {
        int counter = 0;

        for (Activity activity : arrayList) {
            String name = activity.getActivityName();
            int id = activity.getActivityId();
            double emissions = activity.getCo2();

            if (name.equals("Eating a vegetarian meal") && id == 1) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * Method which counts how many kms a user has travelled by bike.
     *
     * @param arrayList Which contains the list of all the activities of a user.
     * @return The number of kms travelled by bike.
     */
    public int listBike(ArrayList<Activity> arrayList) {
        int counter = 0;

        for (Activity activity : arrayList) {
            String name = activity.getActivityName();
            int id = activity.getActivityId();
            double emissions = activity.getCo2();
            int quantity = activity.getQuantity();

            if (name.equals("Using a bike instead of a car") && id == 3) {
                counter += quantity;
            }
        }
        return counter;
    }

    /**
     * Counts the total Co2 of the user's activity.
     *
     * @param arrayList Which contains the list of all the activities of a user.
     * @return Total number of Co2 emissions in Kg.
     */
    public double countCarbon(ArrayList<Activity> arrayList) {
        double totalCo2 = 0.0;

        for (Activity activity: arrayList) {
            double emissions = activity.getCo2();
            int quantity = activity.getQuantity();

            totalCo2 += emissions * quantity;
        }

        return totalCo2;
    }

    /**
     * Counts the total points of the user.
     *
     * @param arrayList Which contains the list of all the activities of a user.
     * @return Total number of points.
     */
    public int countPoints(ArrayList<Activity> arrayList) {
        int totalPoints = 0;

        for (Activity activity: arrayList) {
            int points = activity.getPoints();
            int quantity = activity.getQuantity();

            totalPoints += points * quantity;
        }
        return totalPoints;
    }


    /**
     * Method to test equality.
     *
     * @param other Object to compare with.
     * @return True if it's equal to this, false if not.
     */
    public boolean equals( Object other) {

        if (this == other) {
            return true;
        }

        if ( other == null) {
            return false;
        }

        ActivityLister that = (ActivityLister) other;
        return this.getId() == that.getId();
    }
}
