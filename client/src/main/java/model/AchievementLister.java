package model;

import controller.Controller;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

//import static sun.management.snmp.jvminstr.JvmThreadInstanceEntryImpl.ThreadStateMap.Byte1.other;

public class AchievementLister {

    private DatabaseModel databaseModel = new DatabaseModel();
    private Controller controller = new Controller();
    private int id;

    public AchievementLister() {
        this.id = 0;
    }

    public AchievementLister(int userId) {
        this.id = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns a JSONArray representation of the current object.
     *
     * @return A JSONArray representation of the current object
     */
    public JSONArray getJsonArray() {
        JSONArray jsonArray = new JSONArray();
        jsonArray = controller.getAllUserActivities(id);
        return jsonArray;
    }

    /**
     * Returns an ArrayList representation of activities.
     *
     * @return An ArrayList representation of activities
     */
    public ArrayList<Activity> getActivityArray() {

        JSONArray jsonArray = new JSONArray();
        jsonArray = getJsonArray();
        ArrayList<Activity> activityList = new ArrayList<Activity>();

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String activityName = jsonObject.getJSONObject("activity").getString("name");
            int activityId = jsonObject.getJSONObject("activity").getInt("id");
            int quantity = jsonObject.getInt("quantity");
            int points = jsonObject.getJSONObject("activity")
                    .getJSONObject("co2Category").getInt("points");
            double emissions = jsonObject.getJSONObject("activity")
                    .getJSONObject("co2Category").getDouble("kgCO2Emission");

            Activity activity = new Activity(activityName, activityId);
            activity.setCo2(emissions);
            activity.setPoints(points);
            activity.setQuantity(quantity);

            activityList.add(activity);
        }
        return activityList;
    }


    /**
     * Counts the total number of vegetarian meals eaten.
     *
     * @param arrayList Which contains the user's activities.
     * @return Total number of vegetarian meals eaten.
     */
    public int listBike(ArrayList<Activity> arrayList) {
        int counter = 0;

        for (Activity activity : arrayList) {
            String name = activity.getActivityName();
            int id = activity.getActivityId();
            double emissions = activity.getCo2();

            if (name.equals("Using a bike instead of a car") && id == 3) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * Returns the total number of points in the specified ArrayList representation of activities.
     *
     * @param arrayList The specified ArrayList representation of achievements
     * @return The total number of points in the specified ArrayList representation of activities
     */
    public int countPoints(ArrayList<Activity> arrayList) {
        int totalPoints = 0;

        for (Activity activity : arrayList) {
            String name = activity.getActivityName();
            int id = activity.getActivityId();
            double emissions = activity.getCo2();
            int points = activity.getPoints();
            int quantity = activity.getQuantity();

            totalPoints += points * quantity;
        }
        return totalPoints;
    }

    /**
     * Returns the total number of vegetarian meals eaten.
     *
     * @param arrayList With the user's activities.
     * @return Number of vegetarian meals eaten.
     */
    public int listVegetarianMeal(ArrayList<Activity> arrayList) {
        int counter = 0;

        for (Activity activity : arrayList) {
            String name = activity.getActivityName();
            int id = activity.getActivityId();
            double emissions = activity.getCo2();

            if (name.equals("Eating a vegetarian meal") && id == 1) {
                counter++;
            }
        }
        return counter;
    }

    /*
    public void assignAchievementsToUser(User user) {
        String url = "http://136.144.209.147:8080";
        ArrayList<Activity> activityList = getActivityArray();
        int points = databaseModel.getIntfromServer(url);

        if (points >= 50) {
            Achievement achievement = new Achievement(new User(36), "Reach 50 points", 3, 5);
            databaseModel.addAchievement(achievement);
        }
    }
    */
}
