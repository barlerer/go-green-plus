package model;

import java.util.Date;
import java.util.Objects;

public class Achievement {
    User user;
    String name;
    int id;
    int points;
    Date date;

    /**
     * Constructor for an achievement.
     *
     * @param user   The user that owns the achievement
     * @param name   Name of achievement
     * @param id     Unique id of achievement
     * @param points How many points that achievement has granted
     * @param date The date the achievement was achieved
     */
    public Achievement(User user, String name, int id, int points, Date date) {
        this.user = user;
        this.name = name;
        this.id = id;
        this.points = points;
        this.date = date;
    }

    /**
     * Constructor for achievement without a user.
     *
     * @param name   Name of achievement
     * @param id     Unique id of achievement
     * @param points How many points that achievement has granted
     */
    public Achievement(String name, int id, int points) {
        this.name = name;
        this.id = id;
        this.points = points;
    }

    /**
     * Constructor without date.
     * @param user The user that owns the achievement
     * @param name Name of achievement
     * @param id Unique id of achievement
     * @param points How many points that achievement has granted
     */
    public Achievement(User user, String name, int id, int points) {
        this.user = user;
        this.name = name;
        this.id = id;
        this.points = points;
    }

    /**
     * Getter for the user.
     *
     * @return The user that owns the Achievement
     */
    public User getUser() {
        return user;
    }

    /**
     * Setter for the user.
     *
     * @param user The new user that will own the achievement
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Getter for name of the achievement.
     *
     * @return The name of achievement
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for name.
     *
     * @param name The new name of the achievement
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for id.
     *
     * @return The id of achievement
     */
    public int getId() {
        return id;
    }

    /**
     * Setter for id.
     *
     * @param id The new id of the achievement
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Getter for points.
     *
     * @return The number of points the achievement has granted
     */
    public int getPoints() {
        return points;
    }

    /**
     * Setter for the points.
     *
     * @param points The new value of points
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * Getter for date.
     * @return The date the achievement was achieved
     */
    public Date getDate() {
        return date;
    }

    /**
     * Setter for date.
     * @param date The new date to be set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * equals method.
     <<<<<<< client/src/main/java/model/Achievement.java
     * @param achievement The object to check if they are equals.
     * @return Whether they are equals
     */
    public boolean equals(Object achievement) {
        if (this == achievement) {
            return true;
        }
        if (achievement == null || getClass() != achievement.getClass()) {
            return false;
        }
        Achievement that = (Achievement) achievement;
        return id == that.id
                && points == that.points
                && Objects.equals(name, that.name);
    }
}
