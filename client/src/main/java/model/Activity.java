package model;


import java.util.Date;

/**
 * This class defines the activity
 *
 * @author OOP Project Group 17
 * @version 1.9
 * @since 1.9
 */
public class Activity {
    String activityName;
    int activityId;
    int quantity;
    int points;
    double co2;
    Date date;

    /**
     * Constructor for Activity class.
     *
     * @param activityName The name of the activity
     * @param activityId   The server ID of activity
     */
    public Activity(String activityName, int activityId) {
        this.activityName = activityName;
        this.activityId = activityId;
        this.quantity = 1;
        this.co2 = 0.0;
        this.points = 0;
        this.date = new Date();
    }

    /**
     * Getter for points.
     *
     * @return The points of the activity
     */
    public int getPoints() {
        return points;
    }

    /**
     * Setter for activity.
     *
     * @param points The new value of points to set
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * Getter for co2.
     *
     * @return The co2 of the activity
     */
    public double getCo2() {
        return co2;
    }

    /**
     * Setter for co2.
     *
     * @param co2 The new value to set.
     */
    public void setCo2(double co2) {
        this.co2 = co2;
    }

    /**
     * Getter for the name of activity.
     *
     * @return The name of the activity
     */
    public String getActivityName() {
        return activityName;
    }

    /**
     * Getter for the activity ID.
     *
     * @return The activity ID
     */
    public int getActivityId() {
        return activityId;
    }

    /**
     * Getter for the quantity.
     *
     * @return The quantity of activity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Setter for the quantity of activity.
     *
     * @param quantity The new quantity to be set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Setter for the date of the activity.
     *
     * @param newDate The value of the new date.
     */
    public void setDate(Date newDate) {
        this.date = newDate;
    }

    /**
     * Getter for the date of the activity.
     *
     * @return The date of the activity.
     */
    public Date getDate() {
        return date;
    }
}
