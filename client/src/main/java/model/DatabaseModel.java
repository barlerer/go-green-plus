package model;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * This class defines the model of the MVC (Model-View-Controller)
 *
 * @author OOP Project Group 17
 * @version 1.9
 * @since 1.9
 */
public class DatabaseModel {

    public static final String url = "http://136.144.209.147:8080";
    String path;


    /**
     * This method lists the ID of the activity when requested.
     *
     * @return the list of all activities
     */
    public JSONArray getActivites() {
        path = url + "/activity";
        HttpResponse<JsonNode> jsonResponse = jsonRequest(path);
        if (jsonResponse.getStatus() == 200) {
            JSONArray listOfActivities = jsonResponse.getBody().getArray();
            return listOfActivities;
        } else {
            return null;
        }
    }

    /**
     * This method lists all activities a user has done.
     *
     * @return a list of all the activities a user has done
     */
    public JSONArray getAllActivitesOfUser(String url) {
        HttpResponse<JsonNode> httpResponse = jsonRequest(url);
        if (httpResponse.getStatus() == 200) {
            return httpResponse.getBody().getArray();
        } else {
            return null;
        }
    }

    /**
     * This method retrieves the total amount of points a user has accumulated.
     *
     * @param url The path to address the server
     * @return the amount of points a user has accumulated up until now
     */
    public int getIntfromServer(String url) {
        HttpResponse<String> jsonResponse = null;
        try {
            jsonResponse = Unirest
                    .get(url)
                    .asString();
        } catch (UnirestException e) {
            System.out.println("Server is unreachable at the moment. Please try again later");
        }
        if (jsonResponse != null) {
            return Integer.parseInt(jsonResponse.getBody());
        }
        return -1;
    }

    /**
     * This method retrieves the amount of CO2 a user reduced by adding an activity.
     *
     * @return the amount of CO2 a user has reduced per activity
     */
    public double getDoublefromserver(String url) {
        HttpResponse<String> jsonResponse = null;
        try {
            jsonResponse = Unirest
                    .get(url)
                    .asString();
        } catch (UnirestException e) {
            System.out.println("Server is unreachable at the moment. Please try again later");
        }
        if (jsonResponse != null) {
            return Double.parseDouble(jsonResponse.getBody());
        }
        return -1;
    }

    /**
     * Method to retrieve a user by its associated username.
     *
     * @param username username of it's user
     * @return null if username does not exist, a new object of User otherwise
     */
    public User retrieveUserByUserName(String username) {
        path = url + "/user/id-hashedPassword/" + username;
        HttpResponse<JsonNode> httpResponse = jsonRequest(path);
        if (httpResponse.getStatus() != 200) {
            return null;
        }
        int idOfUser = 0;
        String password = "";
        JSONObject jsonObject = httpResponse.getBody().getObject();
        if (!jsonObject.has("id") || !jsonObject.has("hashedPassword")) {
            return new User(null, null);
        }
        idOfUser = Integer.parseInt(jsonObject.get("id").toString());
        password = jsonObject.get("hashedPassword").toString();
        return new User(password, username, idOfUser);
    }

    /**
     * Sends a JSON request to the server.
     *
     * @param path Path of the server
     * @return If the request was successful=
     */
    public HttpResponse<JsonNode> jsonRequest(String path) {
        HttpResponse<JsonNode> jsonResponse = null;
        try {
            jsonResponse = Unirest
                    .get(path)
                    .header("accept", "application/json")
                    .asJson();
        } catch (UnirestException e) {
            System.out.println("Server is unreachable at the moment. Please try again later");
        }
        return jsonResponse;
    }

    /**
     * Method to update the password of a user.
     *
     * @param user        the user
     * @param newPassword the updated password
     * @return true if password successfully changed, false otherwise
     */
    public boolean updatePassword(User user, String newPassword) {
        JSONObject obj = new JSONObject();
        obj.put("id", user.getId());
        obj.put("hashedPassword", newPassword);
        path = url + "/user/" + user.getId() + "/password";
        HttpResponse<JsonNode> jsonResponse = jsonPatchRequest(path, obj);
        if (jsonResponse.getStatus() == 200) {
            return true;
        }
        return false;
    }

    /**
     * Sends a patch request to the server.
     *
     * @param path      The path of the server
     * @param objToSend The JSON object to send
     * @return The sever response
     */
    public HttpResponse<JsonNode> jsonPatchRequest(String path, JSONObject objToSend) {
        HttpResponse<JsonNode> jsonResponse = null;
        try {
            jsonResponse = Unirest
                    .patch(path)
                    .header("accept", "application/json")
                    .header("Content-Type", "application/json")
                    .body(objToSend)
                    .asJson();
        } catch (UnirestException e) {
            System.out.println("Server is unreachable at the moment. Please try again later");
        }
        return jsonResponse;
    }

    /**
     * Method to register a user.
     *
     * @param user the user
     * @return true if a user successfully registered, false otherwise
     */
    public boolean registerUser(User user) {
        JSONObject userToSend = new JSONObject();
        userToSend.put("username", user.getUsername());
        userToSend.put("hashedPassword", user.getPassword());
        path = url + "/user";
        HttpResponse<JsonNode> jsonResponse = jsonPostRequest(path, userToSend);
        return jsonResponse.getStatus() == 200;
    }

    /**
     * Sends a Post request to the server.
     *
     * @param path      The path of the server
     * @param objToSend The JSON object to send
     * @return The servers response
     */
    public HttpResponse<JsonNode> jsonPostRequest(String path, JSONObject objToSend) {
        HttpResponse<JsonNode> jsonResponse = null;
        try {
            jsonResponse = Unirest
                    .post(path)
                    .header("accept", "application/json")
                    .header("Content-Type", "application/json")
                    .body(objToSend)
                    .asJson();
        } catch (UnirestException e) {
            System.out.println("Server is unreachable at the moment. Please try again later");
        }
        return jsonResponse;
    }

    /**
     * Method to retrieve the ID of the user.
     *
     * @param user the user
     */
    public void getIdOfUser(User user) {
        path = url + "/user/id-hashedPassword/" + user.getUsername();
        HttpResponse<JsonNode> jsonResponse = jsonRequest(path);
        if (jsonResponse.getStatus() != 200) {
            return;
        }
        JSONObject jsonObject = jsonResponse.getBody().getObject();
        if (jsonObject.has("id")) {
            user.setId(jsonObject.getInt("id"));
        }
    }

    /**
     * Method to retrieve the username of a user.
     *
     * @param user the user
     */
    public void getUsernameOfUser(User user) {
        path = url + "/user/" + user.getId();
        HttpResponse<JsonNode> jsonResponse = jsonRequest(path);
        if (jsonResponse.getStatus() == 200) {
            JSONObject jsonObject = jsonResponse.getBody().getObject();
            if (jsonObject.has("username")) {
                user.setUsername(jsonObject.getString("username"));
            }
        }
    }

    /**
     * Method to add a friend to your friends list.
     *
     * @param user        the user
     * @param friendToadd the user to be added
     * @return status code 200 (OK) when a friend is successfully added
     */
    public boolean addFriend(User user, String friendToadd) {
        path = url + "/user/" + user.getId() + "/friendship/" + friendToadd;
        HttpResponse<JsonNode> jsonResponse = jsonPostRequestNoBody(path);
        return jsonResponse.getStatus() == 200;
    }

    /**
     * Sends a post request to the server.
     *
     * @param path The path of the server
     * @return The servers response
     */
    public HttpResponse<JsonNode> jsonPostRequestNoBody(String path) {
        HttpResponse<JsonNode> jsonResponse = null;
        jsonResponse = Unirest
                .post(path)
                .header("accept", "application/json")
                .header("Content-Type", "application/json")
                .asJson();

        return jsonResponse;
    }

    /**
     * Method to retrieve the friends list of a user.
     *
     * @param user the user
     * @return a list of friends of a user
     */
    public ArrayList<User> getFriendsList(User user) {
        path = url + "/user/" + user.getId() + "/friendship";
        HttpResponse<JsonNode> jsonResponse = jsonRequest(path);
        JSONArray jsonArray = jsonResponse.getBody().getArray();
        ArrayList<User> listOfFriends = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            int friendId = jsonObject.getJSONObject("id").getJSONObject("user2").getInt("id");
            String friendUsername =
                    jsonObject.getJSONObject("id")
                            .getJSONObject("user2")
                            .getString("username");
            int totalPoints =
                    jsonObject.getJSONObject("id")
                            .getJSONObject("user2")
                            .getInt("totalPoints");


            User friend = new User(friendId);
            friend.setUsername(friendUsername);
            friend.setPoints(totalPoints);
            double co2 = 0.0;
            if (totalPoints != 0) {
                co2 = jsonObject.getJSONObject("id").getJSONObject("user2").getDouble("totalKgCo2");
            }
            friend.setCo2(co2);

            listOfFriends.add(friend);
        }
        return listOfFriends;
    }

    /**
     * Returns all of the activities from server.
     *
     * @return ArrayList of all the activities
     */
    public ArrayList<Activity> getAllActivities() {
        path = url + "/activity";
        HttpResponse<JsonNode> jsonResponse = jsonRequest(path);
        if (jsonResponse.getStatus() != 200) {
            return null;
        }
        JSONArray jsonArray = jsonResponse.getBody().getArray();
        ArrayList<Activity> listOfActivities = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String activityName = jsonObject.getString("name");
            int idOfActivity = jsonObject.getInt("id");
            Activity activity = new Activity(activityName, idOfActivity);
            activity.setCo2(jsonObject.getJSONObject("co2Category").getDouble("kgCO2Emission"));
            activity.setPoints(jsonObject.getJSONObject("co2Category").getInt("points"));
            listOfActivities.add(activity);
        }
        return listOfActivities;
    }

    /**
     * Send the activity a user has performed.
     *
     * @param user     The user id to send to the server
     * @param activity The activity Id to send to server
     * @return True if server accepted
     */
    public boolean sendDoneActivityToServer(User user, Activity activity) {
        JSONObject doneActivity = new JSONObject();
        doneActivity.put("activity", new JSONObject().put("id", activity.getActivityId()));
        doneActivity.put("user", new JSONObject().put("id", user.getId()));
        doneActivity.put("quantity", activity.getQuantity());
        path = url + "/doneActivity";
        HttpResponse<JsonNode> jsonResponse = jsonPostRequest(path, doneActivity);
        return jsonResponse.getStatus() == 200;
    }

    /**
     * Method to get all of a user achievements.
     *
     * @param user The user of whom achievements we want to achieve
     * @return An ArrayList of achievements
     */
    public ArrayList<Achievement> getAchievements(User user) {
        ArrayList<Achievement> listOfAchievements = new ArrayList<>();
        path = url + "/user/" + user.getId() + "/userAchievements";
        HttpResponse<JsonNode> jsonResponse = jsonRequest(path);
        if (jsonResponse.getStatus() == 200) {
            JSONArray jsonArray = jsonResponse.getBody().getArray();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int idOfAchievement = jsonObject
                        .getJSONObject("achievement")
                        .getInt("id");
                String nameOfAchievement = jsonObject
                        .getJSONObject("achievement")
                        .getString("name");
                int points = jsonObject
                        .getJSONObject("achievement")
                        .getInt("points");
                String dateString = jsonObject.getString("date");
                Date date = dateParser(dateString);
                Achievement achievement =
                        new Achievement(user, nameOfAchievement, idOfAchievement, points, date);
                listOfAchievements.add(achievement);
            }

        }
        return listOfAchievements;
    }

    /**
     * Method to parse the date from server.
     *
     * @param dateString The string of date we want to parse
     * @return A Date object
     */
    public Date dateParser(String dateString) {
        if (dateString == null) {
            return null;
        }
        dateString = dateString.substring(0, 23);
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        Date date = null;
        try {
            date = parser.parse(dateString);
        } catch (ParseException e) {
            System.out.println("Cant parse, unsupported format");
        }
        return date;
    }

    /**
     * Function to remove a friend.
     *
     * @param user           The user of who we want to remove a friend.
     * @param friendToRemove The friend we want to remove
     * @return True if friend removed
     */
    public boolean removeFriend(User user, User friendToRemove) {
        path = url + "/user/" + user.getId() + "/friendship/" + friendToRemove.getId();
        HttpResponse<JsonNode> jsonResponse = jsonDeleteRequest(path);
        return jsonResponse.getStatus() == 200;
    }

    /**
     * Send a delete request to the server.
     *
     * @param path The path to access
     * @return The response from server
     */
    public HttpResponse<JsonNode> jsonDeleteRequest(String path) {
        HttpResponse<JsonNode> jsonResponse = null;
        jsonResponse = Unirest
                .delete(path)
                .header("accept", "application/json")
                .header("Content-Type", "application/json")
                .asJson();

        return jsonResponse;
    }

    /**
     * This method gets all the possible achievements from the server.
     *
     * @return An arrayList of all the achievements in the database.
     */
    public ArrayList<Achievement> getAllAchievements() {
        path = url + "/achievement";
        HttpResponse<JsonNode> jsonResponse = jsonRequest(path);
        if (jsonResponse.getStatus() != 200) {
            return null;
        }

        JSONArray jsonArray = jsonResponse.getBody().getArray();
        ArrayList<Achievement> listOfAchievements = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String achievementName = jsonObject.getString("name");
            int achievementId = jsonObject.getInt("id");
            int achievementPoints = jsonObject.getInt("points");

            Achievement achv = new Achievement(achievementName, achievementId, achievementPoints);
            listOfAchievements.add(achv);

        }
        return listOfAchievements;

    }

    /**
     * Method to recieve a CO2 by userId.
     *
     * @param userId The id of user we want to recieve its co2
     * @return The co2
     */
    public double getCO2ByUser(int userId) {
        path = url + "/user/" + userId + "/kgCO2Emission";
        return getDoublefromserver(path);
    }

    /**
     * Method to recieve a points by userId.
     *
     * @param userId The id of user we want to receive its points
     * @return The points
     */
    public int getPointsOfUser(int userId) {
        path = url + "/user/" + userId + "/totalPoints";
        return getIntfromServer(path);
    }

    /**
     * Assigns the provided achievement to the provided user.
     *
     * @param userId        The provided user
     * @param achievementId The provided achievement
     * @return If the upload was successful
     */
    public boolean addAchievementUser(int userId, int achievementId) {
        String path = url + "/userAchievement";

        JSONObject userToSend = new JSONObject();
        userToSend.put("user", new JSONObject().put("id", userId));
        userToSend.put("achievement", new JSONObject().put("id", achievementId));

        HttpResponse<JsonNode> jsonResponse = jsonPostRequest(path, userToSend);
        return jsonResponse.getStatus() == 200;
    }

    /*
    public boolean addAchievement(Achievement achievement) {
        path = url + "/userAchievement";
        JSONObject achievementToSend = new JSONObject();
        achievementToSend.put("id", new JSONObject().put("id", achievement.getId()));
        achievementToSend.put("name", new JSONObject().put("name", achievement.getName()));
        achievementToSend.put("points", new JSONObject().put("points", achievement.getPoints()));
        achievementToSend.put("user", new JSONObject().put("user", achievement.getUser()));

        HttpResponse<JsonNode> jsonResponse = jsonPostRequest(path, achievementToSend);
        return jsonResponse.getStatus() == 200;
    }
    */

    /*
    public ArrayList<Activity> arryifyUserActivities() {
        ArrayList<Activity> allActivities = new ArrayList<>();
        JSONArray jsonArray = getAllActivitesOfUser(url);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            int idOfActivity = jsonObject
                    .getJSONObject("activity")
                    .getInt("id");
            String nameOfActivity = jsonObject
                    .getJSONObject("activity")
                    .getString("name");
            int points = jsonObject
                    .getJSONObject("activity")
                    .getJSONObject("co2Category")
                    .getInt("points");
            double kgco2 = jsonObject
                    .getJSONObject("activity")
                    .getJSONObject("co2Category")
                    .getDouble("kgco2");
            Activity activity = new Activity(nameOfActivity, idOfActivity);
            activity.setPoints(points);
            activity.setCo2(kgco2);
            allActivities.add(activity);
        }
        return allActivities;
    }
    */
}