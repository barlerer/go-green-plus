The source of the client and the documentation can be found here:

http://unirest.io/java.html

To check if the code compiles run:

```
mvn install
```

To see the checkstyle result run:

```
mvn site
```

and open /target/site/index.html.

To run the tests:

```
mvn test
```