To check if the code compiles run the maven install command:

```
mvn install
```

To see the checkstyle result run:

```
mvn site
```

and open /target/site/index.html.

Then to compile the jar file containing all dependencies for the server to run:

```
mvn package
```

To run the tests:

```
mvn test
```