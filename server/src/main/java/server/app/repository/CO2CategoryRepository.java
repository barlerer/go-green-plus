package server.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import server.app.model.Co2Category;

@Repository
public interface CO2CategoryRepository extends JpaRepository<Co2Category, Long> {
}
