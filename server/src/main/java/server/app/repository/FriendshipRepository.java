package server.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import server.app.model.Friendship;
import server.app.model.FriendshipPk;

public interface FriendshipRepository extends JpaRepository<Friendship, FriendshipPk> {

    @Query(value = "SELECT * FROM friendships WHERE user1_id = ?1", nativeQuery = true)
    Iterable<Friendship> findAllByUserIdAndStatus(Long id);

    @Modifying
    @Query(value = "DELETE FROM friendships WHERE ((user1_id=?1 AND user2_id=?2)"
            + " OR (user1_id=?2 AND user2_id=?1))", nativeQuery = true)
    void deleteById(Long userId1, Long userId2);

}
