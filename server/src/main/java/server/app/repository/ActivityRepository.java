package server.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import server.app.model.Activity;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long> {
}
