package server.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import server.app.model.DoneActivity;

@Repository
public interface DoneActivityRepository extends JpaRepository<DoneActivity, Long> {
}
