package server.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import server.app.model.Achievement;

@Repository
public interface AchievementRepository extends JpaRepository<Achievement, Long> {
}
