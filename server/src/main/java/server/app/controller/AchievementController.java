package server.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import server.app.dao.AchievementDao;
import server.app.model.Achievement;

import javax.validation.Valid;

@RestController
public class AchievementController {

    @Autowired
    private AchievementDao achievementDao;

    //Add achievement
    @PostMapping(path = "/achievement")
    public Achievement addAchievement(@Valid @RequestBody Achievement achievement) {
        return achievementDao.save(achievement);
    }

    //Get all achievements
    @GetMapping(path = "/achievement")
    public Iterable<Achievement> getAllAchievements() {
        return achievementDao.findAll();
    }

}
