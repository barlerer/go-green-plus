package server.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import server.app.dao.FriendshipDao;
import server.app.dao.UserDao;
import server.app.model.DoneActivity;
import server.app.model.Friendship;
import server.app.model.FriendshipPk;
import server.app.model.User;
import server.app.model.UserAchievement;
import server.app.model.UserHashedPasswordOnly;

import java.math.BigDecimal;
import java.util.List;
import javax.validation.Valid;

@RestController
public class UserController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private FriendshipDao friendshipDao;

    //add user
    @PostMapping(path = "/user")
    public User addUser(@Valid @RequestBody User user) {
        return userDao.save(user);
    }

    //get all users
    @GetMapping(path = "/user")
    public Iterable<User> getAllUsers() {
        return userDao.findAll();
    }

    /**
     * @param userId specifies the id of the user that should be returned.
     * @return user object with the specified id
     *      if the user doesn't exist null is returned.
     */
    //get user by id
    @GetMapping(path = "/user/{id}")
    public ResponseEntity<User> getUserById(@PathVariable(value = "id") Long userId) {
        User user;
        try {
            user = userDao.findById(userId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(user);
    }

    /**
     * @param userId specifies the id of the activity that should be added to the database.
     * @param userDetails specifies the name of the user that should be
     *      added to the database.
     * @return user object with the specified id and details after it is added to the database,
     *      if the user could not be added to the database null is returned.
     */
    //update user by id
    @PutMapping(path = "/user/{id}")
    public ResponseEntity<User> updateUserById(@PathVariable(value = "id")
        Long userId, @RequestBody User userDetails) {

        User user;
        try {
            user = userDao.findById(userId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        userDao.update(user, userDetails);

        return ResponseEntity.ok().body(user);

    }

    /**
     * @param userId specifies the id of the user that should be deleted to the database.
     * @return user object with the specified id after it is deleted from the database,
     *      if the user doesn't exist null is returned.
     */
    //delete a user by id
    @DeleteMapping(path = "/user/{id}")
    public ResponseEntity<User> deleteUserById(@PathVariable(value = "id") Long userId) {
        User user;
        try {
            user = userDao.findById(userId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        userDao.delete(user);

        return ResponseEntity.ok().build();

    }

    /**
     * @param userId specifies the id of the user of which the total points should be calculated.
     * @return the total amount of point the user has earned.
     */
    //Get total points of one user by id
    //TODO Move implementation to DAO and use sql statements
    @GetMapping(path = "/user/{id}/totalPoints")
    public ResponseEntity<Integer> getTotalPointsById(@PathVariable(value = "id") Long userId) {
        User user;
        try {
            user = userDao.findById(userId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        List<UserAchievement> userAchievements = user.getUserAchievements();
        int totalPoints = user.getTotalPoints();

        if (!userAchievements.isEmpty()) {
            for (int i = 0; i < userAchievements.size(); i++) {
                totalPoints += userAchievements.get(i).getAchievement().getPoints();
            }
        }

        return ResponseEntity.ok().body(totalPoints);
    }

    /**
     * @param userId specifies the id of the user of which the total amount of CO2 emission saved
     *     should be returned.
     * @return the amount of CO2 emission reduced by specified user.
     */
    //Get total kg of CO2 reduced by user id
    //TODO Move implementation to DAO and use sql statements
    @GetMapping(path = "/user/{id}/kgCO2Emission")
    public ResponseEntity<BigDecimal> getKgCO2EmissionById(
        @PathVariable(value = "id") Long userId) {
        User user;
        try {
            user = userDao.findById(userId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        BigDecimal totalPoints = user.getTotalKgCo2();

        return ResponseEntity.ok().body(totalPoints);
    }

    /**
     * @param userId specifies the id of the user of which the done activities should be returned.
     * @return List of all DoneActivity objects of a user.
     */
    //Get all activities of one user by id
    @GetMapping(path = "/user/{id}/doneActivities")
    public ResponseEntity<List<DoneActivity>> getDoneActivitiesById(
            @PathVariable(value = "id") Long userId) {
        User user;
        try {
            user = userDao.findById(userId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        List<DoneActivity> doneActivities = user.getDoneActivities();

        return ResponseEntity.ok().body(doneActivities);
    }

    /**
     * @param userId specifies the id of the user of which the achievements should be returned.
     * @return List of all UserAchievement objects of a user.
     */
    //Get all achievements of one user by id
    @GetMapping(path = "/user/{id}/userAchievements")
    public ResponseEntity<List<UserAchievement>> getUserAchievementsById(
            @PathVariable(value = "id") Long userId) {
        User user;
        try {
            user = userDao.findById(userId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        List<UserAchievement> userAchievements = user.getUserAchievements();

        return ResponseEntity.ok().body(userAchievements);
    }

    /**
     * @param userId specifies the id of the user of which the password should be returned.
     * @return password of a user.
     */
    //Get password of user by id
    @GetMapping(path = "/user/{id}/password")
    public ResponseEntity<UserHashedPasswordOnly> getPasswordById(
            @PathVariable(value = "id") Long userId) {
        User user;
        try {
            user = userDao.findById(userId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        UserHashedPasswordOnly userHashedPasswordOnly = new UserHashedPasswordOnly();
        userHashedPasswordOnly.setId(user.getId());
        userHashedPasswordOnly.setHashedPassword(user.getHashedPassword());

        return ResponseEntity.ok().body(userHashedPasswordOnly);
    }

    /**
     * @param userId id of the user who's password should be updated.
     * @param userHashedPasswordOnly the password String retrieved as a json
     *                              which should be added to the user.
     * @return the updated user with the new password.
     */
    //update password by id
    @PatchMapping(path = "/user/{id}/password")
    public ResponseEntity<UserHashedPasswordOnly> updatePasswordById(
            @PathVariable(value = "id") Long userId,
            @RequestBody UserHashedPasswordOnly userHashedPasswordOnly) {
        if (userHashedPasswordOnly.getId() != userId) {
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).build();
        }

        User user;

        try {
            user = userDao.findById(userId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        user.setHashedPassword(userHashedPasswordOnly.getHashedPassword());
        userDao.save(user);

        return ResponseEntity.ok().body(userHashedPasswordOnly);
    }

    /**
     * @param username specifies the username of the user of which you want to get it's hash
     *                and id from.
     * @return Map containing the id and hash of the user as a json array.
     */
    //Get id and hash from username
    @GetMapping(path = "/user/id-hashedPassword/{username}")
    public ResponseEntity<UserHashedPasswordOnly> getIdAndHashByUsername(
            @PathVariable(value = "username") String username) {
        User user;
        try {
            user = userDao.findByUsername(username);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        UserHashedPasswordOnly userHashedPasswordOnly = new UserHashedPasswordOnly();
        userHashedPasswordOnly.setId(user.getId());
        userHashedPasswordOnly.setHashedPassword(user.getHashedPassword());

        return ResponseEntity.ok().body(userHashedPasswordOnly);
    }

    /**
     * @param user1Id id of the user that wants to send a friend request.
     * @param user2Username id of the user that user1 wants to add.
     * @return a friendship object containing both users.
     */
    @PostMapping(path = "/user/{user1Id}/friendship/{user2Username}")
    public ResponseEntity<Friendship> friendRequest(
            @PathVariable(value = "user1Id") Long user1Id,
            @PathVariable(value = "user2Username") String user2Username) {
        System.out.println(user1Id + " " + user2Username);
        User user1;
        try {
            user1 = userDao.findById(user1Id);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.noContent().build();
        }
        User user2;
        try {
            user2 = userDao.findByUsername(user2Username);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.noContent().build();
        }

        FriendshipPk id = new FriendshipPk(user1, user2);
        Friendship friendship = new Friendship(id);
        friendshipDao.save(friendship);

        return ResponseEntity.ok().body(friendship);
    }

    /**
     * @param id of the users whose friendships you want to see.
     * @return a list of users.
     */
    @GetMapping(path = "/user/{id}/friendship")
    public ResponseEntity<Iterable<Friendship>> getAllFriends(
            @PathVariable(value = "id") Long id) {
        User user;
        try {
            user = userDao.findById(id);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        Iterable<Friendship> friendships = friendshipDao.findAllByUserIdAndStatus(user.getId());
        if (!friendships.iterator().hasNext()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok().body(friendships);
    }

    /**
     * @param userId id of the user that wants to delete a friend.
     * @param friendId id of the user that's going to get deleted.
     * @return code 200 with a message that confirms that the friend was successfully removed
     */
    @DeleteMapping(path = "/user/{userId}/friendship/{friendId}")
    public ResponseEntity<?> removeFriendById(
            @PathVariable(value = "userId") Long userId,
            @PathVariable(value = "friendId") Long friendId) {
        User user1;
        try {
            user1 = userDao.findById(userId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }
        User user2;
        try {
            user2 = userDao.findById(friendId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        friendshipDao.deleteById(user1.getId(), user2.getId());

        return ResponseEntity.ok("friend successfully removed");
    }

}
