package server.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import server.app.dao.ActivityDao;
import server.app.model.Activity;

import java.math.BigDecimal;
import javax.validation.Valid;

@RestController
public class ActivityController {

    @Autowired
    private ActivityDao activityDao;

    @PostMapping(path = "/activity")
    public Activity addActivity(@Valid @RequestBody Activity activity) {
        return activityDao.save(activity);
    }

    @GetMapping(path = "/activity")
    public Iterable<Activity> getAllActivities() {
        return activityDao.findAll();
    }

    /**
     * @param activityId specifies the id of the activity that should be returned.
     * @return activity object with the specified id
     *     if the activity doesn't exist null is returned.
     */
    @GetMapping(path = "/activity/{id}")
    public ResponseEntity<Activity> getActivityById(@PathVariable(value = "id") Long activityId) {
        Activity activity;
        try {
            activity = activityDao.findById(activityId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(activity);
    }

    /**
     * @param activityId specifies the id of the activity of which the points should be returned.
     * @return the amount of point earned by doing the specified activity.
     */
    //Get points by activity id
    @GetMapping(path = "/activity/{id}/points")
    public ResponseEntity<Integer> getPointsById(@PathVariable(value = "id") Long activityId) {
        Activity activity;
        try {
            activity = activityDao.findById(activityId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(activity.getCo2Category().getPoints());
    }

    /**
     * @param activityId specifies the id of the activity of which the amount of CO2 it reduces
     *     should be returned.
     * @return the amount of CO2 emission reduced by doing the specified activity.
     */
    @GetMapping(path = "/activity/{id}/kgCO2Emission")
    public ResponseEntity<BigDecimal> getKgCO2EmissionById(
        @PathVariable(value = "id") Long activityId) {
        Activity activity;
        try {
            activity = activityDao.findById(activityId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(activity.getCo2Category().getKgCO2Emission());
    }

    /**
     * @param activityId specifies the id of the activity that should be added to the database.
     * @param activityDetails specifies the name of the activity that should be
     *     added to the database.
     * @return activity object with the specified id and details after it is added to the database,
     *     if the activity could not be added to the database null is returned.
     */
    @PutMapping(path = "/activity/{id}")
    public ResponseEntity<Activity> updateActivityById(@PathVariable(value = "id") Long activityId,
        @RequestBody Activity activityDetails) {
        Activity activity;
        try {
            activity = activityDao.findById(activityId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        activityDao.update(activity, activityDetails);

        return ResponseEntity.ok().body(activity);

    }

    /**
     * @param activityId specifies the id of the activity that should be deleted to the database.
     * @return activity object with the specified id after it is deleted from the database,
     *     if the activity doesn't exist null is returned.
     */
    @DeleteMapping(path = "/activity/{id}")
    public ResponseEntity<Activity> deleteActivityById(@PathVariable(value = "id")
        Long activityId) {
        Activity activity;
        try {
            activity = activityDao.findById(activityId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        activityDao.delete(activity);

        return ResponseEntity.ok().build();

    }
}
