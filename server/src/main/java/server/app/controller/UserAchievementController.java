package server.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import server.app.dao.UserAchievementDao;
import server.app.model.UserAchievement;

import javax.validation.Valid;

@RestController
public class UserAchievementController {

    @Autowired
    private UserAchievementDao userAchievementDao;

    //Add UserAchievement
    @PostMapping(path = "/userAchievement")
    public UserAchievement addUserAchievement(@Valid @RequestBody UserAchievement userAchievement) {
        return userAchievementDao.save(userAchievement);
    }

    //Get all UserAchievements
    @GetMapping(path = "/userAchievement")
    public Iterable<UserAchievement> getAllUserAchievements() {
        return userAchievementDao.findAll();
    }
}
