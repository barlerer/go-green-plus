package server.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import server.app.dao.ActivityDao;
import server.app.dao.DoneActivityDao;
import server.app.dao.UserDao;
import server.app.model.Activity;
import server.app.model.DoneActivity;
import server.app.model.User;

import javax.validation.Valid;

@RestController
public class DoneActivityController {

    @Autowired
    private DoneActivityDao doneActivityDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ActivityDao activityDao;

    /**
     * @param doneActivity specifies the object of the doneActivity that should be added
     *      to the database.
     * @return doneActivity object with the specified id after it is added from the database.
     */
    @PostMapping(path = "/doneActivity")
    public ResponseEntity<DoneActivity> addDoneActivity(
        @Valid @RequestBody DoneActivity doneActivity) {

        if (doneActivity.getQuantity() == 0) {
            doneActivity.setQuantity(1);
        }

        User user = doneActivity.getUser();
        User actualUser = userDao.findById(user.getId());

        doneActivity.setActivity(activityDao.findById(doneActivity.getActivity().getId()));

        actualUser.addDoneActivity(doneActivity);

        userDao.save(actualUser);

        return ResponseEntity.ok().body(doneActivity);
    }

    @GetMapping(path = "/doneActivity")
    public Iterable<DoneActivity> getAllDoneActivities() {
        return doneActivityDao.findAll();
    }

    /**
     * @param doneActivityId specifies the id of the doneActivity that should be returned.
     * @return doneActivity object with the specified id,
     *     if the doneActivity doesn't exist null is returned.
     */
    @GetMapping(path = "/doneActivity/{id}")
    public ResponseEntity<DoneActivity> getDoneActivityById(@PathVariable(value = "id")
        Long doneActivityId) {
        DoneActivity doneActivity = doneActivityDao.findById(doneActivityId);

        if (doneActivity == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(doneActivity);
    }

    /**
     * @param doneActivityId specifies the id of the doneActivity that should be changed
     *     to the database.
     * @param doneActivityDetails specifies the name of the activity that should be
     *     added to the database.
     * @return doneActivity object with the specified id and details after it is added
     *     to the database,
     *     if the doneActivity could not be added to the database null is returned.
     */
    @PutMapping(path = "/doneActivity/{id}")
    public ResponseEntity<DoneActivity> updateDoneActivityById(@PathVariable(value = "id")
        Long doneActivityId, @RequestBody DoneActivity doneActivityDetails) {
        DoneActivity doneActivity = doneActivityDao.findById(doneActivityId);

        if (doneActivity == null) {
            return ResponseEntity.notFound().build();
        } else {
            doneActivityDao.update(doneActivity, doneActivityDetails);
        }

        return ResponseEntity.ok().body(doneActivity);

    }

    /**
     * @param doneActivityId specifies the id of the doneActivity that should be deleted
     *     from the database.
     * @return doneActivity object with the specified id after it is deleted from the database,
     *     if the doneActivity doesn't exist null is returned.
     */
    @DeleteMapping(path = "/doneActivity/{id}")
    public ResponseEntity<DoneActivity> deleteDoneActivityById(@PathVariable(value = "id")
        Long doneActivityId) {
        DoneActivity doneActivity = doneActivityDao.findById(doneActivityId);

        if (doneActivity == null) {
            return ResponseEntity.notFound().build();
        }

        doneActivityDao.delete(doneActivity);

        return ResponseEntity.ok().build();

    }


    /**
     * Adding a DoneActivity each day for every user where hasSolarPanel is true.
     * Each day at 01:00 AM.
     */
    @GetMapping(value = "/solar")
    @Scheduled(cron = "0 1 * * *")
    public void addSolarPanelDoneActivity() {
        DoneActivity doneActivity = new DoneActivity();
        Iterable<User> users = userDao.findAll();
        Activity activity = activityDao.findById(5L);
        doneActivity.setQuantity(1);
        doneActivity.setActivity(activity);
        for (User user :
                users) {
            if (user.hasSolarPanel()) {
                doneActivity.setUser(user);
                doneActivityDao.save(doneActivity);
            }
        }
    }

    /**
     * Adding activity each day scaled by change in temperature.
     * Each day at 01:00 AM.
     */
    @GetMapping(value = "/temp")
    @Scheduled(cron = "0 1 * * *")
    public void addTemperatureDoneActivity() {
        DoneActivity doneActivity = new DoneActivity();
        Iterable<User> users = userDao.findAll();
        Activity activity = activityDao.findById(6L);
        doneActivity.setActivity(activity);
        int quantity;
        for (User user :
                users) {
            for (DoneActivity doneActivity1 :
                    user.getDoneActivities()) {
                if (doneActivity1.getActivity().getId() == 6L) {
                    quantity = doneActivity1.getQuantity();
                    doneActivity.setQuantity(quantity);
                    doneActivity.setUser(doneActivity1.getUser());
                    doneActivityDao.save(doneActivity);
                    break;
                }
            }
        }
    }

}
