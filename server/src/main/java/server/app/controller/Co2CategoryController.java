package server.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import server.app.dao.Co2CategoryDao;
import server.app.model.Co2Category;

import javax.validation.Valid;

@RestController
public class Co2CategoryController {

    @Autowired
    private Co2CategoryDao co2CategoryDao;

    @PostMapping(path = "/co2Category")
    public Co2Category addCO2Category(@Valid @RequestBody Co2Category co2Category) {
        return co2CategoryDao.save(co2Category);
    }

    @GetMapping(path = "/co2Category")
    public Iterable<Co2Category> getAllCO2Categories() {
        return co2CategoryDao.findAll();
    }

    /**
     * @param co2CategoryId specifies the id of the co2Category that should be returned.
     * @return co2Category object with the specified id,
     *     if the co2Category doesn't exist null is returned.
     */
    @GetMapping(path = "/co2Category/{id}")
    public ResponseEntity<Co2Category> getCO2CategoryById(@PathVariable(value = "id")
        Long co2CategoryId) {
        //TODO Move the .get() to right here for more flexibility (and also validate with .filter()
        // and .isPresent() maybe)
        Co2Category co2Category;
        try {
            co2Category = co2CategoryDao.findById(co2CategoryId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(co2Category);
    }

    /**
     * @param co2CategoryId specifies the id of the co2Category that should be
     *     added to the database.
     * @param co2CategoryDetails specifies the name of the co2Category that should be
     *     added to the database.
     * @return co2Category object with the specified id and details after it is added
     *     to the database,
     *     if the co2Category could not be added to the database null is returned.
     */
    @PutMapping(path = "/co2Category/{id}")
    public ResponseEntity<Co2Category> updateCO2CategoryById(@PathVariable(value = "id")
        Long co2CategoryId, @RequestBody Co2Category co2CategoryDetails) {

        Co2Category co2Category;
        try {
            co2Category = co2CategoryDao.findById(co2CategoryId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }
        co2CategoryDao.update(co2Category, co2CategoryDetails);

        return ResponseEntity.ok().body(co2Category);
    }

    /**
     * @param co2CategoryId specifies the id of the co2Category that should be
     *     deleted to the database.
     * @return co2Category object with the specified id after it is deleted from the database,
     *      if the co2Category doesn't exist null is returned.
     */
    @DeleteMapping(path = "/co2Category/{id}")
    public ResponseEntity<Co2Category> deleteCo2CategoryById(@PathVariable(value = "id")
        Long co2CategoryId) {
        Co2Category co2Category;
        try {
            co2Category = co2CategoryDao.findById(co2CategoryId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        co2CategoryDao.delete(co2Category);

        return ResponseEntity.ok().build();

    }

}
