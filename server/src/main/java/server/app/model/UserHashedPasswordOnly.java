package server.app.model;

import java.util.Objects;

public class UserHashedPasswordOnly {

    private Long id;

    private String hashedPassword;

    public UserHashedPasswordOnly() {
    }

    public UserHashedPasswordOnly(Long id, String hashedPassword) {
        this.id = id;
        this.hashedPassword = hashedPassword;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    @SuppressWarnings({"checkstyle:parametername", "checkstyle:needbraces",
            "checkstyle:operatorwrap"})
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserHashedPasswordOnly that = (UserHashedPasswordOnly) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(hashedPassword, that.hashedPassword);
    }

}
