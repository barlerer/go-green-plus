package server.app.model;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Achievements")
public class Achievement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private int points;

    public Achievement() {
    }

    /**
     * @param id to identify this achievement.
     * @param name for this achievement.
     * @param points amount of points this achievement gives.
     */
    public Achievement(Long id, String name, int points) {
        this.id = id;
        this.name = name;
        this.points = points;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @SuppressWarnings({"checkstyle:parametername", "checkstyle:needbraces",
            "checkstyle:operatorwrap"})
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Achievement that = (Achievement) o;
        return points == that.points &&
                Objects.equals(id, that.id) &&
                Objects.equals(name, that.name);
    }

}
