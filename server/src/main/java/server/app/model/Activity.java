package server.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "Activities")
public class Activity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", unique = true)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JoinColumn(name = "CO2_Category_Id", nullable = false,
        foreignKey = @ForeignKey(name = "FK_Activities_CO2Categories_CO2_Category_Id"))
    private Co2Category co2Category;

    public Activity() {
    }

    /**
     * @param id for unique identification in the database.
     * @param name of the activity.
     * @param co2Category the category the activity belongs to.
     */
    public Activity(Long id, String name, Co2Category co2Category) {
        this.id = id;
        this.name = name;
        this.co2Category = co2Category;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Co2Category getCo2Category() {
        return co2Category;
    }

    public void setCo2Category(Co2Category co2Category) {
        this.co2Category = co2Category;
    }

    /**
     * @param other the object for checking equality against.
     * @return true if the objects are the same or contain the same fields.
     */
    @SuppressWarnings({"checkstyle:parametername", "checkstyle:needbraces",
            "checkstyle:operatorwrap"})
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Activity that = (Activity) other;
        return id.equals(that.id)
            && Objects.equals(name, that.name)
            && co2Category.equals(that.co2Category);
    }

}
