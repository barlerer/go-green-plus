package server.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "DoneActivities")
public class DoneActivity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    // TODO Research if this JsonIgnore affects performance
    // TODO Add username to JsonIgnore (also for activity and possibly other places)
    // This probably causes the null values in the username fields after a successful post request
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "hashedPassword", "joinDate",
            "hasSolarPanel"})
    @JoinColumn(name = "User_Id", nullable = false,
        foreignKey = @ForeignKey(name = "FK_DoneActivities_Users_User_Id"))
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JoinColumn(name = "Activity_Id", nullable = false,
        foreignKey = @ForeignKey(name = "FK_DoneActivities_Activities_Activity_Id"))
    private Activity activity;

    private int quantity;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date", updatable = false)
    private Date date;

    public DoneActivity() {
    }

    /**
     * @param id for unique identification in the database.
     * @param user the user that has completed an activity.
     * @param activity the activity completed by the user.
     */
    public DoneActivity(Long id, User user, Activity activity) {
        this.id = id;
        this.user = user;
        this.activity = activity;
    }

    /**
     * @param id for unique identification in the database.
     * @param user the user that has completed an activity.
     * @param activity the activity completed by the user.
     * @param quantity the amount of times you want to add this activity.
     */
    public DoneActivity(Long id, User user, Activity activity, int quantity) {
        this.id = id;
        this.user = user;
        this.activity = activity;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @PrePersist
    void prePersist() {
        this.date = new Date();
    }

    @SuppressWarnings({"checkstyle:parametername", "checkstyle:needbraces",
            "checkstyle:operatorwrap"})
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoneActivity that = (DoneActivity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(user, that.user) &&
                Objects.equals(activity, that.activity) &&
                Objects.equals(date, that.date);
    }

}
