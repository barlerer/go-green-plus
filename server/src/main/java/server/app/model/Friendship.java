package server.app.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Friendships")
public class Friendship implements Serializable {

    @EmbeddedId
    protected FriendshipPk id = new FriendshipPk();

    @SuppressWarnings("checkstyle:declarationorder")
    @Temporal(TemporalType.TIMESTAMP)
    Date date;

    public Friendship() {
    }

    /**
     * @param id FriendshipPk containing the users that have a friendship.
     */
    public Friendship(FriendshipPk id) {
        this.id = id;
    }

    public FriendshipPk getId() {
        return id;
    }

    public void setId(FriendshipPk id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @PrePersist
    void prePersist() {
        this.date = new Date();
    }

    @SuppressWarnings({"checkstyle:parametername", "checkstyle:needbraces",
            "checkstyle:operatorwrap"})
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Friendship that = (Friendship) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(date, that.date);
    }

}
