package server.app.model;

import java.math.BigDecimal;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CO2Categories")
public class Co2Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "Kg_CO2_Emission", precision = 3, scale = 2)
    private BigDecimal kgCO2Emission;

    private int points;

    public Co2Category() {
    }

    /**
     * @param id for unique identification in the database.
     * @param kgCO2Emission the amount of co2 it reduces.
     * @param points the amount of point earned by doing an activity of this category.
     */
    public Co2Category(Long id, BigDecimal kgCO2Emission, int points) {
        this.id = id;
        this.kgCO2Emission = kgCO2Emission;
        this.points = points;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getKgCO2Emission() {
        return kgCO2Emission;
    }

    public void setKgCO2Emission(BigDecimal kgCO2Emission) {
        this.kgCO2Emission = kgCO2Emission;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * @param other the object for checking equality against.
     * @return true if the objects are the same or contain the same fields.
     */
    @SuppressWarnings({"checkstyle:parametername", "checkstyle:needbraces",
            "checkstyle:operatorwrap"})
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Co2Category that = (Co2Category) other;
        return points == that.points
            && id.equals(that.id)
            && Objects.equals(kgCO2Emission, that.kgCO2Emission);
    }

}
