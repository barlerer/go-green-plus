package server.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String hashedPassword;

    @Temporal(value = TemporalType.DATE)
    @Column(name = "join_date", updatable = false)
    private Date joinDate;

    private int totalPoints;

    private BigDecimal totalKgCo2;

    private boolean hasSolarPanel;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    private List<DoneActivity> doneActivities = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    private List<UserAchievement> userAchievements = new ArrayList<>();

    public User() {
    }

    /**
     * @param id to identify the user.
     * @param username to name a user.
     */
    public User(Long id, String username) {
        this.id = id;
        this.username = username;
        totalKgCo2 = new BigDecimal(0);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public boolean hasSolarPanel() {
        return hasSolarPanel;
    }

    public void setHasSolarPanel(boolean hasSolarPanel) {
        this.hasSolarPanel = hasSolarPanel;
    }

    public List<DoneActivity> getDoneActivities() {
        return doneActivities;
    }

    public void setDoneActivities(List<DoneActivity> doneActivities) {
        this.doneActivities = doneActivities;
    }

    public List<UserAchievement> getUserAchievements() {
        return userAchievements;
    }

    public void setUserAchievements(List<UserAchievement> userAchievements) {
        this.userAchievements = userAchievements;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public BigDecimal getTotalKgCo2() {
        return totalKgCo2;
    }

    public void setTotalKgCo2(BigDecimal totalKgCo2) {
        this.totalKgCo2 = totalKgCo2;
    }

    /**
     * @param doneActivity the activity that the user has done.
     */
    public void addDoneActivity(DoneActivity doneActivity) {
        doneActivities.add(doneActivity);
        int points = doneActivity.getActivity().getCo2Category().getPoints();
        BigDecimal totalKgCo2 = doneActivity.getActivity().getCo2Category().getKgCO2Emission();
        for (int i = 0; i < doneActivity.getQuantity(); i++) {
            this.setTotalPoints(this.getTotalPoints() + points);
            this.setTotalKgCo2(this.getTotalKgCo2().add(totalKgCo2));
        }
        doneActivity.setUser(this);
    }

    public void removeDoneActivity(DoneActivity doneActivity) {
        doneActivities.remove(doneActivity);
        doneActivity.setUser(null);
    }

    public void addUserAchievement(UserAchievement userAchievement) {
        userAchievements.add(userAchievement);
        userAchievement.setUser(this);
    }

    public void removeUserAchievement(UserAchievement userAchievement) {
        userAchievements.remove(userAchievement);
        userAchievement.setUser(null);
    }

    @PrePersist
    void prePersist() {
        this.joinDate = new Date();
        this.totalPoints = 0;
        this.totalKgCo2 = new BigDecimal(0);
    }

    @SuppressWarnings({"checkstyle:parametername", "checkstyle:needbraces",
            "checkstyle:operatorwrap"})
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User object = (User) o;
        return Objects.equals(id, object.id) &&
                Objects.equals(username, object.username);
    }

}
