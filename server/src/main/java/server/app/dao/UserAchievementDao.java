package server.app.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.app.model.UserAchievement;
import server.app.repository.UserAchievementRepository;

@Service
public class UserAchievementDao {

    @Autowired
    UserAchievementRepository userAchievementRepository;

    public UserAchievement save(UserAchievement userAchievement) {
        return userAchievementRepository.save(userAchievement);
    }

    public Iterable<UserAchievement> findAll() {
        return userAchievementRepository.findAll();
    }

    public UserAchievement findById(Long userAchievementId) {
        return userAchievementRepository.findById(userAchievementId).get();
    }

    /**
     * @param userAchievement specifies the userAchievement object that should be updated.
     * @param userAchievementDetails an object containing the new values
     *                               for the userAchievement object.
     * @return the userAchievement object updated with the details.
     */
    public UserAchievement update(
            UserAchievement userAchievement, UserAchievement userAchievementDetails) {

        userAchievement.setId(userAchievementDetails.getId());
        userAchievement.setUser(userAchievementDetails.getUser());
        userAchievement.setAchievement(userAchievementDetails.getAchievement());
        userAchievement.setDate(userAchievementDetails.getDate());

        return userAchievement;
    }

    public void delete(UserAchievement userAchievement) {
        userAchievementRepository.delete(userAchievement);
    }

}
