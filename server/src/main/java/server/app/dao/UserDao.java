package server.app.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.app.model.User;
import server.app.repository.UserRepository;

@Service
public class UserDao {

    @Autowired
    UserRepository userRepository;

    //save user
    public User save(User user) {
        return userRepository.save(user);
    }

    //search all users
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    /**
     * @param userId id of the user you want to retrieve.
     * @return user object.
     * @throws IllegalArgumentException if the user with specified id does not exist.
     */
    //get a user by id
    public User findById(Long userId) throws IllegalArgumentException {
        User user;

        if (userRepository.findById(userId).isPresent()) {
            user = userRepository.findById(userId).get();
        } else {
            throw new IllegalArgumentException("User not found");
        }

        return user;
    }

    /**
     * @param username of the user you want to retrieve.
     * @return user object.
     * @throws IllegalArgumentException if the user with specified username does not exist.
     */
    //get a user by username
    public User findByUsername(String username) throws IllegalArgumentException {
        User user;

        if (userRepository.findByUsername(username).isPresent()) {
            user = userRepository.findByUsername(username).get();
        } else {
            throw new IllegalArgumentException("User not found");
        }

        return user;
    }

    //delete a user
    public void delete(User user) {
        userRepository.delete(user);
    }

    /**
     * @param user specifies the user object that should be updated.
     * @param userDetails an object containing the new values for the user object.
     * @return the user object updated with the details.
     */
    public User update(User user, User userDetails) {

        user.setId(userDetails.getId());
        user.setUsername(userDetails.getUsername());

        return userRepository.save(user);
    }

}
