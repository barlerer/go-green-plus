package server.app.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.app.model.Achievement;
import server.app.repository.AchievementRepository;

@Service
public class AchievementDao {

    @Autowired
    AchievementRepository achievementRepository;

    public Achievement save(Achievement achievement) {
        return achievementRepository.save(achievement);
    }

    public Iterable<Achievement> findAll() {
        return achievementRepository.findAll();
    }

    public Achievement findById(Long achievementId) {
        return achievementRepository.findById(achievementId).get();
    }

    /**
     * @param achievement specifies the achievement object that should be updated.
     * @param achievementDetails an object containing the new values for the achievement object.
     * @return the achievement object updated with the details.
     */
    public Achievement update(
            Achievement achievement, Achievement achievementDetails) {

        achievement.setId(achievementDetails.getId());
        achievement.setName(achievementDetails.getName());
        achievement.setPoints(achievementDetails.getPoints());

        return achievement;
    }

    public void delete(Achievement achievement) {
        achievementRepository.delete(achievement);
    }
}
