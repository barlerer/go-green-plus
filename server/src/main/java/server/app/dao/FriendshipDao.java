package server.app.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import server.app.model.Friendship;
import server.app.model.FriendshipPk;
import server.app.repository.FriendshipRepository;

@Service
public class FriendshipDao {

    @Autowired
    FriendshipRepository friendshipRepository;

    public Friendship save(Friendship friendship) {
        return friendshipRepository.save(friendship);
    }

    public Friendship findById(FriendshipPk id) {
        return friendshipRepository.findById(id).get();
    }

    public Iterable<Friendship> findAll() {
        return friendshipRepository.findAll();
    }

    public Iterable<Friendship> findAllByUserIdAndStatus(Long id) {
        return friendshipRepository.findAllByUserIdAndStatus(id);
    }

    public void deleteById(FriendshipPk id) {
        friendshipRepository.deleteById(id);
    }

    @Transactional
    public void deleteById(Long userId1, Long userId2) {
        friendshipRepository.deleteById(userId1, userId2);
    }

}
