package server.app.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.app.model.Co2Category;
import server.app.repository.CO2CategoryRepository;

@Service
public class Co2CategoryDao {

    @Autowired
    CO2CategoryRepository co2CategoryRepository;

    public Co2Category save(Co2Category co2Category) {
        return co2CategoryRepository.save(co2Category);
    }

    public Iterable<Co2Category> findAll() {
        return co2CategoryRepository.findAll();
    }

    public Co2Category findById(Long co2CategoryId) {
        return co2CategoryRepository.findById(co2CategoryId).get();
    }

    /**
     * @param co2Category specifies the co2Category object that should be updated.
     * @param co2CategoryDetails an object containing the new values for the co2Category object.
     * @return the co2Category object updated with the details.
     */
    public Co2Category update(
        Co2Category co2Category, Co2Category co2CategoryDetails) {

        co2Category.setId(co2CategoryDetails.getId());
        co2Category.setKgCO2Emission(co2CategoryDetails.getKgCO2Emission());
        co2Category.setPoints(co2CategoryDetails.getPoints());

        return co2Category;
    }

    public void delete(Co2Category co2Category) {
        co2CategoryRepository.delete(co2Category);
    }
}
