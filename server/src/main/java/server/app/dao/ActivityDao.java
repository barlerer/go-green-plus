package server.app.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.app.model.Activity;
import server.app.repository.ActivityRepository;

@Service
public class ActivityDao {

    @Autowired
    ActivityRepository activityRepository;

    public Activity save(Activity activity) {
        return activityRepository.save(activity);
    }

    public Iterable<Activity> findAll() {
        return activityRepository.findAll();
    }

    public Activity findById(Long activityId) {
        return activityRepository.findById(activityId).get();
    }

    /**
     * @param activity specifies the activity object that should be updated.
     * @param activityDetails an object containing the new values for the activity object.
     * @return the activity object updated with the details.
     */
    public Activity update(
        Activity activity, Activity activityDetails) {

        activity.setId(activityDetails.getId());
        activity.setName(activityDetails.getName());
        activity.setCo2Category(activityDetails.getCo2Category());

        return activityRepository.save(activity);
    }

    public void delete(Activity activity) {
        activityRepository.delete(activity);
    }

}
