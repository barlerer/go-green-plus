package server.app.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.app.model.Activity;
import server.app.model.Co2Category;
import server.app.model.DoneActivity;
import server.app.model.User;
import server.app.repository.ActivityRepository;
import server.app.repository.CO2CategoryRepository;
import server.app.repository.DoneActivityRepository;
import server.app.repository.UserRepository;

import java.math.BigDecimal;

@Service
public class DoneActivityDao {

    @Autowired
    DoneActivityRepository doneActivityRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ActivityRepository activityRepository;

    @Autowired
    CO2CategoryRepository co2CategoryRepository;

    public DoneActivity save(DoneActivity doneActivity) {
        return doneActivityRepository.save(doneActivity);
    }

    public Iterable<DoneActivity> findAll() {
        return doneActivityRepository.findAll();
    }

    public DoneActivity findById(Long doneActivityId) {
        return doneActivityRepository.findById(doneActivityId).get();
    }

    /**
     * @param doneActivity the activity that should be deleted for the user.
     */
    public void delete(DoneActivity doneActivity) {
        User user = doneActivity.getUser();
        for (int i = 0; i < doneActivity.getQuantity(); i++) {
            user.setTotalPoints(user.getTotalPoints() - doneActivity.getActivity()
                .getCo2Category().getPoints());
            user.setTotalKgCo2(user.getTotalKgCo2().subtract(doneActivity.getActivity()
                .getCo2Category().getKgCO2Emission()));
        }
        userRepository.save(user);
        doneActivityRepository.delete(doneActivity);
    }

    /**
     * @param doneActivity specifies the doneActivity object that should be updated.
     * @param doneActivityDetails an object containing the new values for the doneActivity object.
     * @return the doneActivity object updated with the details.
     */
    public DoneActivity update(DoneActivity doneActivity, DoneActivity doneActivityDetails) {

        doneActivity.setId(doneActivityDetails.getId());
        doneActivity.setUser(doneActivityDetails.getUser());
        doneActivity.setActivity(doneActivityDetails.getActivity());

        return doneActivityRepository.save(doneActivity);
    }

    /**
     * @param doneActivity The object of which the earned points should be returned.
     * @return the amount of points earned by doing the activity.
     */
    public int getPoints(DoneActivity doneActivity) {
        DoneActivity doneActivity1 = doneActivityRepository.findById(doneActivity.getId()).get();
        Activity activity = activityRepository.findById(doneActivity1.getActivity().getId()).get();
        Co2Category co2Category = co2CategoryRepository.findById(activity.getId()).get();

        return co2Category.getPoints();
    }

    /**
     * @param doneActivity The object of which the saved co2 should be returned.
     * @return the amount of co2 saved by doing the activity.
     */
    public BigDecimal getKgCo2(DoneActivity doneActivity) {
        DoneActivity doneActivity1 = doneActivityRepository.findById(doneActivity.getId()).get();
        Activity activity = activityRepository.findById(doneActivity1.getActivity().getId()).get();
        Co2Category co2Category = co2CategoryRepository.findById(activity.getId()).get();

        return co2Category.getKgCO2Emission();
    }

}
