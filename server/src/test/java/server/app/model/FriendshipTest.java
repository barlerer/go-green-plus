package server.app.model;

import java.util.Date;
import org.junit.Before;
import org.junit.Test;

public class FriendshipTest {

  private User user1;
  private User user2;
  private User user3;
  FriendshipPk friendshipPk1;
  FriendshipPk friendshipPk2;
  Friendship friendship1;
  Friendship friendship2;

  @Before
  public void initObjects() {
    user1 = new User(1L, "Steve");
    user2 = new User(2L, "Peter");
    user3 = new User(3L, "Alice");
    friendshipPk1 = new FriendshipPk(user1, user2);
    friendshipPk2 = new FriendshipPk(user3, user2);
    friendship1 = new Friendship(friendshipPk1);
    friendship2 = new Friendship(friendshipPk2);
  }

  @Test
  public void testSetId() {
    friendship1.setId(friendshipPk2);
    assert(friendship1.getId().equals(friendshipPk2));
  }

  @Test
  public void testSetDate() {
    friendship1.setDate(new Date(000000000L));
    assert(friendship1.getDate().equals(new Date(000000000L)));
  }

  @Test
  public void testEqualsSelf() {
    assert(friendship1.equals(friendship1));
  }

  @Test
  public void testEqualsNull() {
    assert(!friendship1.equals(null));
  }

  @Test
  public void testEqualsDifferentObject() {
    assert(!friendship1.equals(new Object()));
  }

  @Test
  public void testEqualsDifferentObjectSameFields() {
    Friendship friendship3 = new Friendship(friendshipPk1);
    assert(friendship1.equals(friendship3));
  }

  @Test
  public void testEqualsDifferentId() {
    Friendship friendship3 = new Friendship(friendshipPk1);
    friendship3.setId(friendshipPk2);
    assert(!friendship1.equals(friendship3));
  }

  @Test
  public void testEqualsDifferentDate() {
    Friendship friendship3 = new Friendship(friendshipPk1);
    friendship3.setDate(new Date(0000000000L));
    assert(!friendship1.equals(friendship3));
  }

}
