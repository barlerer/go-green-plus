package server.app.model;

import java.math.BigDecimal;
import org.junit.Test;

public class ActivityTest {

  @Test
  public void testConstructorDefault() {
    Activity activity = new Activity();
    assert(activity.getId() == null);
    assert(activity.getName() == null);
    assert(activity.getCo2Category() == null);
  }

  @Test
  public void testConstructor() {
    Co2Category co2Category = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    Activity activity = new Activity(Long.valueOf(1), "Eating a vegetarian meal", co2Category);
    assert(activity.getId().equals(Long.valueOf(1)));
    assert(activity.getName().equals("Eating a vegetarian meal"));
    assert(activity.getCo2Category().equals(co2Category));
  }

  @Test
  public void testSetId() {
    Co2Category co2Category = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    Activity activity = new Activity(Long.valueOf(1), "Eating a vegetarian meal", co2Category);
    activity.setId(Long.valueOf(2));
    assert(activity.getId().equals(Long.valueOf(2)));
  }

  @Test
  public void testSetCo2Category() {
    Co2Category co2Category1 = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    Co2Category co2Category2 = new Co2Category(Long.valueOf(2), BigDecimal.valueOf(0.84), 27);
    Activity activity = new Activity(Long.valueOf(1), "Eating a vegetarian meal", co2Category1);
    activity.setCo2Category(co2Category2);
    assert(activity.getCo2Category().equals(co2Category2));
  }

  @Test
  public void testSetName() {
    Co2Category co2Category = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    Activity activity = new Activity(Long.valueOf(1), "Eating a vegetarian meal", co2Category);
    activity.setName("Riding your bike");
    assert(activity.getName().equals("Riding your bike"));
  }

  @Test
  public void testEqualsSameObject() {
    Co2Category co2Category = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    Activity activity1 = new Activity(Long.valueOf(1), "Eating a vegetarian meal", co2Category);
    assert(activity1.equals(activity1));
  }

  @Test
  public void testEqualsOtherClass() {
    Co2Category co2Category = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    Activity activity1 = new Activity(Long.valueOf(1), "Eating a vegetarian meal", co2Category);
    assert(!activity1.equals(new Object()));
  }

  @Test
  public void testEqualsNull() {
    Co2Category co2Category = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    Activity activity1 = new Activity(Long.valueOf(1), "Eating a vegetarian meal", co2Category);
    assert(!activity1.equals(null));
  }

  @Test
  public void testEqualsDifferentObjectSameFields() {
    Co2Category co2Category = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    Activity activity1 = new Activity(Long.valueOf(1), "Eating a vegetarian meal", co2Category);
    Activity activity2 = new Activity(Long.valueOf(1), "Eating a vegetarian meal", co2Category);
    assert(activity1.equals(activity2));
  }

  @Test
  public void testDifferentObjectDifferentId() {
    Co2Category co2Category = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    Activity activity1 = new Activity(Long.valueOf(1), "Eating a vegetarian meal", co2Category);
    Activity activity2 = new Activity(Long.valueOf(2), "Eating a vegetarian meal", co2Category);
    assert(!activity1.equals(activity2));
  }

  @Test
  public void testDifferentObjectName() {
    Co2Category co2Category = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    Activity activity1 = new Activity(Long.valueOf(1), "Eating a vegetarian meal", co2Category);
    Activity activity2 = new Activity(Long.valueOf(1), "Riding your bike", co2Category);
    assert(!activity1.equals(activity2));
  }

  @Test
  public void testDifferentObjectDifferentCo2Category() {
    Co2Category co2Category1 = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    Co2Category co2Category2 = new Co2Category(Long.valueOf(2), BigDecimal.valueOf(0.84), 27);
    Activity activity1 = new Activity(Long.valueOf(1), "Eating a vegetarian meal", co2Category1);
    Activity activity2 = new Activity(Long.valueOf(1), "Eating a vegetarian meal", co2Category2);
    assert(!activity1.equals(activity2));
  }

}
