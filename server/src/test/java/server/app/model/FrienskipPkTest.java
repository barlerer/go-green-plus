package server.app.model;

import org.junit.Before;
import org.junit.Test;

public class FrienskipPkTest {

  private User user1;
  private User user2;
  private User user3;

  @Before
  public void initObjects() {
    user1 = new User(1L, "Steve");
    user2 = new User(2L, "Peter");
    user3 = new User(3L, "Alice");
  }

  @Test
  public void testConstructorDefault() {
    FriendshipPk friendshipPk = new FriendshipPk();
    assert(friendshipPk.getUser1() == null);
    assert(friendshipPk.getUser2() == null);
  }

  @Test
  public void testConstructor() {
    FriendshipPk friendshipPk = new FriendshipPk(user1, user2);
    assert(friendshipPk.getUser1().equals(user1));
    assert(friendshipPk.getUser2().equals(user2));
  }

  @Test
  public void testSetUser1() {
    FriendshipPk friendshipPk = new FriendshipPk(user1, user2);
    friendshipPk.setUser1(user3);
    assert(friendshipPk.getUser1().equals(user3));
  }

  @Test
  public void testSetUser2() {
    FriendshipPk friendshipPk = new FriendshipPk(user1, user2);
    friendshipPk.setUser2(user3);
    assert(friendshipPk.getUser2().equals(user3));
  }

  @Test
  public void testEqualsSelf() {
    FriendshipPk friendshipPk1 = new FriendshipPk(user1, user2);
    FriendshipPk friendshipPk2 = new FriendshipPk(user1, user2);
    assert(friendshipPk1.equals(friendshipPk1));
  }

  @Test
  public void testEqualsNull() {
    FriendshipPk friendshipPk1 = new FriendshipPk(user1, user2);
    FriendshipPk friendshipPk2 = new FriendshipPk(user1, user2);
    assert(!friendshipPk1.equals(null));
  }

  @Test
  public void testEqualsDifferentObject() {
    FriendshipPk friendshipPk1 = new FriendshipPk(user1, user2);
    FriendshipPk friendshipPk2 = new FriendshipPk(user1, user2);
    assert(!friendshipPk1.equals(new Object()));
  }

  @Test
  public void testEqualsDifferentObjectSameFields() {
    FriendshipPk friendshipPk1 = new FriendshipPk(user1, user2);
    FriendshipPk friendshipPk2 = new FriendshipPk(user1, user2);
    assert(friendshipPk1.equals(friendshipPk2));
  }

  @Test
  public void testEqualsDifferentUser1() {
    FriendshipPk friendshipPk1 = new FriendshipPk(user1, user2);
    FriendshipPk friendshipPk2 = new FriendshipPk(user3, user2);
    assert(!friendshipPk1.equals(friendshipPk2));
  }

  @Test
  public void testEqualsDifferentUser2() {
    FriendshipPk friendshipPk1 = new FriendshipPk(user1, user2);
    FriendshipPk friendshipPk2 = new FriendshipPk(user1, user3);
    assert(!friendshipPk1.equals(friendshipPk2));
  }

}
