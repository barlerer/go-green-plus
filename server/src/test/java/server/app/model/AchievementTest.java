package server.app.model;

import org.junit.Test;

public class AchievementTest {

  @Test
  public void testConstructorDefault() {
    Achievement achievement = new Achievement();
    assert(achievement.getId() == null);
    assert(achievement.getName() == null);
    assert(achievement.getPoints() == 0);
  }

  @Test
  public void testConstructor() {
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    assert(achievement.getId().equals(1L));
    assert(achievement.getName().equals("7 days of activities"));
    assert(achievement.getPoints() == 20);
  }

  @Test
  public void testSetId() {
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    achievement.setId(2L);
    assert(achievement.getId().equals(2L));
  }

  @Test
  public void testSetName() {
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    achievement.setName("14 days of activities");
    assert(achievement.getName().equals("14 days of activities"));
  }

  @Test
  public void testSetPoints() {
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    achievement.setPoints(30);
    assert(achievement.getPoints() == 30);
  }

  @Test
  public void testEqualsSelf() {
    Achievement achievement1 = new Achievement(1L, "7 days of activities", 20);
    Achievement achievement2 = new Achievement(2L, "14 days of activities", 30);
    assert(achievement1.equals(achievement1));
  }

  @Test
  public void testEqualsNull() {
    Achievement achievement1 = new Achievement(1L, "7 days of activities", 20);
    Achievement achievement2 = new Achievement(2L, "14 days of activities", 30);
    assert(!achievement1.equals(null));
  }

  @Test
  public void testEqualsDifferentObject() {
    Achievement achievement1 = new Achievement(1L, "7 days of activities", 20);
    Achievement achievement2 = new Achievement(2L, "14 days of activities", 30);
    assert(!achievement1.equals(new Object()));
  }

  @Test
  public void testEqualsDifferentObjectSameFields() {
    Achievement achievement1 = new Achievement(1L, "7 days of activities", 20);
    Achievement achievement2 = new Achievement(1L, "7 days of activities", 20);
    assert(achievement1.equals(achievement2));
  }

  @Test
  public void testEqualsDifferentId() {
    Achievement achievement1 = new Achievement(1L, "7 days of activities", 20);
    Achievement achievement2 = new Achievement(2L, "7 days of activities", 20);
    assert(!achievement1.equals(achievement2));
  }

  @Test
  public void testEqualsDifferentName() {
    Achievement achievement1 = new Achievement(1L, "7 days of activities", 20);
    Achievement achievement2 = new Achievement(1L, "14 days of activities", 20);
    assert(!achievement1.equals(achievement2));
  }

  @Test
  public void testEqualsDifferentPoints() {
    Achievement achievement1 = new Achievement(1L, "7 days of activities", 20);
    Achievement achievement2 = new Achievement(1L, "7 days of activities", 30);
    assert(!achievement1.equals(achievement2));
  }

}
