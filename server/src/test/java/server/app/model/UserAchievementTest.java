package server.app.model;

import java.util.Date;
import org.junit.Test;

public class UserAchievementTest {

  @Test
  public void testConstructorDefault() {
    UserAchievement userAchievement = new UserAchievement();
    assert(userAchievement.getId() == null);
    assert(userAchievement.getAchievement() == null);
    assert(userAchievement.getDate() == null);
  }

  @Test
  public void testConstructor() {
    User user = new User(1L, "Steve");
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    UserAchievement userAchievement = new UserAchievement(user, achievement, new Date(2323223232L));
    assert(userAchievement.getAchievement().equals(achievement));
    assert(userAchievement.getDate().equals(new Date(2323223232L)));
  }

  @Test
  public void testSetUser() {
    User user = new User(1L, "Steve");
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    UserAchievement userAchievement = new UserAchievement(user, achievement, new Date(2323223232L));
    User user2 = new User(2L, "Peter");
    userAchievement.setUser(user2);
    assert(userAchievement.getUser().equals(user2));
  }

  @Test
  public void testSetId() {
    User user = new User(1L, "Steve");
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    UserAchievement userAchievement = new UserAchievement(user, achievement, new Date(2323223232L));
    userAchievement.setId(2L);
    assert(userAchievement.getId().equals(2L));
  }

  @Test
  public void testSetAchievement() {
    User user = new User(1L, "Steve");
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    UserAchievement userAchievement = new UserAchievement(user, achievement, new Date(2323223232L));
    Achievement achievement2 = new Achievement(2L, "14 days of activities", 30);
    userAchievement.setAchievement(achievement2);
    assert(userAchievement.getAchievement().equals(achievement2));
  }

  @Test
  public void testSetDate() {
    User user = new User(1L, "Steve");
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    UserAchievement userAchievement = new UserAchievement(user, achievement, new Date(2323223232L));
    userAchievement.setDate(new Date(1212121212L));
    assert(userAchievement.getDate().equals(new Date(1212121212L)));
  }

  @Test
  public void testEqualsSelf() {
    User user = new User(1L, "Steve");
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    UserAchievement userAchievement = new UserAchievement(user, achievement, new Date(2323223232L));
    assert(userAchievement.equals(userAchievement));
  }

  @Test
  public void testEqualsNull() {
    User user = new User(1L, "Steve");
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    UserAchievement userAchievement = new UserAchievement(user, achievement, new Date(2323223232L));
    assert(!userAchievement.equals(null));
  }

  @Test
  public void testEqualsDifferentObject() {
    User user = new User(1L, "Steve");
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    UserAchievement userAchievement = new UserAchievement(user, achievement, new Date(2323223232L));
    assert(!userAchievement.equals(new Object()));
  }

  @Test
  public void testEqualsDifferentObjectSameFields() {
    User user = new User(1L, "Steve");
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    UserAchievement userAchievement = new UserAchievement(user, achievement, new Date(2323223232L));
    UserAchievement userAchievement2 = new UserAchievement(user, achievement, new Date(2323223232L));
    assert(userAchievement.equals(userAchievement2));
  }

  @Test
  public void testEqualsDifferentUser() {
    User user = new User(1L, "Steve");
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    UserAchievement userAchievement = new UserAchievement(user, achievement, new Date(2323223232L));
    User user2 = new User(2L, "Peter");
    UserAchievement userAchievement2 = new UserAchievement(user2, achievement, new Date(2323223232L));
    assert(!userAchievement.equals(userAchievement2));
  }

  @Test
  public void testEqualsDifferentAchievement() {
    User user = new User(1L, "Steve");
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    UserAchievement userAchievement = new UserAchievement(user, achievement, new Date(2323223232L));
    Achievement achievement2 = new Achievement(2L, "14 days of activities", 30);
    UserAchievement userAchievement2 = new UserAchievement(user, achievement2, new Date(2323223232L));
    assert(!userAchievement.equals(userAchievement2));
  }

  @Test
  public void testEqualsDifferentDate() {
    User user = new User(1L, "Steve");
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    UserAchievement userAchievement = new UserAchievement(user, achievement, new Date(2323223232L));
    UserAchievement userAchievement2 = new UserAchievement(user, achievement, new Date(1212121212L));
    assert(!userAchievement.equals(userAchievement2));
  }

  @Test
  public void testEqualsDifferentId() {
    User user = new User(1L, "Steve");
    Achievement achievement = new Achievement(1L, "7 days of activities", 20);
    UserAchievement userAchievement = new UserAchievement(user, achievement, new Date(2323223232L));
    UserAchievement userAchievement2 = new UserAchievement(user, achievement, new Date(2323223232L));
    userAchievement2.setId(2L);
    assert(!userAchievement.equals(userAchievement2));
  }

}
