package server.app.model;

import org.junit.Before;
import org.junit.Test;

public class UserHashedPasswordOnlyTest {

  private UserHashedPasswordOnly passwordHash;
  private UserHashedPasswordOnly passwordHashSame;

  @Before
  public void initObjects() {
    passwordHash = new UserHashedPasswordOnly(1L, "hf8349f93h");
    passwordHashSame = new UserHashedPasswordOnly(1L, "hf8349f93h");
  }

  @Test
  public void testConstructor() {
    assert(passwordHash.getId().equals(1L));
    assert(passwordHash.getHashedPassword().equals("hf8349f93h"));
  }

  @Test
  public void testSetId() {
    passwordHash.setId(2L);
    assert(passwordHash.getId().equals(2L));
  }

  @Test
  public void testSetHash() {
    passwordHash.setHashedPassword("fb5e0d73f8");
    assert(passwordHash.getHashedPassword().equals("fb5e0d73f8"));
  }

  @Test
  public void testEqualsSelf() {
    assert(passwordHash.equals(passwordHash));
  }

  @Test
  public void testEqualsNull() {
    assert(!passwordHash.equals(null));
  }

  @Test
  public void testEqualsDifferentObject() {
    assert(!passwordHash.equals(new Object()));
  }

  @Test
  public void testEqualsDifferentObjectSameFields() {
    assert(passwordHash.equals(passwordHashSame));
  }

  @Test
  public void testEqualsDifferentId() {
    passwordHashSame.setId(2L);
    assert(!passwordHash.equals(passwordHashSame));
  }

  @Test
  public void testEqualsDifferentName() {
    passwordHashSame.setHashedPassword("fb5e0d73f8");
    assert(!passwordHash.equals(passwordHashSame));
  }

}
