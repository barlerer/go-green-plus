package server.app.model;

import java.util.Date;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class DoneActivityTest {

    private User user1;
    private User user2;
    private Activity activity1;
    private Activity activity2;
    private DoneActivity doneActivity1;
    private DoneActivity doneActivity2;

    @Before
    public void init() {
        user1 = new User(1L, "Steve");
        user2 = new User(2L, "Peter");
        Co2Category category1 = new Co2Category(1L, BigDecimal.valueOf(0.63), 20);
        Co2Category category2 = new Co2Category(2L, BigDecimal.valueOf(2.52), 80);
        activity1 = new Activity(1L, "Eating a vegetarian meal", category1);
        activity2 = new Activity(2L, "Cycling instead of driving", category2);
        doneActivity1 = new DoneActivity(null, null, null);
        doneActivity2 = new DoneActivity(null, null, null);
    }

    @Test
    public void testConstructorEmpty() {
        new DoneActivity();
    }

    @Test
    public void testConstructorDefault() {
        doneActivity1 = new DoneActivity(1L, user1, activity1);
        assert(doneActivity1.getId().equals(1L));
        assert(doneActivity1.getUser().getId().equals(1L));
        assert(doneActivity1.getUser().getUsername().equals("Steve"));
        assert(doneActivity1.getActivity().getId().equals(1L));
        assert(doneActivity1.getActivity().getName().equals("Eating a vegetarian meal"));
        assert(doneActivity1.getActivity().getCo2Category().getId().equals(1L));
        assert(doneActivity1.getActivity().getCo2Category().getKgCO2Emission().equals(BigDecimal.valueOf(0.63)));
//        assert(doneActivity1.getActivity().getCo2Category().getPoints().equals(20));
    }

    @Test
    public void testSetId() {
        doneActivity1.setId(2L);
        assert(doneActivity1.getId().equals(2L));
    }

    @Test
    public void testSetUser() {
        doneActivity1.setUser(user1);
        assert(doneActivity1.getUser().getId().equals(1L));
        assert(doneActivity1.getUser().getUsername().equals("Steve"));
    }

    @Test
    public void testSetActivity() {
        doneActivity1.setActivity(activity1);
        assert(doneActivity1.getActivity().getId().equals(1L));
        assert(doneActivity1.getActivity().getName().equals("Eating a vegetarian meal"));
        assert(doneActivity1.getActivity().getCo2Category().getId().equals(1L));
        assert(doneActivity1.getActivity().getCo2Category().getKgCO2Emission().equals(BigDecimal.valueOf(0.63)));
        assert(doneActivity1.getActivity().getCo2Category().getPoints() == 20);
    }

    @Test
    public void testSetDate() {
        doneActivity1.setDate(new Date(2323223232L));
        assert(doneActivity1.getDate().equals(new Date(2323223232L)));
    }

    @Test
    public void testSetQuantity() {
        doneActivity1.setQuantity(3);
        assert(doneActivity1.getQuantity() == 3);
    }

    @Test
    public void testEqualsSameObject() {
        assert(doneActivity1.equals(doneActivity1));
    }

    @Test
    public void testEqualsOtherClass() {
        assert(!doneActivity1.equals(new Object()));
    }

    @Test
    public void testEqualsNull() {
        assert(!doneActivity1.equals(null));
    }

    @Test
    public void testEqualsDifferentObjectSameFields() {
        doneActivity1 = new DoneActivity(1L, user1, activity1);
        doneActivity1.setDate(new Date(2323223232L));
        DoneActivity sameFields = new DoneActivity(1L, user1, activity1);
        sameFields.setDate(new Date(2323223232L));
        assert(doneActivity1.equals(sameFields));
    }

    @Test
    public void testEqualsDifferentId() {
        doneActivity1 = new DoneActivity(1L, user1, activity1);
        DoneActivity sameFields = new DoneActivity(2L, user1, activity1);
        assert(!doneActivity1.equals(sameFields));
    }

    @Test
    public void testEqualsDifferentUser() {
        doneActivity1 = new DoneActivity(1L, user1, activity1);
        DoneActivity sameFields = new DoneActivity(1L, user2, activity1);
        assert(!doneActivity1.equals(sameFields));
    }

    @Test
    public void testEqualsDifferentActivity() {
        doneActivity1 = new DoneActivity(1L, user1, activity1);
        DoneActivity sameFields = new DoneActivity(1L, user1, activity2);
        assert(!doneActivity1.equals(sameFields));
    }

    @Test
    public void testEqualsDifferentDate() {
        doneActivity1 = new DoneActivity(1L, user1, activity1);
        doneActivity1.setDate(new Date(2323223232L));
        DoneActivity sameFields = new DoneActivity(1L, user1, activity1);
        sameFields.setDate(new Date(21212121212L));
        assert(!doneActivity1.equals(sameFields));
    }
}
