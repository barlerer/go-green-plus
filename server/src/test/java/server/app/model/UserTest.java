package server.app.model;

import java.util.Date;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class UserTest {

    private User user1;
    private User user2;
    private DoneActivity doneActivity1;
    private DoneActivity doneActivity2;
    private List<DoneActivity> doneActivities = new ArrayList<>();
    Achievement achievement1;
    Achievement achievement2;
    UserAchievement userAchievement1;
    UserAchievement userAchievement2;
    private List<UserAchievement> userAchievements = new ArrayList<>();

    @Before
    public void init() {
        user1 = new User(1L, "Steve");
        user2 = new User(2L, "Peter");
        Co2Category category1 = new Co2Category(1L, BigDecimal.valueOf(0.63), 20);
        Co2Category category2 = new Co2Category(2L, BigDecimal.valueOf(2.52), 80);
        Activity activity1 = new Activity(1L, "Eating a vegetarian meal", category1);
        Activity activity2 = new Activity(2L, "Cycling instead of driving", category2);
        doneActivity1 = new DoneActivity(1L, user1, activity1);
        doneActivity2 = new DoneActivity(2L, user1, activity2);
        doneActivities.add(doneActivity1);
        doneActivities.add(doneActivity2);
        achievement1 = new Achievement(1L, "7 days of activities", 20);
        achievement2 = new Achievement(2L, "14 days of activities", 30);
        userAchievement1 = new UserAchievement(user1, achievement1, new Date(2323223232L));
        userAchievement2 = new UserAchievement(user2, achievement2, new Date(2323223232L));
        userAchievements.add(userAchievement1);
        userAchievements.add(userAchievement2);
    }

    @Test
    public void testConstructorDefault() {
        assert(user1.getId().equals(1L));
        assert(user1.getUsername().equals("Steve"));
    }

    @Test
    public void testSetId() {
        user1.setId(2L);
        assert(user1.getId().equals(2L));
    }

    @Test
    public void testSetUsername() {
        user1.setUsername("Peter");
        assert(user1.getUsername().equals("Peter"));
    }

    @Test
    public void testSetDoneActivity() {
        user1.setDoneActivities(doneActivities);
        assert(user1.getDoneActivities().get(0).equals(doneActivity1));
        assert(user1.getDoneActivities().get(1).equals(doneActivity2));
    }

    @Test
    public void testAddDoneActivity() {
        user1.addDoneActivity(doneActivity1);
        assert(user1.getDoneActivities().get(0).equals(doneActivity1));
    }

    @Test
    public void testRemoveDoneActivity() {
        user1.addDoneActivity(doneActivity1);
        user1.removeDoneActivity(doneActivity1);
        assert(user1.getDoneActivities().isEmpty());
    }

    @Test
    public void testSetAchievement() {
        user1.setUserAchievements(userAchievements);
        assert(user1.getUserAchievements().get(0).equals(userAchievement1));
        assert(user1.getUserAchievements().get(1).equals(userAchievement2));
    }

    @Test
    public void testAddAchievement() {
        user1.addUserAchievement(userAchievement1);
        assert(user1.getUserAchievements().get(0).equals(userAchievement1));
    }

    @Test
    public void testRemoveAchievement() {
        user1.addUserAchievement(userAchievement1);
        user1.removeUserAchievement(userAchievement1);
        assert(user1.getUserAchievements().isEmpty());
    }

    @Test
    public void testSetHashPassword() {
        user1.setHashedPassword("hf8349f93h");
        assert(user1.getHashedPassword().equals("hf8349f93h"));
    }

    @Test
    public void testSetJoinDate() {
        user1.setJoinDate(new Date(2323223232L));
        assert(user1.getJoinDate().equals(new Date(2323223232L)));
    }

    @Test
    public void testSetHasSolarPanel() {
        user1.setHasSolarPanel(true);
        assert(user1.hasSolarPanel());
        user1.setHasSolarPanel(false);
        assert(!user1.hasSolarPanel());
    }

    @Test
    public void testSetUserAchievements() {
        user1.setUserAchievements(userAchievements);
        assert(user1.getUserAchievements().equals(userAchievements));
    }

    @Test
    public void testEqualsSameObject() {
        assert(user1.equals(user1));
    }

    @Test
    public void testEqualsOtherClass() {
        assert(!user1.equals(new Object()));
    }

    @Test
    public void testEqualsNull() {
        assert(!user1.equals(null));
    }

    @Test
    public void testEqualsDifferentObjectSameFields() {
        User userSame = new User(1L, "Steve");
        assert(user1.equals(userSame));
    }

    @Test
    public void testDifferentObjectDifferentId() {
        User userSteve2 = new User(2L, "Steve");
        assert(!user1.equals(userSteve2));
    }

    @Test
    public void testDifferentObjectDifferentUsername() {
        User userPeter1 = new User(1L, "Peter");
        assert(!user1.equals(userPeter1));
    }

}
