package server.app.model;

import org.junit.Test;

import java.math.BigDecimal;

public class Co2CategoryTest {

  @Test
  public void testConstructorDefault() {
    Co2Category co2Category = new Co2Category();
    assert(co2Category.getId() == null);
    assert(co2Category.getKgCO2Emission() == null);
    assert(co2Category.getPoints() == 0);
  }

  @Test
  public void testConstructor() {
    Co2Category co2Category = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    assert(co2Category.getId().equals(Long.valueOf(1)));
    assert(co2Category.getKgCO2Emission().equals(BigDecimal.valueOf(0.63)));
    assert(co2Category.getPoints() == 20);
  }

  @Test
  public void testSetId() {
    Co2Category co2Category = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    co2Category.setId(Long.valueOf(2));
    assert(co2Category.getId().equals(Long.valueOf(2)));
  }

  @Test
  public void testSetKgCO2Emission() {
    Co2Category co2Category = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    co2Category.setKgCO2Emission(BigDecimal.valueOf(0.84));
    assert(co2Category.getKgCO2Emission().equals(BigDecimal.valueOf(0.84)));
  }

  @Test
  public void testSetPoints() {
    Co2Category co2Category = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    co2Category.setPoints(27);
    assert(co2Category.getPoints() == 27);
  }

  @Test
  public void testEqualsSameObject() {
    Co2Category co2Category1 = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    assert(co2Category1.equals(co2Category1));
  }

  @Test
  public void testEqualsOtherClass() {
    Co2Category co2Category1 = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    assert(!co2Category1.equals(new Object()));
  }

  @Test
  public void testEqualsNull() {
    Co2Category co2Category1 = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    assert(!co2Category1.equals(null));
  }

  @Test
  public void testEqualsDifferentObjectSameFields() {
    Co2Category co2Category1 = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    Co2Category co2Category2 = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    assert(co2Category1.equals(co2Category2));
  }

  @Test
  public void testDifferentObjectDifferentId() {
    Co2Category co2Category1 = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    Co2Category co2Category2 = new Co2Category(Long.valueOf(2), BigDecimal.valueOf(0.63), 20);
    assert(!co2Category1.equals(co2Category2));
  }

  @Test
  public void testDifferentObjectDifferentKgCo2() {
    Co2Category co2Category1 = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    Co2Category co2Category2 = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.84), 20);
    assert(!co2Category1.equals(co2Category2));
  }

  @Test
  public void testDifferentObjectDifferentPoints() {
    Co2Category co2Category1 = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 20);
    Co2Category co2Category2 = new Co2Category(Long.valueOf(1), BigDecimal.valueOf(0.63), 27);
    assert(!co2Category1.equals(co2Category2));
  }
}
