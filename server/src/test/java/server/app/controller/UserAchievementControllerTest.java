package server.app.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import server.app.dao.UserAchievementDao;
import server.app.model.UserAchievement;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class UserAchievementControllerTest extends BasicControllerTest{

  private MockMvc mockMvc;

  @Mock
  private UserAchievementDao mockDao;

  @InjectMocks
  private UserAchievementController controller;

  @Before
  public void init(){
    mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    url = "/userAchievement";
  }

  @Test
  public void testGetAll() throws Exception {

    List<UserAchievement> achievements = Arrays.asList(userAchievement1, userAchievement2);

    doReturn(achievements).when(mockDao).findAll();

    this.mockMvc.perform(get(url))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].user.id", is(1)))
        .andExpect(jsonPath("$[0].user.username", is("Steve")))
        .andExpect(jsonPath("$[0].achievement.id", is(1)))
        .andExpect(jsonPath("$[0].achievement.name", is("7 days of activities")))
        .andExpect(jsonPath("$[0].achievement.points", is(20)))
        .andExpect(jsonPath("$[1].user.id", is(2)))
        .andExpect(jsonPath("$[1].user.username", is("Peter")))
        .andExpect(jsonPath("$[1].achievement.id", is(2)))
        .andExpect(jsonPath("$[1].achievement.name", is("14 days of activities")))
        .andExpect(jsonPath("$[1].achievement.points", is(30)));
    verify(mockDao, times(1)).findAll();
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testCreate() throws Exception {
    doReturn(userAchievement1).when(mockDao).save(userAchievement1);
    mockMvc.perform(
        post(url)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(userAchievement1)))
        .andExpect(status().isOk());
    verify(mockDao, times(1)).save(userAchievement1);
    verifyNoMoreInteractions(mockDao);
  }

}
