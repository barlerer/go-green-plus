package server.app.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Date;
import org.junit.Before;
import server.app.model.Achievement;
import server.app.model.Activity;
import server.app.model.Co2Category;
import server.app.model.DoneActivity;
import server.app.model.Friendship;
import server.app.model.FriendshipPk;
import server.app.model.User;
import server.app.model.UserAchievement;

import java.math.BigDecimal;

public class BasicControllerTest {

    User user1;
    User user2;
    Co2Category category1;
    Co2Category category2;
    Activity activity1;
    Activity activity2;
    DoneActivity doneActivity1;
    DoneActivity doneActivity2;
    DoneActivity doneActivity3;
    Achievement achievement1;
    Achievement achievement2;
    UserAchievement userAchievement1;
    UserAchievement userAchievement2;
    FriendshipPk friendship1Pk;
    Friendship friendship1;

    String url;

    @Before
    public void initObjects(){
        user1 = new User(1L, "Steve");
        user2 = new User(2L, "Peter");
        category1 = new Co2Category(1L, BigDecimal.valueOf(0.63), 20);
        category2 = new Co2Category(2L, BigDecimal.valueOf(2.52), 80);
        activity1 = new Activity(1L, "Eating a vegetarian meal", category1);
        activity2 = new Activity(2L, "Cycling instead of driving", category2);
        doneActivity1 = new DoneActivity(1L, user1, activity1, 1);
        doneActivity2 = new DoneActivity(2L, user1, activity2, 1);
        doneActivity3 = new DoneActivity(3L, user2, activity1, 1);
        achievement1 = new Achievement(1L, "7 days of activities", 20);
        achievement2 = new Achievement(2L, "14 days of activities", 30);
        userAchievement1 = new UserAchievement(user1, achievement1, new Date(2323223232L));
        userAchievement2 = new UserAchievement(user2, achievement2, new Date(2323223232L));
        friendship1Pk = new FriendshipPk(user1, user2);
        friendship1 = new Friendship(friendship1Pk);
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
