package server.app.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import server.app.dao.ActivityDao;
import server.app.dao.Co2CategoryDao;
import server.app.model.Activity;
import server.app.model.Co2Category;

@RunWith(MockitoJUnitRunner.class)
public class ActivityControllerTest extends BasicControllerTest{

  private MockMvc mockMvc;

  @Mock
  private ActivityDao mockDao;

  @InjectMocks
  private ActivityController controller;

  @Before
  public void init(){
    mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    url = "/activity";
  }

  @Test
  public void testGetAll() throws Exception {

    List<Activity> activities = Arrays.asList(activity1, activity2);

    doReturn(activities).when(mockDao).findAll();

    this.mockMvc.perform(get(url))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].id", is(1)))
        .andExpect(jsonPath("$[0].name", is("Eating a vegetarian meal")))
        .andExpect(jsonPath("$[0].co2Category.id", is(1)))
        .andExpect(jsonPath("$[0].co2Category.kgCO2Emission", is(0.63)))
        .andExpect(jsonPath("$[0].co2Category.points", is(20)))
        .andExpect(jsonPath("$[1].id", is(2)))
        .andExpect(jsonPath("$[1].name", is("Cycling instead of driving")))
        .andExpect(jsonPath("$[1].co2Category.id", is(2)))
        .andExpect(jsonPath("$[1].co2Category.kgCO2Emission", is(2.52)))
        .andExpect(jsonPath("$[1].co2Category.points", is(80)));
    verify(mockDao, times(1)).findAll();
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testGetByIdSucces() throws Exception {
    doReturn(activity1).when(mockDao).findById(Long.valueOf(1));
    mockMvc.perform(get(url + "/{id}", 1))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id", is(1)))
        .andExpect(jsonPath("$.name", is("Eating a vegetarian meal")))
        .andExpect(jsonPath("$.co2Category.id", is(1)))
        .andExpect(jsonPath("$.co2Category.kgCO2Emission", is(0.63)))
        .andExpect(jsonPath("$.co2Category.points", is(20)));
    verify(mockDao, times(1)).findById(Long.valueOf(1));
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testGetByIdFail404NotFound() throws Exception {
    doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
    mockMvc.perform(get(url + "/{id}", 1))
        .andExpect(status().isNotFound());
    verify(mockDao, times(1)).findById(Long.valueOf(1));
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testCreateActivity() throws Exception {
    doReturn(activity1).when(mockDao).save(activity1);
    mockMvc.perform(
        post(url)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(activity1)))
        .andExpect(status().isOk());
    verify(mockDao, times(1)).save(activity1);
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testUpdateSucces() throws Exception {
    activity2.setId(1L);
    doReturn(activity1).when(mockDao).findById(activity1.getId());
    mockMvc.perform(
        put(url + "/{id}", activity1.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(activity2)))
        .andExpect(status().isOk());
    doReturn(activity2).when(mockDao).findById(Long.valueOf(1));
    mockMvc.perform(get(url + "/{id}", 1))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id", is(1)))
        .andExpect(jsonPath("$.name", is("Cycling instead of driving")))
        .andExpect(jsonPath("$.co2Category.id", is(2)))
        .andExpect(jsonPath("$.co2Category.kgCO2Emission", is(2.52)))
        .andExpect(jsonPath("$.co2Category.points", is(80)));
    verify(mockDao, times(2)).findById(activity1.getId());
    verify(mockDao, times(1)).update(activity1, activity2);
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testUpdateNotFound() throws Exception {
    doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
    mockMvc.perform(
        put(url + "/{id}", activity1.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(activity2)))
        .andExpect(status().isNotFound());
    verify(mockDao, times(1)).findById(activity1.getId());
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testDeleteSucces() throws Exception {
    doReturn(activity1).when(mockDao).findById(activity1.getId());
    doNothing().when(mockDao).delete(activity1);
    mockMvc.perform(
        delete(url + "/{id}", 1))
        .andExpect(status().isOk());
    verify(mockDao, times(1)).findById(activity1.getId());
    verify(mockDao, times(1)).delete(activity1);
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testDeleteNotFound() throws Exception {
    doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
    mockMvc.perform(
        delete(url + "/{id}", 1))
        .andExpect(status().isNotFound());
    verify(mockDao, times(1)).findById(activity1.getId());
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testGetPointsSucces() throws Exception {
    doReturn(activity1).when(mockDao).findById(activity1.getId());
    mockMvc.perform(get(url + "/{id}/points", activity1.getId()))
        .andExpect(status().isOk())
        .andExpect(content().string("20"));
    verify(mockDao, times(1)).findById(activity1.getId());
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testGetPointsNotFound() throws Exception {
    doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
    mockMvc.perform(get(url + "/{id}/points", activity1.getId()))
        .andExpect(status().isNotFound());
    verify(mockDao, times(1)).findById(activity1.getId());
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testGetCo2ReductionSucces() throws Exception {
    doReturn(activity1).when(mockDao).findById(activity1.getId());
    mockMvc.perform(get(url + "/{id}/kgCO2Emission", activity1.getId()))
        .andExpect(status().isOk())
        .andExpect(content().string("0.63"));
    verify(mockDao, times(1)).findById(activity1.getId());
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testGetCo2ReductionNotFound() throws Exception {
    doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
    mockMvc.perform(get(url + "/{id}/kgCO2Emission", activity1.getId()))
        .andExpect(status().isNotFound());
    verify(mockDao, times(1)).findById(activity1.getId());
    verifyNoMoreInteractions(mockDao);
  }

}
