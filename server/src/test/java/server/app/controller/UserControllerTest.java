package server.app.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import server.app.dao.FriendshipDao;
import server.app.dao.UserDao;
import server.app.model.Friendship;
import server.app.model.User;

import java.util.Arrays;
import java.util.List;
import server.app.model.UserHashedPasswordOnly;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.iterableWithSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest extends BasicControllerTest{

    private MockMvc mockMvc;

    @Mock
    private UserDao mockDao;

    @Mock
    private FriendshipDao mockFriendshipDao;

    @InjectMocks
    private UserController controller;

    @Before
    public void init(){
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        url = "/user";
    }

    @Test
    public void testGetAll() throws Exception {
        List<User> users = Arrays.asList(user1, user2);

        doReturn(users).when(mockDao).findAll();

        this.mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("[0].id", is(1)))
                .andExpect(jsonPath("[0].username", is("Steve")))
                .andExpect(jsonPath("[1].id", is(2)))
                .andExpect(jsonPath("[1].username", is("Peter")));
        verify(mockDao, times(1)).findAll();
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetByIdSuccess() throws Exception {
        doReturn(user1).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.username", is("Steve")));
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetByIdFail() throws Exception {
        doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}", 1))
                .andExpect(status().isNotFound());
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testCreateUser() throws Exception {
        doReturn(user1).when(mockDao).save(user1);
        mockMvc.perform(post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(user1)))
                .andExpect(status().isOk());
        verify(mockDao, times(1)).save(user1);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testUpdateUserFoundUser() throws Exception {
        doReturn(user1).when(mockDao).findById(user1.getId());
        mockMvc.perform(
                put(url + "/{id}", user1.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(user2)))
                .andExpect(status().isOk());
        doReturn(user2).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.username", is("Peter")));
        verify(mockDao, times(2)).findById(user1.getId());
        verify(mockDao, times(1)).update(user1, user2);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testUpdateUserDidNotFindUser() throws Exception {
        doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
        mockMvc.perform(
                put(url + "/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(user1)))
                .andExpect(status().isNotFound());
        verify(mockDao, times(1)).findById(1L);
        verify(mockDao, never()).update(any(User.class), any(User.class));
        verifyNoMoreInteractions(mockDao);
    }

    @SuppressWarnings("Duplicates")
    @Test
    public void testDeleteUserSuccess() throws Exception {
        doReturn(user1).when(mockDao).findById(user1.getId());
        doNothing().when(mockDao).delete(user1);
        mockMvc.perform(
                delete(url + "/{id}", 1))
                .andExpect(status().isOk());
        verify(mockDao, times(1)).findById(user1.getId());
        verify(mockDao, times(1)).delete(user1);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testDeleteUserFail() throws Exception {
        doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
        mockMvc.perform(
                delete(url + "/{id}", 1))
                .andExpect(status().isNotFound());
        verify(mockDao, times(1)).findById(user1.getId());
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetTotalPointsByIdNoDoneActivities() throws Exception {
        doReturn(user1).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/totalPoints", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", is(0)));
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetTotalPointsByIdWithDoneActivities() throws Exception {
        user1.addDoneActivity(doneActivity1);
        user1.addDoneActivity(doneActivity2);
        doReturn(user1).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/totalPoints", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", is(100)));
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetTotalPointsByIdWithAchievement() throws Exception {
        user1.addUserAchievement(userAchievement1);
        doReturn(user1).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/totalPoints", 1))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$", is(20)));
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetTotalPointsByIdNotFound() throws Exception {
        doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/totalPoints", 1))
            .andExpect(status().isNotFound());
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetKgCO2EmissionByIdNoDoneActivities() throws Exception {
        doReturn(user1).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/kgCO2Emission", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", is(0)));
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetKgCO2EmissionByIdWithDoneActivities() throws Exception {
        user1.addDoneActivity(doneActivity1);
        user1.addDoneActivity(doneActivity2);
        doReturn(user1).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/kgCO2Emission", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", is(3.15)));
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetKgCO2EmissionByIdNotFound() throws Exception {
        doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/kgCO2Emission", 1))
            .andExpect(status().isNotFound());
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetDoneActivitiesNotFound() throws Exception {
        doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/doneActivities", 1))
            .andExpect(status().isNotFound());
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetNoDoneActivities() throws Exception {
        doReturn(user1).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/doneActivities", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(jsonPath("$").isEmpty());
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetWithDoneActivities() throws Exception {
        user1.addDoneActivity(doneActivity1);
        user1.addDoneActivity(doneActivity2);
        doReturn(user1).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/doneActivities", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("[0].id", is(1)))
                .andExpect(jsonPath("[0].user.id", is(1)))
                .andExpect(jsonPath("[0].user.username", is("Steve")))
                .andExpect(jsonPath("[0].activity.id", is(1)))
                .andExpect(jsonPath("[0].activity.name", is("Eating a vegetarian meal")))
                .andExpect(jsonPath("[0].activity.co2Category.id", is(1)))
                .andExpect(jsonPath("[0].activity.co2Category.kgCO2Emission", is(0.63)))
                .andExpect(jsonPath("[0].activity.co2Category.points", is(20)))
                .andExpect(jsonPath("[1].id", is(2)))
                .andExpect(jsonPath("[1].user.id", is(1)))
                .andExpect(jsonPath("[1].user.username", is("Steve")))
                .andExpect(jsonPath("[1].activity.id", is(2)))
                .andExpect(jsonPath("[1].activity.name", is("Cycling instead of driving")))
                .andExpect(jsonPath("[1].activity.co2Category.id", is(2)))
                .andExpect(jsonPath("[1].activity.co2Category.kgCO2Emission", is(2.52)))
                .andExpect(jsonPath("[1].activity.co2Category.points", is(80)));
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetAchievementsNotFound() throws Exception {
        doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/userAchievements", 1))
            .andExpect(status().isNotFound());
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetNoAchievements() throws Exception {
        doReturn(user1).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/userAchievements", 1))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$", hasSize(0)))
            .andExpect(jsonPath("$").isEmpty());
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetWithAchievements() throws Exception {
        user1.addUserAchievement(userAchievement1);
        user1.addUserAchievement(userAchievement2);
        doReturn(user1).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/userAchievements", 1))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[0].user.id", is(1)))
            .andExpect(jsonPath("$[0].user.username", is("Steve")))
            .andExpect(jsonPath("$[0].achievement.id", is(1)))
            .andExpect(jsonPath("$[0].achievement.name", is("7 days of activities")))
            .andExpect(jsonPath("$[0].achievement.points", is(20)))
            .andExpect(jsonPath("$[1].user.id", is(1)))
            .andExpect(jsonPath("$[1].user.username", is("Steve")))
            .andExpect(jsonPath("$[1].achievement.id", is(2)))
            .andExpect(jsonPath("$[1].achievement.name", is("14 days of activities")))
            .andExpect(jsonPath("$[1].achievement.points", is(30)));
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetPasswordNotFound() throws Exception {
        doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/password", 1))
            .andExpect(status().isNotFound());
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetNoPassword() throws Exception {
        doReturn(user1).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/password", 1))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is(1)))
            .andExpect(jsonPath("$.hashedPassword").doesNotExist()) ;
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetWithPassword() throws Exception {
        user1.setHashedPassword("hf8349f93h");
        doReturn(user1).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/password", 1))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is(1)))
            .andExpect(jsonPath("$.hashedPassword", is("hf8349f93h")));
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testUpdatePassword() throws Exception {
        UserHashedPasswordOnly passwordHash = new UserHashedPasswordOnly(1L, "fb5e0d73f8");
        user1.setHashedPassword("hf8349f93h");
        doReturn(user1).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/password", 1))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is(1)))
            .andExpect(jsonPath("$.hashedPassword", is("hf8349f93h")));
        mockMvc.perform(patch(url + "/{id}/password", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(passwordHash)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is(1)))
            .andExpect(jsonPath("$.hashedPassword", is("fb5e0d73f8")));
        verify(mockDao, times(2)).findById(1L);
        verify(mockDao, times(1)).save(user1);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testUpdatePasswordWrongId() throws Exception {
        UserHashedPasswordOnly passwordHash = new UserHashedPasswordOnly(3L, "fb5e0d73f8");
        user1.setHashedPassword("hf8349f93h");
        doReturn(user1).when(mockDao).findById(1L);
        mockMvc.perform(get(url + "/{id}/password", 1))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is(1)))
            .andExpect(jsonPath("$.hashedPassword", is("hf8349f93h")));
        mockMvc.perform(patch(url + "/{id}/password", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(passwordHash)))
            .andExpect(status().isOk());
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testUpdatePasswordNotFound() throws Exception {
        UserHashedPasswordOnly passwordHash = new UserHashedPasswordOnly(1L, "fb5e0d73f8");
        doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
        mockMvc.perform(patch(url + "/{id}/password", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(passwordHash)))
            .andExpect(status().isNotFound());
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetIdAndHashFound() throws Exception {
        user1.setHashedPassword("hf8349f93h");
        doReturn(user1).when(mockDao).findByUsername(user1.getUsername());
        mockMvc.perform(get(url + "/id-hashedPassword/{username}", user1.getUsername()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is(1)))
            .andExpect(jsonPath("$.hashedPassword", is("hf8349f93h")));
        verify(mockDao, times(1)).findByUsername(user1.getUsername());
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetIdAndHashNotFound() throws Exception {
        doThrow(new IllegalArgumentException()).when(mockDao).findByUsername(user1.getUsername());
        mockMvc.perform(get(url + "/id-hashedPassword/{username}", user1.getUsername()))
            .andExpect(status().isNotFound());
        verify(mockDao, times(1)).findByUsername(user1.getUsername());
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testAddFriendSuccess() throws Exception {
        doReturn(user1).when(mockDao).findById(1L);
        doReturn(user2).when(mockDao).findByUsername("Peter");
        doReturn(friendship1).when(mockFriendshipDao).save(friendship1);
        mockMvc.perform(post(url + "/{user1Id}/friendship/{user2Username}", 1, user2.getUsername()))
            .andExpect(status().isOk());
        verify(mockDao, times(1)).findById(1L);
        verify(mockDao, times(1)).findByUsername("Peter");
        verify(mockFriendshipDao, times(1)).save(friendship1);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testAddFriendUserNotFound() throws Exception {
        doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
        mockMvc.perform(post(url + "/{user1Id}/friendship/{user2Username}", 1, user2.getUsername()))
            .andExpect(status().isNoContent());
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testAddFriendFriendNotFound() throws Exception {
        doReturn(user1).when(mockDao).findById(1L);
        doThrow(new IllegalArgumentException()).when(mockDao).findByUsername("Peter");
        mockMvc.perform(post(url + "/{user1Id}/friendship/{user2Username}", 1, user2.getUsername()))
            .andExpect(status().isNoContent());
        verify(mockDao, times(1)).findById(1L);
        verify(mockDao, times(1)).findByUsername("Peter");
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetAllFriends() throws Exception {
        List<Friendship> friendships = Arrays.asList(friendship1);
        doReturn(friendships).when(mockFriendshipDao).findAllByUserIdAndStatus(1L);
        doReturn(user1).when(mockDao).findById(1L);
        this.mockMvc.perform(get(url + "/{id}/friendship", 1))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].id.user1.id", is(1)))
            .andExpect(jsonPath("$.[0].id.user2.id", is(2)));
        verify(mockFriendshipDao, times(1)).findAllByUserIdAndStatus(1L);
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetAllFriendsNoFriendships() throws Exception {
        List<Friendship> friendships = Arrays.asList();
        doReturn(friendships).when(mockFriendshipDao).findAllByUserIdAndStatus(1L);
        doReturn(user1).when(mockDao).findById(1L);
        this.mockMvc.perform(get(url + "/{id}/friendship", 1))
            .andExpect(status().isNoContent());
        verify(mockFriendshipDao, times(1)).findAllByUserIdAndStatus(1L);
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testGetAllFriendsNotFound() throws Exception {
        doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
        this.mockMvc.perform(get(url + "/{id}/friendship", 1))
            .andExpect(status().isNotFound());
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testDeleteFriendSuccess() throws Exception {
        doReturn(user1).when(mockDao).findById(user1.getId());
        doReturn(user2).when(mockDao).findById(user2.getId());
        doNothing().when(mockFriendshipDao).deleteById(1L, 2L);
        mockMvc.perform(delete(url + "/{userId}/friendship/{friendId}", user1.getId(), user2.getId()))
            .andExpect(status().isOk());
        verify(mockDao, times(1)).findById(1L);
        verify(mockDao, times(1)).findById(2L);
        verify(mockFriendshipDao, times(1)).deleteById(user1.getId(), user2.getId());
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testDeleteFriendUserNotFound() throws Exception {
        doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
        mockMvc.perform(delete(url + "/{userId}/friendship/{friendId}", user1.getId(), user2.getId()))
            .andExpect(status().isNotFound());
        verify(mockDao, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDao);
    }

    @Test
    public void testDeleteFriendFriendNotFound() throws Exception {
        doReturn(user1).when(mockDao).findById(1L);
        doThrow(new IllegalArgumentException()).when(mockDao).findById(2L);
        mockMvc.perform(delete(url + "/{userId}/friendship/{friendId}", user1.getId(), user2.getId()))
            .andExpect(status().isNotFound());
        verify(mockDao, times(1)).findById(1L);
        verify(mockDao, times(1)).findById(2L);
        verifyNoMoreInteractions(mockDao);
    }

}
