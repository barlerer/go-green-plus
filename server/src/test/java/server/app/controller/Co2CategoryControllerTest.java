package server.app.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import server.app.dao.Co2CategoryDao;
import server.app.model.Co2Category;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class Co2CategoryControllerTest extends BasicControllerTest{

  private MockMvc mockMvc;

  @Mock
  private Co2CategoryDao mockDao;

  @InjectMocks
  private Co2CategoryController controller;

  @Before
  public void init(){
    mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    url = "/co2Category";
  }

  @Test
  public void testGetAll() throws Exception {

    List<Co2Category> co2_categories = Arrays.asList(category1, category2);

    doReturn(co2_categories).when(mockDao).findAll();

    this.mockMvc.perform(get(url))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].id", is(1)))
        .andExpect(jsonPath("$[0].kgCO2Emission", is(0.63)))
        .andExpect(jsonPath("$[0].points", is(20)))
        .andExpect(jsonPath("$[1].id", is(2)))
        .andExpect(jsonPath("$[1].kgCO2Emission", is(2.52)))
        .andExpect(jsonPath("$[1].points", is(80)));
    verify(mockDao, times(1)).findAll();
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testGetByIdSuccess() throws Exception {
    Co2Category category1 = new Co2Category(1L, BigDecimal.valueOf(0.63), 20);
    doReturn(category1).when(mockDao).findById(1L);
    mockMvc.perform(get(url + "/{id}", 1))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id", is(1)))
        .andExpect(jsonPath("$.kgCO2Emission", is(0.63)))
        .andExpect(jsonPath("$.points", is(20)));
    verify(mockDao, times(1)).findById(1L);
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testGetByIdFail404NotFound() throws Exception {
    doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
    mockMvc.perform(get(url + "/{id}", 1))
        .andExpect(status().isNotFound());
    verify(mockDao, times(1)).findById(1L);
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testCreate() throws Exception {
    Co2Category category1 = new Co2Category(1L, BigDecimal.valueOf(0.63), 20);
    doReturn(category1).when(mockDao).save(category1);
    mockMvc.perform(
        post(url)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(category1)))
        .andExpect(status().isOk());
    verify(mockDao, times(1)).save(category1);
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testUpdateSucces() throws Exception {
    Co2Category category1 = new Co2Category(1L, BigDecimal.valueOf(0.63), 20);
    Co2Category category2 = new Co2Category(1L, BigDecimal.valueOf(0.63), 80);
    doReturn(category1).when(mockDao).findById(category1.getId());
    mockMvc.perform(
        put(url + "/{id}", category1.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(category2)))
        .andExpect(status().isOk());
    doReturn(category2).when(mockDao).findById(1L);
    mockMvc.perform(get(url + "/{id}", 1))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id", is(1)))
        .andExpect(jsonPath("$.kgCO2Emission", is(0.63)))
        .andExpect(jsonPath("$.points", is(80)));
    verify(mockDao, times(2)).findById(category1.getId());
    verify(mockDao, times(1)).update(category1, category2);
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testUpdateNotFound() throws Exception {
    doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
    mockMvc.perform(
        put(url + "/{id}", category1.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(category2)))
        .andExpect(status().isNotFound());
    verify(mockDao, times(1)).findById(category1.getId());
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testDeleteSucces() throws Exception {
    Co2Category category1 = new Co2Category(1L, BigDecimal.valueOf(0.63), 20);
    doReturn(category1).when(mockDao).findById(category1.getId());
    doNothing().when(mockDao).delete(category1);
    mockMvc.perform(
        delete(url + "/{id}", 1))
        .andExpect(status().isOk());
    verify(mockDao, times(1)).findById(category1.getId());
    verify(mockDao, times(1)).delete(category1);
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testDeleteFail404NotFound() throws Exception {
    Co2Category category1 = new Co2Category(1L, BigDecimal.valueOf(0.63), 20);
    doThrow(new IllegalArgumentException()).when(mockDao).findById(1L);
    mockMvc.perform(
        delete(url + "/{id}", 1))
        .andExpect(status().isNotFound());
    verify(mockDao, times(1)).findById(category1.getId());
    verifyNoMoreInteractions(mockDao);
  }

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

}
