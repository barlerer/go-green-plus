package server.app.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import server.app.dao.ActivityDao;
import server.app.dao.DoneActivityDao;
import server.app.dao.UserDao;
import server.app.model.Activity;
import server.app.model.Co2Category;
import server.app.model.DoneActivity;
import server.app.model.User;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class DoneActivityControllerTest extends BasicControllerTest{

    private MockMvc mockMvc;

    @Mock
    private DoneActivityDao mockDaoDoneActivity;

    @Mock
    private UserDao mockDaoUser;

    @Mock
    private ActivityDao mockDaoActivity;

    @InjectMocks
    private DoneActivityController controller;

    @InjectMocks
    private UserController controllerUser;

    @Before
    public void init(){
        mockMvc = MockMvcBuilders.standaloneSetup(controller, controllerUser).build();
        url = "/doneActivity";
    }

    @Test
    public void testGetAll() throws Exception {

        List<DoneActivity> doneActivities = Arrays.asList(doneActivity1, doneActivity2, doneActivity3);

        doReturn(doneActivities).when(mockDaoDoneActivity).findAll();

        this.mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("[0].id", is(1)))
                .andExpect(jsonPath("[0].user.id", is(1)))
                .andExpect(jsonPath("[0].user.username", is("Steve")))
                .andExpect(jsonPath("[0].activity.id", is(1)))
                .andExpect(jsonPath("[0].activity.name", is("Eating a vegetarian meal")))
                .andExpect(jsonPath("[0].activity.co2Category.id", is(1)))
                .andExpect(jsonPath("[0].activity.co2Category.kgCO2Emission", is(0.63)))
                .andExpect(jsonPath("[0].activity.co2Category.points", is(20)))
                .andExpect(jsonPath("[1].id", is(2)))
                .andExpect(jsonPath("[1].user.id", is(1)))
                .andExpect(jsonPath("[1].user.username", is("Steve")))
                .andExpect(jsonPath("[1].activity.id", is(2)))
                .andExpect(jsonPath("[1].activity.name", is("Cycling instead of driving")))
                .andExpect(jsonPath("[1].activity.co2Category.id", is(2)))
                .andExpect(jsonPath("[1].activity.co2Category.kgCO2Emission", is(2.52)))
                .andExpect(jsonPath("[1].activity.co2Category.points", is(80)))
                .andExpect(jsonPath("[2].id", is(3)))
                .andExpect(jsonPath("[2].user.id", is(2)))
                .andExpect(jsonPath("[2].user.username", is("Peter")))
                .andExpect(jsonPath("[2].activity.id", is(1)))
                .andExpect(jsonPath("[2].activity.name", is("Eating a vegetarian meal")))
                .andExpect(jsonPath("[2].activity.co2Category.id", is(1)))
                .andExpect(jsonPath("[2].activity.co2Category.kgCO2Emission", is(0.63)))
                .andExpect(jsonPath("[2].activity.co2Category.points", is(20)));
        verify(mockDaoDoneActivity, times(1)).findAll();
        verifyNoMoreInteractions(mockDaoDoneActivity);
    }

    @Test
    public void testGetByIdSuccess() throws Exception {
        doReturn(doneActivity1).when(mockDaoDoneActivity).findById(1L);
        mockMvc.perform(get(url + "/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.user.id", is(1)))
                .andExpect(jsonPath("$.user.username", is("Steve")))
                .andExpect(jsonPath("$.activity.id", is(1)))
                .andExpect(jsonPath("$.activity.name", is("Eating a vegetarian meal")))
                .andExpect(jsonPath("$.activity.co2Category.id", is(1)))
                .andExpect(jsonPath("$.activity.co2Category.kgCO2Emission", is(0.63)))
                .andExpect(jsonPath("$.activity.co2Category.points", is(20)));
        verify(mockDaoDoneActivity, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDaoDoneActivity);
    }

    @Test
    public void testGetByIdFail404NotFound() throws Exception {
        doReturn(null).when(mockDaoDoneActivity).findById(1L);
        mockMvc.perform(get(url + "/{id}", 1))
                .andExpect(status().isNotFound());
        verify(mockDaoDoneActivity, times(1)).findById(1L);
        verifyNoMoreInteractions(mockDaoDoneActivity);
    }

    @Test
    public void testCreate() throws Exception {
        doneActivity1.setQuantity(3);
        doReturn(user1).when(mockDaoUser).findById(1L);
        doReturn(activity1).when(mockDaoActivity).findById(1L);
        mockMvc.perform(post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(doneActivity1)))
                .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is(1)))
            .andExpect(jsonPath("$.user.id", is(1)))
            .andExpect(jsonPath("$.user.username", is("Steve")))
            .andExpect(jsonPath("$.activity.id", is(1)))
            .andExpect(jsonPath("$.activity.name", is("Eating a vegetarian meal")))
            .andExpect(jsonPath("$.activity.co2Category.id", is(1)))
            .andExpect(jsonPath("$.activity.co2Category.kgCO2Emission", is(0.63)))
            .andExpect(jsonPath("$.activity.co2Category.points", is(20)));
        verify(mockDaoActivity, times(1)).findById(1L);
        verify(mockDaoUser, times(1)).findById(1L);
        verify(mockDaoUser, times(1)).save(user1);
        verifyNoMoreInteractions(mockDaoUser);
        verifyNoMoreInteractions(mockDaoActivity);
        verifyNoMoreInteractions(mockDaoDoneActivity);
    }

    @Test
    public void testCreateZeroQuantity() throws Exception {
        doneActivity1.setQuantity(0);
        doReturn(user1).when(mockDaoUser).findById(1L);
        doReturn(activity1).when(mockDaoActivity).findById(1L);
        mockMvc.perform(post(url)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(doneActivity1)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is(1)))
            .andExpect(jsonPath("$.user.id", is(1)))
            .andExpect(jsonPath("$.user.username", is("Steve")))
            .andExpect(jsonPath("$.activity.id", is(1)))
            .andExpect(jsonPath("$.activity.name", is("Eating a vegetarian meal")))
            .andExpect(jsonPath("$.activity.co2Category.id", is(1)))
            .andExpect(jsonPath("$.activity.co2Category.kgCO2Emission", is(0.63)))
            .andExpect(jsonPath("$.activity.co2Category.points", is(20)));
        verify(mockDaoUser, times(1)).findById(1L);
        verify(mockDaoActivity, times(1)).findById(1L);
        verify(mockDaoUser, times(1)).save(user1);
        verifyNoMoreInteractions(mockDaoUser);
        verifyNoMoreInteractions(mockDaoActivity);
        verifyNoMoreInteractions(mockDaoDoneActivity);
    }

    @Test
    public void testUpdateFound() throws Exception {
        doReturn(doneActivity1).when(mockDaoDoneActivity).findById(doneActivity1.getId());
        mockMvc.perform(
                put(url + "/{id}", doneActivity1.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(doneActivity2)))
                .andExpect(status().isOk());
        doReturn(doneActivity2).when(mockDaoDoneActivity).findById(1L);
        mockMvc.perform(get(url + "/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.user.id", is(1)))
                .andExpect(jsonPath("$.user.username", is("Steve")))
                .andExpect(jsonPath("$.activity.id", is(2)))
                .andExpect(jsonPath("$.activity.name", is("Cycling instead of driving")))
                .andExpect(jsonPath("$.activity.co2Category.id", is(2)))
                .andExpect(jsonPath("$.activity.co2Category.kgCO2Emission", is(2.52)))
                .andExpect(jsonPath("$.activity.co2Category.points", is(80)));
        verify(mockDaoDoneActivity, times(2)).findById(doneActivity1.getId());
        verify(mockDaoDoneActivity, times(1)).update(doneActivity1, doneActivity2);
        verifyNoMoreInteractions(mockDaoDoneActivity);
    }

    @Test
    public void testUpdateDidNotFind() throws Exception {
        doReturn(null).when(mockDaoDoneActivity).findById(1L);
        mockMvc.perform(
                put(url + "/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(doneActivity1)))
                .andExpect(status().isNotFound());
        verify(mockDaoDoneActivity, times(1)).findById(1L);
        verify(mockDaoDoneActivity, never()).update(any(DoneActivity.class), any(DoneActivity.class));
        verifyNoMoreInteractions(mockDaoDoneActivity);
    }

    @SuppressWarnings("Duplicates")
    @Test
    public void testDeleteSuccess() throws Exception {
        doReturn(doneActivity1).when(mockDaoDoneActivity).findById(doneActivity1.getId());
        doNothing().when(mockDaoDoneActivity).delete(doneActivity1);
        mockMvc.perform(
                delete(url + "/{id}", 1))
                .andExpect(status().isOk());
        verify(mockDaoDoneActivity, times(1)).findById(doneActivity1.getId());
        verify(mockDaoDoneActivity, times(1)).delete(doneActivity1);
        verifyNoMoreInteractions(mockDaoDoneActivity);
    }

    @Test
    public void testDeleteFail404NotFound() throws Exception {
        doReturn(null).when(mockDaoDoneActivity).findById(doneActivity1.getId());
        mockMvc.perform(
                delete(url + "/{id}", 1))
                .andExpect(status().isNotFound());
        verify(mockDaoDoneActivity, times(1)).findById(doneActivity1.getId());
        verifyNoMoreInteractions(mockDaoDoneActivity);
    }

    @Test
    public void testAddSolarPanelDoneActivityNoSolarPanel() throws Exception {
        Iterable<User> users = new ArrayList<>();
        ((ArrayList<User>) users).add(user1);
        ((ArrayList<User>) users).add(user2);
        doReturn(users).when(mockDaoUser).findAll();
        Co2Category co2Category5 = new Co2Category(5L, BigDecimal.valueOf(2.50), 1);
        Activity activity5 = new Activity(5L, "Installing a solar panel", co2Category5);
        doReturn(activity5).when(mockDaoActivity).findById(5L);
        mockMvc.perform(
                get("/solar"))
                .andExpect(status().isOk());
        verify(mockDaoUser, times(1)).findAll();
        verify(mockDaoActivity, times(1)).findById(5L);
        verifyNoMoreInteractions(mockDaoDoneActivity);
    }

    @Test
    public void testAddSolarPanelDoneActivityWithSolarPanel() throws Exception {
        user1.setHasSolarPanel(true);
        Iterable<User> users = new ArrayList<>();
        ((ArrayList<User>) users).add(user1);
        ((ArrayList<User>) users).add(user2);
        doReturn(users).when(mockDaoUser).findAll();
        Co2Category co2Category5 = new Co2Category(5L, BigDecimal.valueOf(2.50), 1);
        Activity activity5 = new Activity(5L, "Installing a solar panel", co2Category5);
        doReturn(activity5).when(mockDaoActivity).findById(5L);
        mockMvc.perform(
                get("/solar"))
                .andExpect(status().isOk());
        verify(mockDaoUser, times(1)).findAll();
        verify(mockDaoActivity, times(1)).findById(5L);
        verify(mockDaoDoneActivity, times(1)).save(any());
    }

    @Test
    public void testAddTemperatureDoneActivityNoTempChange() throws Exception {
        Iterable<User> users = new ArrayList<>();
        ((ArrayList<User>) users).add(user1);
        ((ArrayList<User>) users).add(user2);
        doReturn(users).when(mockDaoUser).findAll();
        Co2Category co2Category6 = new Co2Category(6L, BigDecimal.valueOf(7.50), 2);
        Activity activity6 = new Activity(6L, "Installing a solar panel", co2Category6);
        doReturn(activity6).when(mockDaoActivity).findById(6L);
        mockMvc.perform(
                get("/temp"))
                .andExpect(status().isOk());
        verify(mockDaoUser, times(1)).findAll();
        verify(mockDaoActivity, times(1)).findById(6L);
        verify(mockDaoDoneActivity, times(0)).save(any());
    }

    @Test
    public void testAddTemperatureDoneActivityWithTempChange() throws Exception {
        Iterable<User> users = new ArrayList<>();
        ((ArrayList<User>) users).add(user1);
        ((ArrayList<User>) users).add(user2);
        doReturn(users).when(mockDaoUser).findAll();
        Co2Category co2Category6 = new Co2Category(6L, BigDecimal.valueOf(7.50), 2);
        Activity activity6 = new Activity(6L, "Installing a solar panel", co2Category6);
        DoneActivity doneActivity = new DoneActivity(1L, user1, activity6);
        user1.addDoneActivity(doneActivity);
        doReturn(activity6).when(mockDaoActivity).findById(6L);
        mockMvc.perform(
                get("/temp"))
                .andExpect(status().isOk());
        verify(mockDaoUser, times(1)).findAll();
        verify(mockDaoActivity, times(1)).findById(6L);
        verify(mockDaoDoneActivity, times(1)).save(any());
    }

    @Test
    public void testAddTemperatureDoneActivityNoTempChangeScaled() throws Exception {
        Iterable<User> users = new ArrayList<>();
        ((ArrayList<User>) users).add(user1);
        ((ArrayList<User>) users).add(user2);
        doReturn(users).when(mockDaoUser).findAll();
        Co2Category co2Category6 = new Co2Category(6L, BigDecimal.valueOf(7.50), 2);
        Activity activity6 = new Activity(6L, "Installing a solar panel", co2Category6);
        DoneActivity doneActivity = new DoneActivity(1L, user1, activity6);
        user1.addDoneActivity(doneActivity);
        DoneActivity doneActivityUser2 = new DoneActivity(2L, user2, activity6, 3);
        user2.addDoneActivity(doneActivityUser2);
        doReturn(activity6).when(mockDaoActivity).findById(6L);
        mockMvc.perform(
                get("/temp"))
                .andExpect(status().isOk());
        verify(mockDaoUser, times(1)).findAll();
        verify(mockDaoActivity, times(1)).findById(6L);
        verify(mockDaoDoneActivity, times(2)).save(any());
    }

    @Test
    public void testAddTemperatureDoneActivityWrongActivity() throws Exception {
        Iterable<User> users = new ArrayList<>();
        ((ArrayList<User>) users).add(user1);
        ((ArrayList<User>) users).add(user2);
        doReturn(users).when(mockDaoUser).findAll();
        Co2Category co2Category6 = new Co2Category(6L, BigDecimal.valueOf(7.50), 2);
        Activity activity6 = new Activity(3L, "Installing a solar panel", co2Category6);
        DoneActivity doneActivity = new DoneActivity(1L, user1, activity6);
        user1.addDoneActivity(doneActivity);
        DoneActivity doneActivityUser2 = new DoneActivity(2L, user2, activity6, 3);
        user2.addDoneActivity(doneActivityUser2);
        doReturn(activity6).when(mockDaoActivity).findById(6L);
        mockMvc.perform(
                get("/temp"))
                .andExpect(status().isOk());
        verify(mockDaoUser, times(1)).findAll();
        verify(mockDaoActivity, times(1)).findById(6L);
        verify(mockDaoDoneActivity, times(0)).save(any());
    }

}
