package server.app.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import server.app.dao.AchievementDao;
import server.app.model.Achievement;

@RunWith(MockitoJUnitRunner.class)
public class AchievementControllerTest extends BasicControllerTest{

  private MockMvc mockMvc;

  @Mock
  private AchievementDao mockDao;

  @InjectMocks
  private AchievementController controller;

  @Before
  public void init(){
    mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    url = "/achievement";
  }

  @Test
  public void testGetAll() throws Exception {

    List<Achievement> achievements = Arrays.asList(achievement1, achievement2);

    doReturn(achievements).when(mockDao).findAll();

    this.mockMvc.perform(get(url))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].id", is(1)))
        .andExpect(jsonPath("$[0].name", is("7 days of activities")))
        .andExpect(jsonPath("$[0].points", is(20)))
        .andExpect(jsonPath("$[1].id", is(2)))
        .andExpect(jsonPath("$[1].name", is("14 days of activities")))
        .andExpect(jsonPath("$[1].points", is(30)));
    verify(mockDao, times(1)).findAll();
    verifyNoMoreInteractions(mockDao);
  }

  @Test
  public void testCreate() throws Exception {
    doReturn(achievement1).when(mockDao).save(achievement1);
    mockMvc.perform(
        post(url)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(achievement1)))
        .andExpect(status().isOk());
    verify(mockDao, times(1)).save(achievement1);
    verifyNoMoreInteractions(mockDao);
  }

}
