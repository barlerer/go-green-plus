# Models
- User
    - Long id
    - String username
    - String hashedPassword
    - Date joinDate
    - boolean hasSolarPanel
- Friendship
    - FriendshipPK friendshipId
- FriendshipPK
    - Long user1Id
    - Long user2Id
- Achievement
    - Long id
- UserAchievement
    - Long id
- Co2Category
    - Long id
- Activity
    - Long id
- DoneActivity
    - Long id
    
#Endpoints
- Removing a friend:
    - DELETE /user/{userId}/friendship/{friendId}
    returns code 200 with text/plain;charset=UTF-8: 'friend successfully removed'

- Adding a friend:
    - POST /user/{user1Id}/friendship/{user2Username}

- Getting a list of all friendships
    - GET /user/{id}/friendship

#TODO
- instead of Long id make everything String id
    - when the string is an integer, parse it to an int if it's a string, find by username
















# Final report

## Authors
 - Jari Kasandiredjo 4956230

## The team
Jasper Vermeulen
Worked on designing the server, deciding on how to implement the server, writing server, writing test.
Bar Lerer, connection from client to server, making sure the client has everything it needs from the server.
Ayrthon K, gave me information on co2 reduction and explaining what needs to be implemented in the database.
State person you worked with and describe what you primarily worked (during entire project and also from demo to demo).

## Concept
  - Describe how you and group partner planned on implementing your part of the project.
  - Where did the plans start and what is the place you started working.
    - For the GUI maybe one of the sketches we made can be uploaded in the final report folder as well.

Jasper showed me how to use the server that we we're going to run the server on.
Jasper made a schema for the sql database, i reviewed it and tweaked it a bit later. I started working on the server with hibernate and spring, set up endpoints.
Jasper made some tests, i made some tests.

## Choices were made
  - What are the framework(s) you are using for implementation?
  - Were there other possibilities? Why was this chosen?
  - Which dependencies were used?
    - State : (grouped for a functionality)
      - groupId
      - artifactId
      - version
    - describe in one sentence what they are used for
    
    Spring boot, seemed like the most popular (what is it called?) to make a REST service out there right now with lots of resources online.
    Hibernate / JPA as an ORM to make simple endpoinst for the api really easily.

## Keep in mind the individual feedback needs to written as well... maybe make some notes during the next few weeks.
## It is not necessary for now.
Feedback (individual)
Each team member reflects on how he/she functioned in the
team (at least 200 words per team member):
 - What were your stronger/weaker points during this project
 - Did you have conflicts with other team members? How did you solve them?

## References:
Research for co2 reduction:
  - Eating a vegetarian meal
    - http://www.greeneatz.com/foods-carbon-footprint.html
    - https://www.milieucentraal.nl/klimaat-en-aarde/klimaatklappers/grootste-klappers/