# Meeting 4

People present:

- Bar Lerer (Secretary)
- Teemu Weckroth (Chairman)
- Jasper Vermeulen
- Jari Kasandiredjo
- Mihai Cobzaru
- Ayrthon Karijodikoro


## Points of action
> The items below are things you should look into after the meeting.
During the meeting you can divide (some of) the work between the team members, so that everybody has something to do afterwards.
Denote this with a name between brackets after the point of action.

 - Java version - We discussed the differences between
 the JDK's and concluded that we will try to make a switch from Java 8 to
 10.
 The reasons are:
  - Java 8 will stop receiving support as of January 2019
  - Java 11 does not include JavaFX
  - We will have to explain those points in the final report.

- Pipeline errors were erupted because of file path that was in the part of the
GUI, this was fixed and we changed the project system for the team.
- Demo 2 is next week, we are almost done, just need to add finishing touches
to the GUI and client side functions
    - Handle errors on the GUI
    - Run server database testing
    - Fix CheckStyle warnings on the client side

- We suggested to start thinking and working on future functions\features such as
    - Login screen
    - Username and password, should be hashed


- Mihai Cobzaru is the chairman for the next meeting.
- Jari Kasandiredjo is the secretary for the next meeting.

#### This concludes the meeting.
