# Meeting 3
> Quoted text will give you extra pointers to how an agenda should look like

## Opening
> Here you check if everyone is present
- Bar Lerer
- Ayrthon Karijodikoro
- Teemu Weckroth
- Jasper Vermeulen
- Jari Kasandiredjo
- Mihai Cobzaru

## Agenda
> Make a list of all things that need discussing
- Sprint 2 retrospective
- Server endpoints to client
- How we are going to calculate and store points
- Writing down our own (coding) conventions down somewhere
- What we are going to do in sprint 3

## Approval of the agenda
> Make sure everything that needs to be discussed is in the agenda or add it if something is missing.

## Any other business
> If anybody has something that should be discussed but came up with that after the agenda was finalized (in point 2), he/she should bring that up now so that it can be discussed after all.

## Question round
> If there are any questions, now is the time to ask them.

## Chairman and secretary
> Assign a new chairman and secretary for next weeks meeting
- Teemu Weckroth (Chairman)
- Bar Lerer (Secretary)


## Closing
> Now you can start working on the project. Good luck!
