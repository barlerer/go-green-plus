# Meeting 6

People present:

- Julian van Dijk (TA)
- Ayrthon Karijodikoro (Chairman)
- Jasper Vermeulen
- Bar Lerer
- Teemu Weckroth (Secretary)
- Jari Kasandiredjo
- Mihai Cobzaru

## Points of action
> The items below are things you should look into after the meeting.
During the meeting you can divide (some of) the work between the team members, so that everybody has something to do afterwards.
Denote this with a name between brackets after the point of action.

- Sprint 5 retrospective
    - Issues with achievements, solved
    - Login screen and sign-up screen implemented
    - Achievements(?) and statistics screens to be done

- Achievement design discussion (client/server -side?)
    - Static method to track achievements?
    - Enums?
    - Prioritise friendships and activities over achievements

- Probably no time for bonus features

- Urgent: move to Java 10

- Final report
    - Deadline on Friday 29th for first individual parts of the report
    - Draft deadline, Thursday 4th

- Responsible Computer Science; report?
    - Testing coverage

- New secretary: Mihai
- New chairman: Jasper