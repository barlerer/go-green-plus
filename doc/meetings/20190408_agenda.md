# Meeting 8
> Quoted text will give you extra pointers to how an agenda should look like

## Opening
> Here you check if everyone is present
- Julian (TA)
- Jari (Secretary)
- Jasper
- Ayrthon
- Bar
- Teemu (Chairman)
- Mihai

## Agenda
> Make a list of all things that need discussing
- Project finalisation
- Each team presents and explains their part to the rest of the group.
- Decide on which (bonus) features we still want to implement
- Final report and presentation - roles and content
- Taking care of test coverage and checkstyle warnings

## Approval of the agenda
> Make sure everything that needs to be discussed is in the agenda or add it if something is missing.

## Any other business
> If anybody has something that should be discussed but came up with that after the agenda was finalized (in point 2), he/she should bring that up now so that it can be discussed after all.

## Question round
> If there are any questions, now is the time to ask them.

## Chairman and secretary
> Assign a new chairman and secretary for next weeks meeting

## Closing
> Now you can start working on the project. Good luck!
