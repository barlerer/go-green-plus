# Sprint Review

## Main problems encountered

### Problem 1: No rules and regulations in place
One of the important meetings failed. We had no rules in place to make sure everybody would be there.

### Problem 2: Late on testing and running checkstyle reports
We only started testing and running checkstyle reports on the last day of the week. This gave a lot of work that was not anticipated.

## Adjustments for next sprint

 - Rules are in place from Tuesday 2019-02-26 onwards an can be found on gitlab.
 - Rules are enforced by the cake rule and the lighter coffee rule.