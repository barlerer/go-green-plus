Local produce  saves 0.5 kg CO2 per meal: 15.875 pt/kg

Eating vegetarian meal saves 0.63 kg CO2 per meal: 31.75 pt/kg

Using a bike instead of a car saves 0.16 kg/km:5.102 pt/kg

Public transport instead of car saves 0.32kg/km: 10.158 pt/km

Solar panels(100% coverage) saves 5.419 kg CO2 per day: 172.06 pt/day

Lower temperature(Base 22 degrees Celsius) saves 4.31 kg/degree Celsius: 136 pt/degree Celsius