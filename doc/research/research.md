RESEARCH
Food	- buying local products instead of imports
		- 

Transportation 	
	Bike instead of car 	
		- Average kg/km CO2 reduction 	
	Public transport instead of car 	
		- Average kg/km per person CO2 reduction 
	Electric car instead of gasoline car 	

Energy 
	Temperature
		- how each degree reduces the amount of CO2 
	Solar panels		
		- less energy usage	

Recycling
	- Bottles (PVC/PET)
	- Plastic bags
	- ‘Grey water’


- RESULTS

TRANSPORTATIO
- http://www.technischwerken.nl/nieuws/gemiddelde-co2-uitstoot-van-nieuwe-autos-in-2016-en-2017/
Average emission of all new European cars sum op to 118.1 (109 g) g/km in 2017, it was 117.8 g/km in 2016.

- https://www.delijn.be/nl/overdelijn/organisatie/zorgzaam-ondernemen/milieu/co2-uitstoot-voertuigen.html
Average bus emits 75 g/km (given 14 passengers; this is equivalent to roughly 5.36 g/km per person)

- https://www.cbs.nl/nl-nl/achtergrond/2018/11/woon-werkafstanden-2016
Average travel distance is roughly 22.7km.

- https://www.milieucentraal.nl/duurzaam-vervoer/fiets-ov-of-auto/
Average emission in 2017 where average travel distance between 15 ~ 35 km

- http://www.railforum.nl/wp-content/uploads/2013/03/materieel-energieverbruik.pdf
Research shows that a ‘new’ train, built since 2000, it emits 0.48052226027 kg/km

Re-design current trains to reduce total mass for CO2 reductions (see image)

Lowered traction-energy usage reduces 2.0 kWh/ton

Electric car (Using Tesla Model S as reference)
- https://ev-database.nl/auto/1088/Tesla-Model-S-100D
Full electric car, so zero to none CO2 emission.
Average range = 500km
Average energy usage = 19.0 kWh/100km -> 95 kWh total this is equivalent to a 2.1L/100km traditional petrol car.

Energy
- https://www.radiatorxxl.nl/radiator-vermogen-berekenen
Heating e.g the living room at 22 degrees Celsius requires 77W/m3.
Required amount of Energy by desired Temperature: E = 3.86 x T (Where E is amount of energy required and T is the desired temperature)

- https://www.gaslicht.com/energiebesparing/stroom-opwekken-met-zonnepanelen
Solar energy reduces roughly 0.46 kg per kWh. Six solar panels reduce roughly 630kg a year.

- https://www.rensmart.com/Calculators/KWH-to-CO2

- https://www.co2emissiefactoren.nl/wp-content/uploads/2015/01/2015-01-Elektriciteit.pdf

- https://www.theguardian.com/lifeandstyle/2014/jul/21/greywater-systems-can-they-really-reduce-your-bills

- https://www.dunea.nl/klantenservice/tarieven
In 2018 the price for water was 1.06 euro / 1000L, in 2019 the price was 1.07 euro / 1000L. Households and business customers paid the same price.

- https://financieel.infonu.nl/diversen/56080-wat-kost-een-liter-water.html
On average, 126L of water is used per person on a daily basis.
The average price for water is 1.35 euro per 1000L water. (= 1.35 euro / m3)

- https://www.offthegridnews.com/how-to-2/save-water-and-money-with-a-greywater-system/
Where 1 US gallon = 3.78541178 L. That means 20 US gallons = 75.7082356 L
