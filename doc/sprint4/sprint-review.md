# Sprint Review

## Overview
This was a fruitful week.
After we learned on Monday the functionality for demo 2 was finished we started working some thorough testing.
We also started work on some demo 3 preparations. This could never hurt.
By now the group is working more and more a a whole. Parts are falling together nicely.

## Main problems encountered

### Problem 1: Testing with mockito
Writing tests with mockito turned out to be a intensive researching task.
Figuring out how to do one test turned out to be figuring out how to do all tests.

### Problem 2: What to do next
Once all tasks for a demo are done where do you continue?
We could maybe anticipated this a bit more. Some parts are going to be faster than others.
The GUI did not require testing so they finished a bit early. There was the question of what to do next.
Shortly after they started on some parts of demo 3.

## Adjustments from previous sprint
 - Groups are working together more closely. We start to feel more and more like a team.
 It feels like all of us have a place.

## Adjustments for next sprint
 - Keeping in mind the final report needs to be written.
 - Anticipating running out of work.