# Final report folder

The file you need to fill in is 20190401_draft_0.5_final_report_yourNameHere.tex.
It is in latex but feel free to do it in markdown if it suits you more.
Just make sure I can follow what goes where.
The deadline is the meeting on wednesday 2019-04-03.