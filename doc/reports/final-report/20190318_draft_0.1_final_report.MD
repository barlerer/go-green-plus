# Final report

## authors:
 - our names
 - student numbers
 
## Introduction
Short introduction to the course and the project

## The team
Stating our names, tasks we worked on, roles we fulfilled during the project.
Explanation for how we divided the tasks.
What smaller groups we worked in.

## Project details
  - the project
    - goal (final product, teamwork)
    - the task (global explanation what the assignment is)
    - major parts (global functionality, functionality per demo, testing, checkstyle, gitlab, pipeline)

## User stories
The stories that tell the major features we needed to implement.
Focus on everything that was specified in the rubric.
Lastly stating a user story we added as a bonus.

## Concept
What we got from the assignment and how we are planning to implement.
Designs we made. explanation for how we are going to make the client and server.

## Bonus features
An explanation of what we did extra. What was the user story and how is it implemented.

## Future improvement
There are always things you know can be improved.
There can be a priority list here what we think makes the most sense to do first after release of
the final demo.
Think about:
 - features
 - testing
 - gui
 - code quality

## Choices were made
All choices related to development
During the project we decided on java version, ide, frameworks (graphics, HTTP client, server),
dependencies, ...

## Code quality
We need to talk about the quality of everything we made. For this project is falls down into 
parts. Testing, checkstyle, code review and to a certain degree the pipeline.

  - Testing:
    - Coveragen
    - Mockito usage
    - screenshots
  - Checkstyle:
    - number of warnings
    - standard that is used(I believe its google's)
  - code review:
    
  - pipeline:
    - when was it in use
    - 4 weeks default
    - 2 weeks custom
    - screenshot
    
## Value Sensitive Design
see later...
    
## Process
General: how did the project go?
  1. Did you manage to stick to the planning (why (not))
  2. How did the collaboration in team go?
  3. How did you communicate?
  4. How did version control help (if at all)
  5. What did you learn?

How can the process/collaboration be improved?


## Feedback (individual)
Each team member reflects on how he/she functioned in the
team (at least 200 words per team member):
 - What were your stronger/weaker points during this project
 - Did you have conflicts with other team members? How did you
solve them?

## References:
Eating a vegetarian meal:
http://www.greeneatz.com/foods-carbon-footprint.html
https://www.milieucentraal.nl/klimaat-en-aarde/klimaatklappers/grootste-klappers/

Dependencies: