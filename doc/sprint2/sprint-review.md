# Sprint Review

## Overview

Week 1: The new rules are in place. I think we all now our place in the team better.
There are clear teams we agreed on and the responsibilities are shared equally over them.
The communication is on point. We met twice this week with all members and once with a few to work out a few errors.
Overall there was a lot of research done and the actual implementation is going to start next week on monday.

## Main problems encountered

### Problem 1: The SSL certificate is giving problems
We implemented the SSL certificate on the server-side and in browsers it works.
From java it's a different story.

### Problem 2: Communication server, client
We hoped to be further with the client server connection. It will be there on monday.

## Adjustments from previous sprint
 - Clearer communication
 - Testing from the start

## Adjustments for next sprint
 - Getting the server and client to communicate
 - Starting on some actual implementation