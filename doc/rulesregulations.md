### Regulations:

* Attending meetings is mandatory.

* Members must be present to meeting unless they have been excused. Meeting times must be announced through Mattermost.

* All members must confirm their availability immediately through Mattermost.

* Absence from a meeting must be stated a day in advance.

* Provide clear and self-explanatory commit messages. Good guidelines can be found: https://chris.beams.io/posts/git-commit/

* Add issue numbers to commits.

* Check previous commit messages.

* Create new branches for independent features.

* Merge from a feature branch to the development branch. Merge from the development branch to the master branch.

* Don't break the pipeline.

* Refer to the Punishments section when the regulations are not adhered to. Following repeated offences, the TA (Julian) will be notified.


### Punishments:

* Two broken pipelines - cake

* More than 10 minutes late - cake


**This list is never complete - feel free to develop it further.**