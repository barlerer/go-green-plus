# Sprint Review

## Overview
We are making great progress. The development is in full motion after a week of research.
Everybody is determined to success and it is beginning to show. For the necessary communication
we have frequent meetings and it makes sure the endpoints are connecting. 

## Main problems encountered

### Problem 1: Testing of databases and http requests
There is a lot of technical difficulty involved in testing http request and databases.
There is a lot of material online but to make it fit for our project seems like a challenge
that may be to big for this third week. We know the http request testing is a bonus so we decided
to move it to a later point in time.

### Problem 2: Waiting for finished work of others
Before some work can be done there needs to be work in place of others.
This was to be expected in a software project of this size.
The endpoints of the server really need to be there before the client can start working.

## Adjustments from previous sprint
 - the server and client have a communication set up
 - There is a lot of implementation done. 

## Adjustments for next sprint
 - Better preparation should to know what parts of the application you need from others to be in place before so you can start.
