# GoGreenPlus
A desktop application for helping users becoming more green and making a competition out of who can be more green to the environment.

## What is this?
Java application with JavaFX GUI, client and server side in order to manage a
single server with database to handle all trafic and allow for communications between friends and fellow competitors

## What did I learn here?
I learned how to handle a large project, using SCRUM and Agile methods.
How to manage a team, work in a very tight schedule and to handle unpredicted situations.
How to use a system architecture such as client-server application to make the system modular and allow for future upgrades and replacement.
I also used bCrypt encryption to store user credentials in a database, making sure I keep the credentials as safe as possible.

## Future notes
The graphical user interface can look nicer, and maybe consider making a switch to GraphQL, which in this case might work better than REST api.